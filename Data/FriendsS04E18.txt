

The One With Rachel<U+0092>s New Dress

Teleplay by: Jill Condon & Amy Toomin
Story by: Andrew Reich & Ted Cohen
Transcribed by: Eric Aasen

[Scene: Central Perk, Joey and Chandler are there as Phoebe enters carrying a drum.]

Phoebe: Hey!

Chandler: Hey! Wow, it is true what they say, pregnant bellies look like
a drum.

Phoebe: (not amused) Ha-ha. (She sits down on the couch.) No, it<U+0092>s just
I<U+0092>m so pregnant that I<U+0097>my guitar doesn<U+0092>t fit anymore. So I thought
<U+0091>til I<U+0092>m not, I<U+0092>m just gonna play all my songs on this drum. It sounds really
cool!

Chandler: All right.

Phoebe: Listen. Listen. (She starts to play and sing.) Smelly cat, smelly
cat, what are they feeding you? 


Joey: Wow, Pheebs! That sounds great!

Phoebe: I know! I know, and I<U+0092>ve only been playing for like an hour!

Alice: (entering) Phoebe! Phoebe! Hi! Hi!

Phoebe: Hey! What are you doing here?

Alice: Umm, actually, I came down to ask you a big favour.

Phoebe: Oh, well, don<U+0092>t tell me you want to keep more of your stuff in my
uterus.

Alice: (laughs) No. No. No. (Sits down.) Okay, now, see, I wanna name the girl
baby Leslie, and Frank wants to name one of the boy babies Frank JR. JR. 

Chandler: Wouldn<U+0092>t that be Frank the III?

Alice: Don<U+0092>t get me started. (To Phoebe) Anyway, umm, since there are three
babies and umm, we both got to put our names in, we would be truly honoured if you named
the other boy baby.

Phoebe: Wow! That<U+0092>s so great! Oh! Oh! Cougar.

Alice: You think about it. (Leaves)

Opening Credits

[Scene: Ross<U+0092>s apartment, he and Emily are getting ready to go to the airport.]

Emily: I left a bra drying on the shower rod, you don<U+0092>t think your son will
think it<U+0092>s yours and be horribly traumatised? 

Ross: Hey, if mommy can have a wife, daddy can have a bra.

Emily: (checks the clock) Ohh, it<U+0092>s time to go.

Ross: Oh, no-no-no, see, that-that clock<U+0092>s a little fast, uh, we have 17
minutes. Huh, what can we do in 17 minutes? Twice?

Emily: Well that<U+0092>s ambitious.

(They kiss but are interrupted by a knock on the door.)

Ross: Hey, uh, you can ignore that.

Emily: That<U+0092>s Carol with your son!

Ross: Uhh, believe me when he<U+0092>s older, he<U+0092>ll understand.

Carol: (knocking on the door) Ross!

Ross: I<U+0092>ll be right there. (He goes over and opens the door to Carol,
Susan, and Ben.) (To Ben.) Hello! (To Carol.) Hello! (To Susan.) Hey. Uhh, Emily, this is
Carol and Susan.

Susan: Hey, it<U+0092>s so nice to finally meet you!

Emily: Me too!

Carol: Ohh, y<U+0092>know, Susan<U+0092>s gonna be shooting a commercial in London
next week.

Susan: Oh yeah, I<U+0092>m so excited, I<U+0092>ve never been there.

Emily: Oh, well, I<U+0092>ll show you around.

Susan: That would be great! Also, uh, I was hoping to catch a show so if you can
make any suggestions<U+0085>

Emily: Oh, there<U+0092>s tonnes of terrific stuff<U+0097>I<U+0092>ll go with you!

Susan: Ahh!

(Ross accidentally, on purpose, bumps into Susan.)

Ross: Look at you two, bonding, making us late for the airport so<U+0085>

Emily: Are you all right?

Susan: Oh, he<U+0092>s fine. He<U+0092>s fine. It<U+0092>s just that us getting along
is difficult for him, because he doesn<U+0092>t like me.

Ross: Oh come on! That<U+0092>s-that<U+0092>s<U+0085> true.

[Scene: Monica and Rachel's erm, Chandler and Joey's, Joey and Chandler are playing
foosball as Phoebe enters.]

Phoebe: Hi!

Joey: Hey!

Chandler: Hey! Do we have a baby name yet?

Phoebe: Ugh! No! This is so hard! I went through this whole book (Holds up a
book) and found nothing! I want a name that<U+0092>s really like, y<U+0092>know strong and
confident, y<U+0092>know? Like-like Exxon. 

Chandler: Well, it certainly worked for that Valdez kid.

Joey: Ooh-ooh, Pheebs, you want a strong name? How about, The Hulk?

Phoebe: No, I<U+0092>m-I<U+0092>m not sure about Hulk, but I like the idea of a name
starting with "The."

Joey: Oh, want a good name, go with Joey. Joey<U+0092>s your pal. Joey<U+0092>s your
buddy. "Where is everybody?" "Well, they<U+0092>re hanging out with
Joey."

Chandler: Hey, y<U+0092>know what, if you<U+0092>re gonna do that, if you<U+0092>re
gonna name him Joey, you should name him Chandler. (Phoebe doesn<U+0092>t think so.) Oh,
come on! Chandler<U+0092>s funny, sophisticated, and he<U+0092>s very loveable, once you get
to know him.

Joey: Oh well, hey, Joey<U+0092>s loveable too! But the thing about Joey is, if
you need him, he<U+0092>ll be there.

Chandler: Well, Chandler will be there for you too. I mean, well, he might be a
little late, but-but, he<U+0092>ll be there. And he<U+0092>ll bring you some cold soda, if
want you need him for is that you<U+0092>re really hot.

Joey: What do ya say? What do ya say?

Phoebe: Well, I, I like the idea of naming him after someone I love, and Joey
and Chandler are great names. (They both stare at her.) But, all right, I
don<U+0092>t<U+0097>maybe I<U+0092>ll just name him The Hulk.

Joey: I knew I shouldn<U+0092>t have mentioned it! That<U+0092>s what I wanted to
name my kid!

[Scene: Chandler and Joey's erm, Monica and Rachel's, Monica is cooking and Rachel is
getting ready for a date with Joshua.]

Rachel: Hey, Mon, if you were hoping to sleep with Joshua the first time
tonight, which one of these would you want to be wearing. (She<U+0092>s holding two frilly,
lace nighties.)

Monica: Y<U+0092>know what? It really creeps me out choosing other
people<U+0092>s sex clothes.

Rachel: Sorry. I<U+0092>m so exited! I<U+0092>ve been waiting for this for months! I
got my hair coloured! I got new sheets! I<U+0092>m making him a very fancy meal. 

Monica: Um-hmm.

Rachel: What am I making him by the way?

Monica: Well, you<U+0092>re making him a frieze salad with goat cheese and pine
nuts, wild nuts, wild rice, roast asparagus, and salmon au croup.

Rachel: I thought I was making him filet mignon? 

Monica: Yeah, you were, but you decided to make salmon because you had some left
over at the restaurant. And then you realised if you (Points at Rachel) bitched about it,
then you (Points to herself) would stop cooking, and you (Points at Rachel) would have to
make your famous baked potato and Diet Coke.

Rachel: Wow, I really get crabby when I cook.

[Scene: Monica and Rachel's erm, Chandler and Joey's, Joey, Chandler, and Phoebe are
there as Ross enters.]

Ross: Hey!

Joey: Hey!

Chandler: Hey!

Ross: So uh, Emily called last night<U+0085>

Chandler: And now you<U+0092>re giving me the message!

Ross: Turns out them Emily is just crazy about Susan. Yeah, they<U+0092>re
going to the theatre together! They<U+0092>re going to dinner! They<U+0092>re going horseback
riding!

Phoebe: God, Susan is so fun!

Ross: Look, this is just a little too familiar, okay? For like, for like six
months before Carol and I spilt up, all I heard was: "My friend Susan is so smart. My
friend Susan is so funny. My friend Susan is so great."

Chandler: You actually think that something can happen between Emily and Susan?

Ross: Hey, they<U+0092>re going to the gym together! Two women! Stretching!
Y<U+0092>know they-they take a steam together! Things get a little playful<U+0097>didn<U+0092>t
you see Personal Best?

Joey: No, but I<U+0092>m gonna!

Chandler: Hi! Hi! You<U+0092>re crazy! Okay? This is Emily. Emily is straight.

Ross: How do you know? I mean we thought Carol was straight before I married
her!

Phoebe: Yeah, I definitely. I don<U+0092>t like the name Ross.

Ross: What a weird way to kick me when I<U+0092>m down.

Phoebe: No! No! I-I meant for the baby!

Ross: Oh. What<U+0092>s wrong with Ross?

Phoebe: Well, it<U+0092>s just y<U+0092>know that something like this would never to
like The Hulk, y<U+0092>know<U+0085>

Ross: Actually that-that<U+0092>s not true, in The Incredible Hulk uh, No.
72, Dr. Bruce Banner found<U+0085> (Sees everyone staring at him and stops.) Y<U+0092>know,
ugh, nevermind, my girlfriend<U+0092>s a lesbian. (Leaves.)

Phoebe: So, I decided I<U+0092>m definitely going to go with either Joey or
Chandler.

Joey: Oh! Oh-oh, you gotta pick Joey! I mean, name one famous person named
Chandler.

Chandler: Raymond Chandler.

Joey: Someone you didn<U+0092>t make up!

Chandler: Okay, there are no famous Joey<U+0092>s. Except for, huh, Joey
Buttafucco.

Joey: Yeah, that guy really hurt us.

Phoebe: Well, how about a compromise then, okay? What if it<U+0092>s like
y<U+0092>know, Chanoey?

Chandler: Okay, look, Joey! Come on, think about it, first of all, he<U+0092>ll
never be President. There<U+0092>s never gonna be a President Joey.

Joey: All right look man, I didn<U+0092>t want to bring this up, but Chandler, is
the stupidest name I ever heard in my life! It<U+0092>s not even a name; it<U+0092>s barely
even a word. Okay? It<U+0092>s kinda like chandelier, but it<U+0092>s not! All right?
It<U+0092>s a stupid, stupid non-name!

Chandler: Wow, you<U+0092>re, you<U+0092>re right. I have a horrible, horrible name.

Joey: I<U+0092>m sorry man, I didn<U+0092>t<U+0097>I<U+0092>m-I<U+0092>m sorry. I<U+0092>m
sorry. (Goes over and comforts him.) 

Chandler: Okay.

Joey: So I guess it<U+0092>s Joey then!

[Scene: Chandler and Joey's erm, Monica and Rachel's, Rachel is on her dinner date with
Joshua.]

Joshua: This is so nice. Thank you for doing this.

Rachel: Ohh, please! Cooking soothes me. (They kiss.) Ahh. So, dig in!

Joshua: Great! Oh, it all looks sooo good!

Rachel: (taking a bite) Hmmm!

Joshua: Oh my God!

Rachel: Oh I know, my God, this is so<U+0097>this rice is so<U+0097>I am so good.

Joshua: Behind you?

Rachel: (sees the chick and the duck) Oh, yeah, I<U+0092>m sorry. They used to
live here; sometimes they migrate back over.

Joshua: (getting up and backing away from they.) Is there ah, is there some way
they can not be here. It<U+0092>s just ah, farm birds really kinda freak me out!

Rachel: Yeah, sure, okay. Okay.

(Rachel gets up and ushers them into the hall, as they pass Joshua, he leaps onto the
counter to avoid them. Rachel drops them off in the hall, and knocks on Joey<U+0092>s door.)

Joey: (answering the door.) Hey, how did you do that?! Come on in. (He brings
them inside.)

[Cut back to Rachel<U+0092>s date.]

Rachel: All gone! So, farm birds, huh?

Joshua: Yeah, it<U+0092>s-it<U+0092>s my only weird thing, I swear. And I-I-I
would<U+0092>ve told you about it, but I didn<U+0092>t know they would be here.

Rachel: Oh.

Joshua: So, all right.

(They both sit back down.)

Rachel: Okay. So, can I serve you a little of<U+0097>What? What? What? (She sees
that Joshua isn<U+0092>t relaxed.)

Joshua: Nothing I uh, it<U+0092>s just that I know that they<U+0092>re still out
there.

Rachel: But, they<U+0092>re across the hall! I mean that<U+0092>s two doors away, it
would take them a long time to peck their way back over here.

Joshua: Okay, that<U+0092>s-that<U+0092>s not funny. Uhh.

Rachel: Okay, y<U+0092>know, would you feel better if we went someplace else? I
mean we could pack all this stuff up and y<U+0092>know go to your apartment.

Joshua: Oh, they<U+0092>re working on this week, it<U+0092>s a total mess. But uh,
I<U+0092>m staying at my parents<U+0092> house, we could go there.

Rachel: Your parents<U+0092>?

Joshua: Yeah, they<U+0092>re out of town.

Rachel: Ohh.

Joshua: Yeah-yeah, it<U+0092>s this huge place, and-and it<U+0092>s got this
gorgeous view of the park, and very, very romantic. What do you say?

Rachel: Yeah that works.

(He moves to kiss her, but stops when he hears the duck.)

Joshua: They-they-they can smell fear.

[Scene: Ross<U+0092>s apartment, Carol has come to pick up Ben.]

Ross: (opening the door.) Hey!

Carol: Hey! How<U+0092>s Ben?

Ross: Well, I asked him if he wanted to eat, he said, "No." I asked
him if he wanted to sleep, he said, "No." I asked him what he wanted to do, he
said, "No." So, he<U+0092>s sweeping. (We see Ben playing with a broom and a
dustpan.)

Carol: Hey, Ben! Hey!

Ross: So umm, any word from Susan?

Carol: Ooh, yeah! She said she<U+0092>s having sooo much fun with Emily.

Ross: Uh-huh. Uh-huh. Uh, by the by, did it uh, did it ever occur to you that, I
don<U+0092>t know, maybe they might be having a little too much fun?

Carol: What<U+0092>s too much fun?

Ross: Y<U+0092>know, the kind of fun, you and Susan had when we were married.

Carol: Oh my God, you are so paranoid! 

Ross: Am I?!

Carol: Yes!

Ross: Am I?!

Carol: I can<U+0092>t speak for Emily, but Susan is in a loving, committed
relationship.

Ross: Uh-huh, Carol, so were we. All right, just-just imagine for a moment,
Susan meets someone and-and they really hit it off. Y<U+0092>know? Say-say they<U+0092>re
coming back from the theatre, and they-they stop at a pub for a couple of drinks,
they<U+0092>re laughing, y<U+0092>know, someone innocently touches someone else<U+0085>
There<U+0092>s electricity, it<U+0092>s new. It<U+0092>s exciting. Are you telling me there
isn<U+0092>t even the slightest possibility of something happening?

Carol: Maybe.

Ross: OH MY GOD!! I didn<U+0092>t really believe it until you just said it!!

[Scene: Joshua<U+0092>s parents<U+0092> apartment, Rachel and Joshua are entering.]

Joshua: <U+0085>and even though none of the other kids believed me, I swear to
God, that duck pushed me!

Rachel: Wow! This place is fabulous!

Joshua: Yeah, yeah, let me show you around. This is the uh, downstairs living
room.

Rachel: Whoa-whoa, there<U+0092>s two living rooms? God, growing up here, this
place must<U+0092>ve been a real babe magnet.

Joshua: Yeah, well, it would<U+0092>ve been, but uh, my parents just moved here.

Rachel: Ohh, you should know, this place is a real babe magnet. Wanna make out?

(They kiss.)

Joshua: Hey, here<U+0092>s an idea. Why don<U+0092>t uh, I put the food in the
fridge and we can eat it later?

Rachel: That sounds like a plan. Umm, is there a place I can go freshen up?

Joshua: Oh yeah, yeah uh, it<U+0092>s down the hall and uh, second door to your
left.

Rachel: Ah.

(She goes down the hall. Joshua goes to put the food away when his parents walk in.)

Mrs. Burgin: Oh, hi, darling!

Joshua: Mom, Dad, what are you guys doing here?

Mrs. Burgin: Oh, well we cut the trip short.

Mr. Burgin: France sucks!

Joshua: Umm, this may be a little weird, but I-I-I got a date here.

Mrs. Burgin: Oh, say no more!

Mr. Burgin: We<U+0092>ll just grab some food and take it with us right upstairs,
and we<U+0092>ll be right out of you hair.

Joshua: Oh, that-that would be great. So you didn<U+0092>t even get to Italy?

Mr. Burgin: Yep, sucks!

(They all go into the kitchen. Just then, Rachel comes back from the bathroom; she had
removed her dress and is wearing nothing but a lace nightie. She tries to find someplace
seductive to wait for Joshua. She tries to sit on the piano, but it makes too much noise.
So she goes over to the couch and kinda half lays down to wait for Joshua. Joshua comes in
from the kitchen, sees Rachel, and freezes.)

Rachel: Hi you!

Joshua: Oh my God!

Rachel: I know, I can do more than cook.

(Just then, his parents enter. Rachel gasps.)

Mr. Burgin: I like her. She sees smart.

Commercial Break

[Scene: Joshua<U+0092>s parents<U+0092> apartment, continued from earlier.]

Joshua: Uhh, Rachel, my parents<U+0085>

Rachel: Ohh! It<U+0092>s so nice to meet you. (She goes over and shakes their
hands.) Hello. 

Mr. Burgin: Hi.

Rachel: Hello.

Mrs. Burgin: Hello. Well, Joshua, that $500 was for groceries.

Rachel: What? This-this, no, oh no, no-no-no, this is
not<U+0097>that<U+0092>s-that<U+0092>s not what it is. See, see, okay, I work in fashion, see
and-and, this is a real dress actually. It<U+0092>s-it<U+0092>s, they<U+0092>re-they<U+0092>re
wearing it in Milan, so part of my job is too wear the clothes, and then I see how people
respond, and then I report back to my superiors at Bloomingdale<U+0092>s, so<U+0085>
And obviously in uh, in-in this case, (She grabs a pen and paper) I am going to report
back, "USA not ready."

Mrs. Burgin: Maybe in L.A?

Rachel: Yes!

Joshua: There you go.

Mr. Burgin: So, have you kids eaten yet?

Rachel: Well, we were going to do that after<U+0097>I mean umm, next.

Mr. Burgin: Well, we<U+0092>re starving, why don<U+0092>t we all go get something to
eat?

Rachel: Oh, yeah, well<U+0085> Yeah, no use wasting this baby, just lyin<U+0092>
around the house. 

Mr. Burgin: So<U+0085> We go eat.

Rachel: Yes.

Mr. Burgin: You<U+0092>ll wear that. We<U+0092>ll be eating, and of course,
you<U+0092>ll be wearing that.

[Scene: Monica and Rachel's erm, Chandler and Joey's, Chandler is looking for a new
name in Phoebe<U+0092>s book of names.]

Joey: Dude, I am sorry about what I said!

Chandler: No, no, you<U+0092>re right, it is a ridiculous name!

Joey: It<U+0092>s not that bad.

Chandler: Yes it is! From now on, I have no first name.

Joey: So, you<U+0092>re just Bing?

Chandler: I have no name.

Phoebe: All right, so, what are we supposed to call you?

Chandler: Okay uh, for now, temporarily, you can call me, Clint.

Joey: No way are you cool enough to pull of Clint.

Chandler: Okay, so what name am I cool enough to pull off?

Phoebe: Umm, Gene.

Chandler: It<U+0092>s Clint. It<U+0092>s Clint! (He heads for his bedroom.)

Joey: See you later, Gene.

Phoebe: Bye, Gene.

Chandler: It<U+0092>s Clint! Clint!

Joey: What<U+0092>s up with Gene?

[Scene: Central Perk, Rachel is telling Phoebe and Monica of her date.]

Monica: So, you wore your nightie to dinner?

Rachel: Oh, yeah. And uh, the best part though, when the uh, waiter spilled
water down my back, I jumped up, and my boob popped out.

Phoebe: Oh my God!

Monica: Oh, no!

Rachel: No, it<U+0092>s all right. I got nice boobs. (Phoebe and Monica nod there
heads in agreement.)

Ross: (returning from the phone.) So, I just picked up a message from Emily, she
and Susan are going to a poetry reading together! 

Rachel: So?

Ross: So! Poetry? Susan<U+0092>s gay! They<U+0092>re being gay together!

Monica: Emily<U+0092>s straight.

Ross: Oh, wake up!

Phoebe: Wow, Carol really messed you up!

Ross: Excuse me?

Phoebe: Yeah, she turned you into this-this-this untrusting, crazy, jealous,
sycophant. (They all look at her.) All right, so I don<U+0092>t know what sycophant means,
but the rest is right.

Ross: Look, I don<U+0092>t know what you<U+0092>re talking about, I am not a crazy,
jealous person.

Rachel: Huh.

Ross: What?

Rachel: She<U+0092>s totally right! When we were together, you got all freaked out
about Mark and there was nothing going on.

Monica: This totally makes sense!

Ross: It does not!

Monica: Oh, sure it does! In high school, you weren<U+0092>t jealous at all even
though all your girlfriends were cheating on you!

Phoebe: All right, all right, so up until <U+0091>92-93 he was very trusting, then
<U+0092>94 hit, Carol left him and bamn! Paranoid city!

Rachel: Absolutely! Absolutely!

Monica: This is so much fun!

Ross: This is not fun!

Monica: Look, all we<U+0092>re trying to say is, don<U+0092>t let what happened with
Carol ruin what you got with Emily.

Phoebe: Yeah. The <U+0092>92 Ross wouldn<U+0092>t.

Ross: Well, I still think I was right about that whole Mark thing.

Rachel: What<U+0097>yeah<U+0097>what, y<U+0092>know what? I hope Emily is a
lesbian.

[Scene: Monica and Rachel's erm, Chandler and Joey's, Phoebe is showing off more of her
drum skills to Joey by rubbing one of the sticks back and forth across the drum.]

Phoebe: Drum roll.

Chandler: (entering) Okay. Okay. All right. Help! Am I a Mark, or a John?

Joey: Nah, you<U+0092>re not tall enough to be a Mark, but you might make a good
Barney.

Chandler: All right look, am I serious, okay? Tomorrow at 3:30 I am going down
to the courthouse.

Phoebe: You<U+0092>re actually going through with this?

Chandler: Hey, look, this name has been holding me back my entire life. Okay,
it<U+0092>s probably why kids picked on me in school, and why I never do well with
women<U+0085> So, as of 4 o<U+0092>clock tomorrow, I<U+0092>m either gonna be Mark Johnson or
John Markson.

Phoebe: You got problems because of you! Not your name! All right, this has got
to stop! Chandler is a great name! In fact<U+0097>yes, (To Joey) I<U+0092>m, I<U+0092>m sorry. I
know you really wanted me to name the baby Joey, but eh, so, I<U+0092>m-I<U+0092>m, I<U+0092>m
gonna, I<U+0092>m gonna name the baby Chandler.

Chandler: (pleased) Really?!

Phoebe: Yeah, but you have to keep the name too!

Chandler: Okay. Thanks.

Phoebe: Okay!

Chandler: You wanna hug it out?

Phoebe: Yeah!

(They both hug.)

Phoebe: Yay! 

Chandler: Yay! 

Phoebe: Yay<U+0097>oh<U+0097>yay! Okay, I gotta go tell Frank and Alice! Right now!

Chandler: Okay!

Phoebe: Ooh, uh<U+0085> (She grabs her coat and runs out.)

Chandler: Bye, Pheebs!

Phoebe: Okay, bye! 

(She exits, and after the door is closed, Chandler turns to Joey and<U+0085>)

Chandler: Ha! Ha! Ha!

Joey: Ohh! (Realises it was all a trick to get Phoebe to name the baby
Chandler.)

Closing Credits

[Scene: The Airport, Carol and Ross are waiting for Emily and Susan to deplane. A
gorgeous woman walks by and they both turn to watch her go.]

Ross: Nice luggage.

Carol: I was gonna say<U+0085>

(Susan and Emily get off.)

Susan: Hey!

(They both run and hug they<U+0092>re respective partners.)

Ross: Hi!

Emily: Hey! I missed you.

Ross: Oh, I missed you too.

Susan: (To Emily) Thanks for everything, I had such a great time.

Emily: Oh, so did I.

(They hug and give each other a little peck on the cheek.)

Ross: (To Carol) No tongue. (And gives her the thumbs up.)

End

