

The One With The Worst Best Man Ever

Teleplay by: Michael Curtis
Story by: Seth Kurland
Transcribed by: Eric Aasen

[Scene: Central Perk, the gang is there, Phoebe is returning from the bathroom.]

Phoebe: (angrily) That<U+0092>s like the tenth time I<U+0092>ve peed since I<U+0092>ve
been here!

Monica: That<U+0092>s also like the tenth time you told us.

Phoebe: Yeah, oh I<U+0092>m sorry, it must be really hard to hear! I tell ya,
it<U+0092>s a lot easier having three babies play Bringing in the Noise, Bringing in da Funk
on your bladder! I<U+0092>m so sick of being pregnant! The only happiness I get is from a
cup of coffee, which of course is decaf, <U+0091>cause<U+0097>Oh! I<U+0092>m pregnant!

Ross: Pheebs, did<U+0085>you want a cookie?

Phoebe: (starting to cry) Thank you so much.

Rachel: So uh, Pheebs, honey, how are those mood swings coming?

Phoebe: I haven<U+0092>t really had any yet.

(Monica, Joey, and Chandler all shake their heads.)

Opening Credits

[Scene: Chandler and Joey's, Joey and Chandler are there as Ross enters.]

Ross: Hey guys!

Chandler: Hey.

Joey: Hey!

Ross: All right, here<U+0092>s the ring. (Shows Chandler the wedding ring he plans
on giving Emily) 

Chandler: (shocked) Yes! Yes! A thousand times, yes!

Ross: So uh, any ideas for the bachelor party yet?

Joey: Whoa-whoa-whoa! Before you start handing out wedding rings and planning
bachelor parties, don<U+0092>t you have to decide who your best man is gonna be?

Chandler: Oh, it<U+0092>s awkward. It<U+0092>s awkward. It<U+0092>s awkward.

Ross: I sort<U+0092>ve already asked Chandler.

Joey: What?! He got to do it at your first wedding!

Ross: Joey, I figured you<U+0092>d understand. I mean, I-I<U+0092>ve known him a lot
longer.

Joey: Come on Ross! Look, I-I don<U+0092>t have any brothers; I<U+0092>ll never get
to be a best man!

Chandler: You can be the best man when I get married.

Joey: (pause) I<U+0092>ll never get to be a best man!

Ross: (to Chandler) Wait-wait, so, you get to be my best man twice and I never
get to be yours at all?

Chandler: Oh no-no-no, you<U+0097>yeah, of course you get to be my best man.

Joey: (impatiently tapping Chandler on the shoulder) What about me?! You-you
just said I could!

Chandler: I<U+0092>m not even getting married! Okay, this is a question for
science fiction writers!

Joey: I can<U+0092>t believe you<U+0092>re not picking me.

Ross: Hey, how can it not be me?!HeyHey!

Chandler: I<U+0092>m not even<U+0085> I<U+0092>m not even<U+0085>

Ross: Fine, y<U+0092>know what, that<U+0092>s it. From now on, Joey, I want you to
be my best man.

Joey: Yes! (to Chandler) Shame about you man.

[Scene: Monica and Rachel's, Monica, Rachel, and Phoebe are eating breakfast.]

Phoebe: (to her babies) Stop it!

Monica: What?

Phoebe: One of the babies is kicking.

Monica: I thought that was a good thing.

Phoebe: It<U+0092>s not kicking me, it<U+0092>s kicking one of the other babies. Oh
(looks down her dress)! Don<U+0092>t make me come in there!

Joey: (entering) Hey!

Monica: Hey!

Joey: Do you guys have like a big bowl I can borrow?

Monica: Yeah, there<U+0092>s one right under the cabinet. 

Joey: (grabs it) Thanks.

Monica: Why do you need it?

Joey: Oh, we<U+0092>re having a big party tomorrow night. Later! (Starts for the
door.)

Rachel: Whoa! Hey-hey, you planning on inviting us?

Joey: Nooo, later. (Walks out the door.)

Phoebe: Hey!! Get your ass back here, Tribbiani!! (Joey walks back in, scared.)

Rachel: Hormones!

Monica: What Phoebe meant to say was umm, how come you<U+0092>re having a party
and we<U+0092>re not invited?

Joey: Oh, it<U+0092>s Ross<U+0092>s bachelor party.

Monica: Sooo?

Joey: Are you bachelors?

Monica: Nooo!

Joey: Are you strippers?

Rachel: Nooo!

Joey: Then you<U+0092>re not invited. (Starts for the door again.)

Rachel: All right fine! You<U+0092>re not invited to the party we<U+0092>re gonna
have either.

Joey: Oh-whoa, what party?

Rachel: Well umm<U+0085>

Monica: The baby shower for Phoebe!

Joey: Baby shower. Wow! That sounds sooo like something I don<U+0092>t want to do!
Later! (Finally, he makes his exit.)

Phoebe: I can<U+0092>t believe I<U+0092>m gonna have a party! This is so great!
(Really excited) A party! (Really, really excited) Yay!! (Suddenly, she starts crying and
Rachel moves to comfort her.) I don<U+0092>t know why.

[Scene: Central Perk, Joey and Ross are talking over party plans.]

Joey: This is what I<U+0092>ve got going for the party so far, liquor wise. Get a
lot of liquor.

Ross: Great. Great.

Joey: Okay, now uh, in terms of the invite list, I<U+0092>ve got you, me, and
Chandler and I<U+0092>m gonna invite Gunther <U+0091>cause, well, we<U+0092>ve been talking
about this pretty loud.

Gunther: I<U+0092>ll be there.

Joey: All right<U+0097>oh! Listen, I know this is your party, but I<U+0092>d really
like to the number of museum geeks that are gonna be there.

Ross: Yeah. Tell ya what, let<U+0092>s not invite any of the anthropologists, only
the dinosaur dudes!

Joey: Okay! We<U+0092>ll need a six-pack of Zima.

Chandler: (entering) Hey guys, what are you doing?

Ross: Oh, just planning my bachelor party with my best man.

Chandler: Yeah, well, good luck trying to top the last one.

Ross: Yeah, see, I don<U+0092>t think it<U+0092>s gonna that difficult considering
this one won<U+0092>t be taking place in the basement of a Pizza Hut.

Chandler: Oh, I<U+0092>m Ross. I<U+0092>m Ross. I<U+0092>m too good for the Hut;
I<U+0092>m too good for the Hut.

Ross: Look, I gotta go pick up Ben. Everything so far sounds great Joey, just
remember to keep it on the mellow side, okay? Just a couple of guys hanging out playing
poker, no-no strippers or anything okay?

Joey: You got it.

Ross: Okay, see ya later.

Chandler: See ya. (Ross exits, and Chandler moves over next to Joey, laughing.)
Have fun planning your mellow bachelor party.

Joey: Well, there<U+0092>s gonna be strippers there. He didn<U+0092>t say anything
about no strippers.

Chandler: He just said, "No strippers."

Joey: Oh, I chose not to hear that.

[Scene: Monica and Rachel's, Monica is returning from shopping and Rachel is there.]

Monica: Look what I got! Look what I got! Look what I got! (She shows Rachel
what she bought. She bought a little leather jacket and a little cowboy outfit for the
babies.) Can you believe they make these for little people?

Rachel: Little village people.

Monica: Okay, look at this one. This is my favourite. (It<U+0092>s a little pink
and white dress for the girl baby.)

Rachel: Oh, that is so sweet!

Monica: I know! Phoebe is gonna love dressing them in these!

Rachel: Huh. Except, Phoebe<U+0092>s not gonna be the one that gets to dress them.

Monica: Because she<U+0092>s not gonna get to keep the babies.

Rachel: Oh my God! We are throwing the most depressing baby shower ever!

Monica: Wait a minute! Unless, we give her all gifts she can use after
she<U+0092>s done being pregnant. Like-like umm, regular coffee, Tequila.

Rachel: Oh, and somebody can get those leather pants she<U+0092>s always wanted!

Monica: Oh, she<U+0092>s gonna love that!

[Scene: Monica and Rachel's, Phoebe<U+0092>s baby shower, she is holding those leather
pants, and isn<U+0092>t happy about it.]

Phoebe: What the hell is this?! What, did you actually thought it would make me
feel better to give me stuff that I can<U+0092>t use for another two months?! This sucks!
All right, what<U+0092>s my next present?!


All: I don<U+0092>t have anything. (All of the rest of the women there hide their
gifts behind their backs.)

[Scene: Chandler and Joey's, Ross<U+0092>s bachelor party. Ross is thanking Joey for the
party.]

Ross: Hey listen man, about the stripper<U+0085>

Joey: Yeah?

Ross: Good call!

Chandler: (banging a spoon against his beer bottle) Okay, a little announcement,
a little announcement. I<U+0092>ve decided that my best man is, my best friend Gunther!

Gunther: What<U+0092>s my last name?

Chandler: Central Perk?

Gunther: (to Ross) Thanks for not marrying Rachel. (He starts to leave.)

Joey: Oh-whoa-wait, Gunther don<U+0092>t-don<U+0092>t forget your shirt. (He gives
Gunther his shirt and Gunther leaves.)

Ross: Hey-hey, what are those?

Joey: Oh, little party favours, check it out! (It<U+0092>s a shirt that reads,
"Ross Geller, Bachelor Bash 1998")

Ross: Wow! Yeah!

Joey: Oh-oh! (Shows him what<U+0092>s on the back, "Best Man Joey Tribbiani,
with a huge picture of him.)

Chandler: (banging on the bottle again) Okay, okay, a little announcement, I
just want everyone to know that the position of my best man is still open! And uh, (to the
stripper) so is the position of the bride.

The Stripper: Great!

Ross: Smooth man. Yeah, you got some chilie on your neck. (Chandler checks and
runs into the bathroom.) Well, I just want to say, thanks everyone, this-this was great.
And hey! See you guys Monday morning. (They museum geeks wave at him.) Thanks Joey.

Joey: Oh, hey, don<U+0092>t forget your shirt.

Ross: Oh, thanks! (Takes it and throws it back into the box and leaves.)

Joey: Okay, hey, museum geeks, party<U+0092>s over. Okay. Wave bye-bye to the nice
lady. There you go. Back to your parent<U+0092>s basement. All right. (The museum geeks exit
and Joey unlocks his door and lets the chick and the duck out.) Come on boys, come on out!
Here you go. All right.

The Stripper: Ohhh, look at the little birdies! Are those yours?

Joey: Yeah!

The Stripper: Wow, I didn<U+0092>t know they let you keep chickens and ducks as
pets.

Joey: Oh yeah-yeah. And I got the duck totally trained. Watch this. Stare at the
wall. (The duck complies.) Hardly move. (The duck complies.) Be white. (The duck
complies.)

The Stripper: You are really good at that. So uh, I had fun tonight, you throw
one hell of a party.

Joey: Oh thanks. Thanks. It was great meetin<U+0092> ya. And listen if any of my
friends gets married, or have a birthday, or a Tuesday<U+0085>

The Stripper: Yeah, that would be great. So I guess umm, good night.

Joey: Oh unless you uh, you wanna hang around.

The Stripper: Yeah?

Joey: Yeah. I<U+0092>ll let you play with my duck.

[Scene: Joey<U+0092>s bedroom, it<U+0092>s the middle of the night, he<U+0092>s waking up and
discovers he<U+0092>s alone in bed.]

Joey: Hey, (realises he doesn<U+0092>t know her name.) stripper! (He notices that
the ring box is open, so he picks it up, sees it<U+0092>s empty and starts to panic.)

Commercial Break

[Scene: Chandler and Joey's, it<U+0092>s continued from earlier. Joey is now waking
Chandler and telling him the news.]

Joey: (running and banging on Chandler<U+0092>s door) The stripper stole the
ring!! The stripper stole the ring!! Chandler! Chandler, get up! Get up! The stripper
stole the ring!

Chandler: (opening the door) What?

Joey: The ring is gone!

Chandler: Ugh. Just a sec, give me a minute to wake up for this<U+0097>Ah-ha-ha!!
You lost the ring! You<U+0092>re the worst best man ever!

Joey: Dude, this isn<U+0092>t funny! What am I gonna do?! I go to bed last night,
everything<U+0092>s cool! I wake up this morning, the stripper<U+0092>s gone and the ring is
gone!

Chandler: You slept with the stripper?

Joey: Of course!! (Shrugs.)

[Scene: Central Perk, Phoebe is entering, Monica and Rachel are talking on the couch.]

Phoebe: Hi, guys.

Rachel: Hi! Phoebe. (Both Monica and her try to move out of Phoebe<U+0092>s way.)

Monica: Hi Phoebe.

Phoebe: I-I wanted to apologise if I<U+0097>y<U+0092>know seemed a tad edgy
yesterday at my shower. Y<U+0092>know it<U+0092>s just the hormones, y<U+0092>know.

Rachel: No we<U+0085>

Monica: Hormones.

Rachel: <U+0085>hormones, yeah.

Phoebe: Anyway, I just wanted to say thank you, it was just, it was so sweet.
(She goes to hug them and they both flinch, thinking that Phoebe is about to attack them.)

Monica: Wow, you seem to be doing so much better. That<U+0092>s great. So how-how
are things going?

Phoebe: Good. Y<U+0092>know<U+0097>no-no, okay, it<U+0092>s-it feels like
everything<U+0092>s been about me lately, so what<U+0092>s happening with you?

Rachel: Oh, well, actually we were just talking about me not going to
Ross<U+0092>s wedding.

Phoebe: Oh!

Rachel: It just might be too hard, given the history and all that<U+0085>

Phoebe: Wow! This reminds me of the time when I was umm, living on the street
and this guy offered to buy me food if I slept with him.

Rachel: Well, h-how is this like that?

Phoebe: Well, let<U+0092>s see, it<U+0092>s not. Really, like that. Because, you see
that was an actual problem, and uh, yours is just like y<U+0092>know a bunch of
y<U+0092>know high school crap that nobody really gives y<U+0092>know<U+0085>

Rachel: (starting to cry) I<U+0092>m-I<U+0092>m sorry, I just thought that<U+0085>

Phoebe: Alrighty, here come the water works. (Rachel starts crying harder.)

[Scene: Chandler and Joey's, Joey is trying to figure out what to do.]

Joey: Ugh! I don<U+0092>t know what I<U+0092>m going to do! I called the company
that sent and th-they don<U+0092>t care! Then I called 9-1-1 and they laughed at me, if this
isn<U+0092>t an emergency, then what is?

Ross: (entering) Hey guys!

Chandler: Hey!

Joey: Hey<U+0085>

Ross: I just wanted to thank you again for last night, what a great party! And
the guys from work had a blast. Y<U+0092>know, one of them had never been to a bachelor
party before. Yeah! And-and another one had never been to a party before, so<U+0085>

Joey: So uh, hey, that uh, that wedding ring, huh? Man, that<U+0092>s nice!

Ross: Yeah, right!

Joey: I was uh, I was thinking I might want to pick one of those babies up for
myself, I might want to get one of those<U+0085>

Ross: That ring? When my grandmother first came to this country, that ring and
the clothes on her back were all she had with her.

Chandler: So you might say, the ring is irreplaceable? (Gives Joey a little
squeeze.)

Ross: Oh absolutely! It has been in my family for generations, and every bride
who has worn it has had a long and happy life.

Chandler: So you might say, it<U+0092>s a magic ring.

Joey: (laughs, softly) Yeah, the stripper stole it.

Ross: My-my ring? My-my wedding ring? The-the stripper stole my wedding ring?!
H-how?! How could this all happen?!

Chandler: Well, I think it all started when you said, "Hey Joey, why
don<U+0092>t you be my best man."

Ross: (dialling the phone) All right-all right, fine! I-I<U+0092>m gonna call the
cops!

Joey: Dude, I screwed up, you don<U+0092>t have to turn me in!

Ross: Not on you! On the stripper!

Joey: Oh, yeah, well I already did that! They said they<U+0092>re gonna look into
it right after they solved all the murders.

Ross: Okay, well, we<U+0092>ll call the company that sent her!

Joey: I did that too! They wouldn<U+0092>t give me her real name or her number.
They said, "If I bothered them again they<U+0092>d call the police." I said,
"If you talk to the police, you tell them I<U+0092>m missing a ring!"

Ross: So what, Joey? Wh-wh-what? What are you telling me? That there<U+0092>s
nothing we can do? Well, how could this happen?!

Joey: Look Ross, I am so-so sorry. I-I-I<U+0085>

Chandler: Well, what if we just ah, called her, used a fake name, and had her
come to my office?

Joey: Oh, that sounds like fun, but we<U+0092>ve got a ring to find!!

[Scene: Monica and Rachel's, Monica and Rachel are cautiously serving Phoebe some tea.]

Monica: Here<U+0092>s your tea Phoebe. (They give it to her and quickly take a
step back.)

Phoebe: (sips it) It<U+0092>s so good. (Monica and Rachel breath a sigh of
relief.) Oh, thanks.

Rachel: Good.

Monica: I<U+0092>m so glad you liked it.

Phoebe: (sets the cup down) Oh! (Grabs her stomach in pain.)

Monica: What?!

Rachel: What?! She made the tea! (Points to Monica.)

Phoebe: Oh! No, I-I think I just had a contraction.

Rachel: You what?

Monica: Oh my God!

Phoebe: Yeah, I thought I had one a couple of minutes ago, and now I know that
was definitely one.

Monica: Wait, you can<U+0092>t have the baby here! I mean I haven<U+0092>t
sterilised it since the guys moved out!

Rachel: Okay. It<U+0092>s okay. We<U+0092>re gonna be okay. Y<U+0092>know what?
It<U+0092>s okay. I<U+0092>m gonna, I<U+0092>m gonna, I<U+0092>m gonna boil some water and just
rip up some sheets!

Phoebe: No. It<U+0092>s all right; it<U+0092>s probably false labour. They said
that, that can happen near the end, just somebody get the book.

Monica: Rachel, get the book! The book!

Rachel: Okay! (Runs and grabs a book and hands it to Monica.) Okay! Here!

Monica: The Bible?!

Rachel: I don<U+0092>t know!

[Scene: Chandler<U+0092>s office, the guys are there waiting to ambush the stripper.]

Joey: All right, okay, this is great, uh, Chandler, you get behind the desk.
And-and when she comes in hopefully, she won<U+0092>t recognise you because, well, why would
she? Uh, okay, and then you buzz Ross and I. (to Ross) You be Mr. Gonzalez, and I<U+0092>ll
be uh, Mr. Wong.

Ross: Diverse.

(There<U+0092>s a knock on the door.)

The Stripper: Did anybody call for security?

Chandler: (to Ross) You be cool. (He opens the door and lets her in as they all
turn there backs on her.)

The Stripper: Okay, which one of you guys is Gunther Central-Perk? (Sees Joey.)
Hey, Joey?

Ross: Where<U+0092>s my ring? My dead grandmother<U+0092>s wedding ring? Where is
it? Where is it?

Chandler: Way to be cool, man.

The Stripper: What<U+0092>s he talking about?

Joey: There was a ring, in a box, on my nightstand, after you left, it was gone!

The Stripper: Wait, you guys think I stole some ring?


The Guys: Yeah!

Ross: We know you took so just-just save yourself the time and confess!

The Stripper: Okay, who are you? The Hardy boys? Look, I don<U+0092>t need to
steal some stupid ring, all right? I make $1,600 a week doing what I do; any of you guys
make that?

Chandler: Marry me. (Both Ross and Chandler hit him.)

[Scene: Chandler and Joey's, the guys are now trying to figure out what next to do,
since their plan with the stripper backfired on them.]

Joey: I don<U+0092>t get it! It was in my room all night! And if she didn<U+0092>t
take it, and I didn<U+0092>t take it; and you (Chandler) didn<U+0092>t take it, then who did?
(The duck quacks.) Shh! We<U+0092>re trying to think! (Ross and Chandler realise it at the
same moment and stare at Joey, who doesn<U+0092>t get it. After a short pause, with the duck
still quacking, Joey figures it out and starts pointing at the duck.)

[Scene: Monica and Rachel's, Phoebe is recovering from her false labour.]

Rachel: I still don<U+0092>t get how you know when it<U+0092>s false labour.

Phoebe: Well, do you see any babies?

Monica: How do you feel?

Phoebe: Okay, I guess. I mean<U+0085> I don<U+0092>t know, it<U+0092>s just, I guess I
know it<U+0092>s going to be over soon.

Rachel: Well, isn<U+0092>t that a good thing? You said you were sick of this.

Phoebe: I know. It<U+0092>s just y<U+0092>know usually when you<U+0092>re, when
you<U+0092>re done with the pregnant thing, y<U+0092>know, then you get to do the mom thing.
I<U+0092>m gonna be y<U+0092>know, sitting around in my leather pants, drinking Tequila.

Monica: Some moms do that.

Phoebe: Okay that<U+0092>s even sadder. Look, I know, I know what I got myself
into, it<U+0092>s just that now that they<U+0092>re in me it<U+0092>s like, it<U+0092>s like I
know them y<U+0092>know, I mean-I mean, it<U+0092>s just not gonna be easy when these little
babies have to go away.

Monica: Aww, sweetie, but it<U+0092>s not like you<U+0092>re not gonna have
anything. You<U+0092>re gonna have nieces and nephews, and some ways that<U+0092>s even
better.

Phoebe: Yeah, okay.

Rachel: No, really. Really, Pheebs, you<U+0092>re not gonna be the one worrying
about saving for college, or yelling at them when they<U+0092>re bad, y<U+0092>know, or
deciding to put them on Ritalin when they just won<U+0092>t calm down. Y<U+0092>know?

Monica: I mean, you<U+0092>re the one they<U+0092>re gonna come to when they wanna
run away from home, and the one they talk to about sex.

Rachel: And you just get to be cool Aunt Phoebe!

Phoebe: Cool Aunt Phoebe. I am pretty cool!

Monica: Yeah.

Rachel: And y<U+0092>know what else, oh my God, are they gonna love you.

Phoebe: They are gonna love me.

Rachel: Oh!

Phoebe: Thanks you guys! Again.

Monica: Oh, sweetie! (They all hug.)

Phoebe: You<U+0092>re the best. Thanks. Oh! 


Monica and Rachel: What?!

Phoebe: Just kidding. Ahh!

Rachel: What?!

Monica: Oh my God!

Phoebe: Got cha again, you guys are so easy.

[Scene: The Animal Hospital, the guys have taken the duck in to remove the ring. Joey
is pacing around like an expectant father.]

Joey: If anything should happen to him<U+0085>

Ross: Joey! The vet said it<U+0092>s a simple procedure.

Joey: So! Things can go wrong! You don<U+0092>t know! What if he doesn<U+0092>t make
it?!

Chandler: He will, Joe.

Joey: Yeah, but what if he doesn<U+0092>t? He<U+0092>s such a good duck.

(With that we go into a little flashback about the guys<U+0092> memories of the duck. The
first one is Joey playing with him in the bathtub and drying him off. Then it<U+0092>s
Chandler sitting on his couch after they moved into the girls apartment, and Chandler
reading to him in bed, and him watching Baywatch when all they had was the canoe
and the duck was in a bucket of water. Then we see Ross eating some cereal and the duck
watching him. He takes a lamp and moves the duck off of the table. Then it<U+0092>s Chandler
shooing them out of the bathroom in the girls<U+0092> apartment, Joey revealing their disco
cubby hole in the entertainment-center, then Chandler playing Hide-and-Go-Seek with them,
and it<U+0092>s concluded with various scenes with the duck flapping it<U+0092>s wings. And
the guys staring into the distance in remembrance of the duck.)

Joey: I<U+0092>m so worried about him, y<U+0092>know?

The Doctor: (coming in from surgery) Somebody lose a ring?

Ross: Oh my God! Thank you! Thank you so much! (He grabs the ring, kisses it,
and then does a double-take realising where it<U+0092>s been.)

Joey: H-h-h-how<U+0092>s the duck?

The Doctor: He<U+0092>s doing just fine, he<U+0092>s resting now, but you can see
him in a little bit.

Joey: Ohh, great! Oh hey, listen Ross, thanks for being so cool about this.

Ross: No, that<U+0092>s all right.

Joey: No, it<U+0092>s not. I mean you-you made me your best man and I totally let
you down!

Chandler: Hey, come on, it<U+0092>s not your fault.

Joey: Yeah, it is! You wouldn<U+0092>t have lost the ring, right? Y<U+0092>know
what, Ross you were right from the start, he (Chandler) should be your best man.

Chandler: No, you should.

Joey: Now, don<U+0092>t argue with me<U+0085>

Ross: Hey! Hey! Hey! I get to choose my best man, and I want both you
guys. 

Chandler: Really?

Joey: Really?

Ross: Hey, both you guys should be up there with me. I mean, you two are-are
my<U+0085> I mean, I<U+0092>m lucky to have just one good<U+0085> (They all start getting
emotional.)

Chandler: Thanks man.

Joey: (starting to cry) I gotta go check something over here. (He walks away so
that they can<U+0092>t see him cry.)

Chandler: What a baby.

Ross: Total wuss!

(They both turn and wipe their eyes.)

Closing Credits

[There was no closing scene, only a preview of the wedding.]

End

