

The One Where Rachel Quits 

Written by: Michael Curtis and Gregory S. Malins
Transcribed by: Eric Aasen 



[Scene: Central Perk, Chandler and Rachel are sitting on the couch.]

Chandler: (reading the comics) Eh..., I don<U+0092>t, I don<U+0092>t know.

Rachel: What?

Chandler: Well, as old as he is in dog years, do you think Snoopy should still
be allowed to fly this thing?

Gunther: Rachel?

Rachel: Yeah.

Gunther: Do you remember when you first came here, how you spent two weeks
getting trained by another waitress?

Rachel: Oh, sure! Do you need me to train somebody new?

Gunther: (laughs) Good one. Actually, ah, Terry wants you to take the training
again, whenever.

Rachel: (to Chandler) Eh, do you believe that?

Chandler: (thinks about it) Yeah?

Opening Credits

[Scene: The hallway of Ross<U+0092>s building, there is a Brown Bird girl selling
cookies, as Ross and Chandler come up the stairs.]

Sarah: So that<U+0092>s two boxes of the Holiday Macaroons. On behalf of the Brown
Birds of America, I salute you. (Does the Brown Bird salute, she blows on a bird call,
then holds her hand, palm facing out, next to her face, and then waves it like a bird
flapping it<U+0092>s wings.)

Ross: Just admit it Chandler, you have no backhand.

Chandler: Excuse me little one, I have a very solid backhand.

Ross: Shielding your face and shrieking like a girl... is not a backhand.

Chandler: I was shrieking... like a Marine.

(they both start up the stairs.)

Ross: All right here. Watch me execute the three <U+0091>P<U+0092>s of championship
play. Power. (swings the racquet) Precision. (swings the racquet.) and penache. (does a
backswing and hits Sarah who<U+0092>s started up the stairs, knocking her down, they both
watch in horror.)

[Scene: Central Perk, the gang<U+0092>s all there discussing the incident.]

Monica: You broke a little girl<U+0092>s leg?!!

Ross: I know. I feel horrible. Okay.

Chandler: (reading the paper) Says here that a muppet got whacked on Seasame
Street last night. (to Ross) Where exactly were around ten-ish?

Ross: Well, I<U+0092>m gonna go see her. I want to bring her something, what do
you think she<U+0092>ll like?

Monica: Maybe a Hello Kitty doll, the ability to walk...

(Rachel starts to laugh, and Ross notices her.)

Rachel: I<U+0092>m gonna get back to retraining. (gets up)

Ross: All right, see you guys. (starts to leave)

Chandler: Look out kids, he<U+0092>s coming! (Ross continues to leave with his
head down in shame.)

Joey: And I gotta go sell some Christmas trees.

Phoebe: Have fun. Oh wait, no, don<U+0092>t! I forgot I am totally against that
now.

Joey: What? Me having a job?

Phoebe: No, no, I am against innocent trees being cut down in their prime, and
their, their corpses grotesquely dressed in like tinsel and twinkly lights. (to Joey) Hey,
how do you sleep at night?

Joey: Well, I<U+0092>m pretty tired from lugging the trees around all day. Hey,
Phoebe listen, you got this all wrong. Those trees were born to be Christmas trees, their
fulfilling their life purpose, by, by making people happy.

Phoebe: Really?

(Phoebe turns and looks at Monica, while Joey frantically motions to Chandler to help
him out.)

Chandler: Yes. Yes, and ah, ah, the trees are happy too, because for most of
them, it<U+0092>s the only chance to see New York.

[cut to Gunther retraining Rachel.]

Gunther: ...and after you<U+0092>ve delivered the drinks, you take the empty
tray....

Rachel: Gunther, Gunther, please, I<U+0092>ve worked here for two and a half
years, I know the empty trays go over there. (points to the counter.)

Gunther: What if you put them here. (sets the empty tray on another stack of
empty trays on the back counter.)

Rachel: Huh. Well, y'know that<U+0092>s actually a really good idea, because that
way they<U+0092>ll be closer to the mugs. Y'know what, you should have the other waitresses
do that too.

Gunther: They already do. That<U+0092>s why they call it the <U+0091>tray
spot.<U+0092>

Rachel: Gee, I always heard them talk about that, I just always thought that it
was a club they went to. Oh God, I<U+0092>m, I<U+0092>m sorry. (walks away)

Gunther: It<U+0092>s all right. Sweetheart.

[Scene: Sarah<U+0092>s bedroom, her room is decorated with a space motif.]

Ross: So, this must be kinda neat for ya, huh? I mean, your Dad tells me that
you get a couple of days off school, and you, you ah, don<U+0092>t have to sell those
cookies anymore. 

Sarah: Well, I kinda wanted to sell the cookies. The girl who sells the most
wins a trip to Spacecamp, and gets to sit in a real space shuttle.

Ross: Wow, you ah, you really like all this space stuff, huh?

Sarah: Yeah. My Dad says if I spend as much time helping him clean apartments,
as I do daydreaming about outer space, he<U+0092>d be able to afford a trip to the Taj
Mahal.

Ross: I think you would have to clean a whole lot of apartments to go all the
way to India.

Sarah: No. The one in Atlantic City, Dad loves the slots. He says he<U+0092>s
gonna double the college money my Grandma left me.

Ross: Huh. Well, good luck to Dad. Say, how many more boxes would you have to
sell in order to win?

Sarah: The girl who won last year sold four hundred and seventy-five.

Ross: Yeah.

Sarah: So far, I<U+0092>ve sold seventy-five.

Ross: Four hundred, huh? Well, that sounds do-able. (starts to get out his
wallet) How much are the boxes?

Sarah: Five dollars a box.

Ross: (puts away his wallet) And what is second prize?

Sarah: A ten speed bike. But, I<U+0092>d rather have something my Dad
couldn<U+0092>t sell.

Ross: Well, that makes sense.

Sarah: Could you do me one favor, if it<U+0092>s not too much trouble?

Ross: Yeah, Sarah, anything.

Sarah: Could you pull open the curtains for me? The astronauts from the space
shuttle are gonna be on the news, and since we don<U+0092>t have a TV, the lady across the
alley said she<U+0092>d push hers up to a window, so I could watch it.

[Scene: A hallway, Ross is selling Brown Bird cookies for Sarah, he stops and knocks on
a door.]

Woman: (looking through her peephole, we see Ross standing in the hallway.)
Yesss?

Ross: Hi, I<U+0092>m selling Brown Bird cookies.

Woman: You<U+0092>re no Brown Bird, I can see you through my peephole.

Ross: No, hi, I<U+0092>m, I<U+0092>m an honorary Brown Bird (does the Brown Bird
salute.)

Woman: What does that mean?

Ross: Ah, well, it means that I can sell cookies, but I<U+0092>m not invited to
sleep-overs.

Woman: I can dial 9-1-1 at the touch of a button, y'know. Now, go away!

Ross: No, please, please, um, it<U+0092>s for a poor little girl who wants to go
to Spacecamp more than anything in the world.

Woman: I<U+0092>m pressing, a policeman is on his way.

Ross: Okay, okay! I<U+0092>m going. I<U+0092>m going. (goes across the hall to knock
on another door.)

Woman: I can still see you!

Ross: All right!!

[Scene: Joey<U+0092>s work, selling Christmas trees.]

Phoebe: (walking up to Joey) Hey.

Joey: Hey. What, what are you doing here?

Phoebe: Well, I-I thought a lot about what you said, and um, I realilized duh,
all right maybe I was a little judgmental. Yeah, (looks at the tree) oh, but oh...

Joey: Look now, Phoebe remember, hey, their just fulfilling their Christmas....

Phoebe: Destiny.

Joey: Sure.

Phoebe: Yes.

Joey: All right.

Phoebe: Okay. (One of Joey<U+0092>s co-workers, walks by with a dead tree.) Yikes!
That one doesn<U+0092>t look very fulfilled.

Joey: Oh, that<U+0092>s, that<U+0092>s ah, one of the old ones, he<U+0092>s just
taking it to the back.

Phoebe: You keep the old ones in the back, that is so ageist.

Joey: Well we have to make room for the fresh ones.

Phoebe: So, what happens to the old guys?

Joey: Well, they go into the chipper.

Phoebe: Why, do I have a feeling that<U+0092>s not as happy as it sounds? (Joey
points out one going into the chipper to her, as this haunty, demonic music starts to play
in the background) No! Nooooo!!! (she winces in horror and hides her face against
Joey<U+0092>s shoulder, as she sees the tree spit out from the chipper.)

Joey: (to the guy operating the chipper) Hey! Hey!! (makes the <U+0091>cut
it<U+0092> motion with his hands)

[Scene: Central Perk, all except Phoebe are there, Ross is telling the gang, minus
Rachel who<U+0092>s still being retrained, about the different cookie options.]

Ross: ...and these come in the shapes of your favourite Christmas characters,
Santa, Rudolph, and Baby Jesus.

Joey: All right, I<U+0092>ll take a box of the cream filled Jesus<U+0092>s.

Ross: Wait a minute, one box! Come on, I<U+0092>m trying to send a little girl to
Spacecamp, I<U+0092>m putting you down for five boxes. Chandler, what about you?

Chandler: Ahh, do you have any coconut flavoured deities?

Ross: No, but ah, there<U+0092>s coconut in the Hanukkah Menoreoes. I tell you
what, I<U+0092>ll put you down for eight boxes, one for each night.

(Chandler mouths <U+0091>Okay.<U+0092>)

Ross: Mon?

Monica: All right, I<U+0092>ll take one box of the mint treasures, just one, and
that<U+0092>s it. I-I started gaining weight after I joined the Brown Birds. (to Ross)
Remember, how Dad bought all my boxes and I ate them all?

Ross: Ah, no Mon, Dad had to buy everyone of your boxes because
you ate them all. But ah, y'know I<U+0092>m sure that<U+0092>s not gonna happen this time, why
don<U+0092>t I put you down for three of the mint treasures and just a couple of the
Rudolph<U+0092>s.

Monica: No.

Ross: Oh, come on, now you know you want <U+0091>em.

Monica: Don<U+0092>t, don<U+0092>t, don<U+0092>t, don<U+0092>t, don<U+0092>t do this.

Ross: I<U+0092>ll tell you what Mon, I<U+0092>ll give you the first box for free.

Monica: (she reaches out for it and stops) Oh God! I gotta go! (runs out)

Ross: Come on! All the cool kids are eating <U+0091>em! (chases after her.)

[cut to Gunther retraining Rachel.]

Gunther: And when you have a second later, I wanna show you why we don<U+0092>t
just trap spiders under coffee mugs and leave them there.

Rachel: (sitting down next to Chandler) I<U+0092>m training to be better at a job
that I hate, my life officially sucks.

Joey: Look Rach, wasn<U+0092>t this supposed to a temporary thing? I thought you
wanted to do fashion stuff?

Rachel: Well, yeah! I<U+0092>m still pursuing that.

Chandler: How... exactly are you pursuing that? Y'know other than sending out
resumes like what, two years ago?

Rachel: Well, I<U+0092>m also sending out.... good thoughts.

Joey: If you ask me, as long as you got this job, you<U+0092>ve got nothing
pushing you to get another one. You need the fear.

Rachel: The fear?

Chandler: He<U+0092>s right, if you quit this job, you then have motivation to go
after a job you really want.

Rachel: Well then how come you<U+0092>re still at a job that you hate, I mean why
don<U+0092>t you quit and get <U+0091>the fear<U+0092>?

(Chandler and Joey both laugh)

Chandler: Because, I<U+0092>m too afraid.

Rachel: I don<U+0092>t know, I mean I would give anything to work for a designer,
y'know, or a buyer.... Oh, I just don<U+0092>t want to be 30 and still work here.

Chandler: Yeah, that<U+0092>d be much worse than being 28, and still
working here.

Gunther: Rachel?

Rachel: Yeah.

Gunther: Remind me to review with you which pot is decaf and which is regular.

Rachel: Can<U+0092>t I just look at the handles on them?

Gunther: You would think.

Rachel: Okay, fine. Gunther, y'know what, I am a terrible waitress, do you know
why I<U+0092>m a terrible waitress? Because, I don<U+0092>t care. I don<U+0092>t care. I
don<U+0092>t care which pot is regular and which pot is decaf, I don<U+0092>t care where the
tray spot is, I just don<U+0092>t care, this is not what I want to do. So I don<U+0092>t think
I should do it anymore. I<U+0092>m gonna give you my weeks notice.

Gunther: What?!

Rachel: Gunther, I quit.

Chandler: (to Joey) Does this mean we<U+0092>re gonna have to start paying for
coffee? (Joey shrugs his shoulders.)

Commercial Break

[Scene: Monica and Rachel's, Chandler is entering numbers on a calculator as Ross reads
off how much he<U+0092>s sold.]

Ross: ....and 12, 22, 18, four... (Chandler starts laughing) What?

Chandler: I spelled out boobies.

Monica: (comes up and starts looking through Ross<U+0092>s cookie supply) Ross,
but me down for another box of the mint treasures, okay. Where, where are the mint
treasures?

Ross: Ah, we<U+0092>re out. I sold them all.

Monica: What?

Ross: Monica, I<U+0092>m cutting you off.

Monica: No. No, just, just, just a couple more boxes. It-it-it<U+0092>s no big
deal, all right, I<U+0092>m-I<U+0092>m cool. You gotta help me out with a couple more boxes!

Ross: Mon, look at yourself. You have cookie on your neck.

Monica: (covers her neck) Oh God! (runs to the bathroom)

Chandler: So, how many have you sold so far?

Ross: Check this out. Five hundred and seventeen boxes!

Chandler: Oh my God, how did you do that?

Ross: Okay, the other night I was leaving the museum just as <U+0091>Laser
Floyd<U+0092> was letting out of the planetarium, without even trying I sold 50 boxes!
That<U+0092>s when it occurred to me, the key to my success, <U+0091>the munchies.<U+0092> So I
ah, started hitting the NYU dorms around midnight. I am selling cookies by the case. They
call me: 'Cookie Dude!'

Rachel: (entering) Okay, stop what you<U+0092>re doing, I need envelope stuffers,
I need stamp lickers.....

Ross: Well hey, who did these resumes for ya?

Chandler: Me! On my computer.

Ross: Well you sure used a large font.

Chandler: Eh, yeah, well ah, waitress at a coffee shop and cheer squad
co-captain only took up so much room.

Rachel: Hey-hey-hey that<U+0092>s funny! Your funny Chandler! Your a funny guy!
You wanna know what else is really funny?!

Chandler: Something else I might have said?

Rachel: I don<U+0092>t know, I don<U+0092>t know, weren<U+0092>t you the guy that told
me to quit my job when I had absolutely nothing else to do. Ha! Ha! Ha! Ha! Ha!!

Ross: Sweetie, calm down, it<U+0092>s gonna be okay.

Rachel: No, it<U+0092>s not gonna be okay Ross, tomorrow is my last day, and I
don<U+0092>t have a lead. Okay, y'know what, I<U+0092>m just gonna, I<U+0092>m just gonna call
Gunther and I<U+0092>m gonna tell him, I<U+0092>m not quitting.

Chandler: You-you-you don<U+0092>t wanna give into the fear.

Rachel: You and your stupid fear. I hate your fear. I would like to take you and
your fear.... 

Joey: (entering, interrupting Rachel) Hey! I got great news!

Chandler: Run, Joey! Run for your life! (runs out)

Joey: What? Rachel, listen, have you ever heard of Fortunata Fashions?

Rachel: No.

Joey: Well my old man is doing a plumbing job down there and he heard they have
an opening. So, you want me to see if I can get you an interview?

Rachel: Oh my God! Yes, I would love that, oh, that is soo sweet, Joey.

Joey: Not a problem.

Rachel: Thanks.

Joey: And now for the great news.

Ross: What, that wasn<U+0092>t the great news?

Joey: Only if you think it<U+0092>s better than this... (holds up an aerosol can)
snow-in-a-can!! I got it at work. Mon, you want me to decorate the window, give it a kind
of Christmas lookie.

Monica: Christmas cookie?

[Scene: Joey work, Joey is showing a guy a tree.]

Joey: Okay, and ah, this one here is a Douglas Fir, now it<U+0092>s a little more
money, but you get a nicer smell.

Guy: Looks good. I<U+0092>ll take it.

Phoebe: (running up carrying a tree) Wait, wait, wait, wait, wait! No, no, you
don<U+0092>t want that one. No, you can have this cool brown one. (points to the almost dead
tree she has)

Guy: It<U+0092>s-it<U+0092>s-it<U+0092>s almost dead!

Phoebe: Okay but that<U+0092>s why you have to buy it, so it can fulfil it<U+0092>s
Christmas destiny, otherwise there gonna throw it into the chipper. Tell him, Joey

Joey: Yeah, the ah, trees that don<U+0092>t fulfil their Christmas destiny are
thrown in the chipper.

Guy: I-I think I<U+0092>m gonna look around a little bit more.

Joey: Pheebs, you gotta stop this, I working on commission here.

Monica: (entering) Hey, guys. I<U+0092>m here to pick out my Christmas tree.

Phoebe: Well look no further, (shows her the dead one) this one<U+0092>s yours!
Ahhh.

Monica: Is this the one that I threw out last year?

Phoebe: All right y'know what, nevermind! Everyone wants to have a green one!
I<U+0092>m sorry, I<U+0092>m sorry, I didn<U+0092>t mean to get so emotional, I guess it<U+0092>s
just the holidays, it<U+0092>s hard.

Monica: Oh honey, is that <U+0091>cause your Mom died around Christmas?

Phoebe: Oh, I wasn<U+0092>t even thinking about that.

Monica: Oh. (turns and looks at Joey, who gives a <U+0091>way-to-go<U+0092> thumbs
up and smile.)

[Scene: A Brown Bird meeting, Ross is there with the other Brown Birds to see who won
the contest.]

Ross: (to the girl sitting next to him) Hi there. How many, how many ah, did you
sell?

Girl: I<U+0092>m not gonna tell you! You<U+0092>re the bad man who broke
Sarah<U+0092>s leg.

Ross: Hey now! That was an accident, okay.

Girl: You<U+0092>re a big scrud.

Ross: What<U+0092>s a scrud?

Girl: Why don<U+0092>t you look in the mirror, scrud.

Ross: I don<U+0092>t have too. I can just look at you.

Leader: All right girls, and man. Let<U+0092>s see your final tallies. (all the
girls raise their hands) Ohhhh, Debbie, (looks at her form) 321 boxes of cookies, (to
Debbie) Very nice.

Ross: (to himself) Not nice enough.

Leader: Charla, 278. Sorry, dear, but still good.

Ross: (to himself) Good for a scrud.

Leader: Oh, yes Elizabeth. Ah, 871.

Ross: That<U+0092>s crap!! Sister Brown Bird. (to Elizabeth) Good going. (does the
salute)

Leader: Who<U+0092>s next? (goes over and stands behind Ross, who<U+0092>s
feverishly writing on his form, and clears her throat to get his attention.)

Ross: Hi there!

Leader: Hi. And batting for Sarah, Ross Geller, 872. Although, it looks like you
bought an awful lot of cookies yourself.

Ross: Um, that is because my doctor says that I have a very serious....
nuget.... diffency.

[Scene: Central Perk, Chandler, Phoebe, and Ross are there.]

Chandler: Tell us what happened, Brown Bird Ross.

Ross: Well, I lost. Some little girl loaned her uniform to her nineteen year old
sister, who went down to the U.S.S. Nimitz, and sold over 2,000 boxes.

Chandler: (to Rachel, who<U+0092>s entering) Hey! How<U+0092>d the interview go?

Rachel: Oh, I blew it. I wouldn<U+0092>t of even hired me.

Ross: Oh, come here sweetie, listen, you<U+0092>re gonna go on like a thousand
interviews before you get a job. (she glares at him) That<U+0092>s not how that was supposed
to come out.

Phoebe: This is the worst Christmas ever.

Chandler: Y'know what Rach, maybe you should just, y'know stay here at the
coffee house.

Rachel: I can<U+0092>t! It<U+0092>s too late! Terry already hired that girl over
there. (points to her) Look at her, she<U+0092>s even got waitress experience. Last night
she was teaching everybody how to make napkin.... (starts to cry) swans.

Ross: That word was swans.

[Scene: The hallway between the two apartments, Chandler, Phoebe, Ross, and Rachel are
coming up the stairs.]

Chandler: Well seeing that drunk Santa wet himself, really perked up my
Christmas.

(They start to go into Monica and Rachel<U+0092>s, their apartment is filled with all of
the old Christmas trees from Joey<U+0092>s work.)

Phoebe: Oh! Oh my God!

Joey and Monica: (jumping up from behind the couch) Merry Christmas!!

Phoebe: You saved them! You guys! Oh God, you<U+0092>re the best!

Chandler: It<U+0092>s like <U+0091>Night of the Living Dead Christmas Trees.<U+0092>

(phone rings)

Rachel: (answering the phone) Hello? (listens) Yeah, this is she. (listens) Oh!
You<U+0092>re kidding! You<U+0092>re kidding! (listens) Oh thank you! I love you!

Chandler: Sure, everybody loves a kidder.

Rachel: (hanging up the phone) I got the job!

All: That<U+0092>s great! Hey! Excellent!

Phoebe: Oh, God bless us, everyone.

[Scene: Central Perk, Rachel is serving her last cup of coffee.]

Rachel: Here we go. I<U+0092>m serving my last cup of coffee. (the gang starts
humming the graduation theme) There you go. (hands it to Chandler) Enjoy. (they all cheer)

Chandler: (to Ross) Should I tell her I ordered tea?

Ross: No. 

Rachel: Um, excuse me, everyone. Ah, this is my last night working here, and I
ah, just wanted say that I made some really good friends working here, and ah, it<U+0092>s
just time to move on. (at the counter Gunther starts to cry and runs into the back room)
Ah, and no offence to everybody who ah, still works here, you have no idea how good it
feels to say that as of this moment I will never have to make coffee again.

[Scene: Rachel<U+0092>s new job, Rachel<U+0092>s boss is telling her what to do.]

Rachel<U+0092>s Boss: Now Mr. Kaplan Sr. likes his coffee strong, so your gonna
use two bags instead of one, see. Now pay attention, <U+0091>cause this part<U+0092>s tricky,
see some people use filters just once.

Closing Credits

[Scene: The hallway between the apartments, Ross is bringing Sarah to Joey and
Chandler<U+0092>s.]

Ross: I<U+0092>m, I<U+0092>m sorry you didn<U+0092>t get to go to Spacecamp, and
I<U+0092>m hoping that maybe somehow, this may make up for it. Presenting Sarah
Tuttle<U+0092>s Private Very Special Spacecamp!! (opens the door and Chandler and Joey jump
up, their apartment is decorated like outer space, one of the leather chairs is covered in
tinfoil.)

Sarah: Really Mr. Geller, you don<U+0092>t have to do this.

Ross: Oh come on! Here we go! (picks her up and puts her in the chair) Stand by
for mission countdown!

Joey: (simulating an echo) Ten, ten.., nine, nine, nine....,
eight, eight, eight... (Chandler hits him in the back of the head) Okay, Blast off!

(They start shaking the chair likes it<U+0092>s flying into outer space. Ross picks up a
soccer ball and starts spinning it in his hand and runs around the chair beeping like a
satellite. Chandler also starts running around the chair and saying...)

Chandler: I<U+0092>m an alien. I<U+0092>m an alien.

Ross: Oh no! An asteroid! (throws the soccer ball off the back of Joey<U+0092>s
head.)

(The camera zooms in on Sarah and she has a big smile on her face.)

End


