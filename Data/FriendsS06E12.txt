

The One With The Joke

Teleplay by: Andrew Reich & Ted Cohen
Story by: Shana Goldberg-Meehan
Transcribed by: Eric Aasen

[Scene: Central Perk, Chandler, Phoebe, Rachel and Monica are there. Ross walks in with
a magazine in his hand.]

Ross: Hey, you<U+0092>re not going to believe this. I made up a joke and sent it
in to Playboy. They printed it!

Phoebe: I didn<U+0092>t know Playboy prints jokes.

Ross: Yeah, they print jokes, interviews, hard-hitting journalism. It<U+0092>s not
just about the pictures.

Monica: That didn<U+0092>t work on mom, it<U+0092>s not going to work on us.

Ross: (showing them the page) Here, check it out. It<U+0092>s the first
one, too.

(They all laugh indifferently, except Chandler, who<U+0092>s a little angry.)

Chandler: That is funny. It was also funny when I made it up.

Ross: What?

Chandler: I made that joke up.

Ross: Uh, oh-oh, no you didn<U+0092>t. I did.

Chandler: Yes, I did. I told it to Dan at work, and he said it was the funniest
joke he<U+0092>d ever heard.

Ross: Hey, tell Dan, <U+0091>Thanks.<U+0092>

(Rachel is looking at the magazine and laughing.)

Ross: What?

Rachel: I<U+0092>m sorry, I was just reading the joke below it. Man, that one is
funny. (Ross grabs the magazine away from her.)

Chandler: Monica, you remember me telling you that joke, right?

Monica: No.

Chandler: Seriously?

Monica: Well, you tell a lot of jokes!

Ross: Look, Chandler, it<U+0092>s my joke. But, hey, if it makes you feel any
better they don<U+0092>t print the name, so it doesn<U+0092>t really matter who gets credit,
right?

Chandler: Yeah, I guess.

Joey: (entering) Hey guys.

Chandler: (jumping up from his chair) Hey, Joey, Playboy
printed my joke.

Ross: No, it<U+0092>s my joke, it<U+0092>s mine. You can call them, they<U+0092>ll
tell you.

Chandler: It<U+0092>s my joke.

Ross: It<U+0092>s my joke.

Joey: Whoa-whoa-whoa. Jokes? You guys know they have naked chicks in there,
right?

Opening Credits

[Scene: Joey<U+0092>s apartment, Joey is sitting at the counter as Chandler enters.]

Chandler: Dude, you have got to turn on Behind the Music. The band Heart
is having a really tough time, and I think they may break up.

Joey: Let<U+0092>s go watch it at your place.

Chandler: Nah, Monica<U+0092>s watching some cooking show. Come on, I don<U+0092>t
want to miss when they were skinny.

Joey: Chandler, Chandler, y<U+0092>know what we should do? You and I should go out
and get some new sunglasses.

Chandler: What? No, I want to watch this. (He turns on the television and the
screen is completely covered in snow). Did your cable go out?

Joey: No, that<U+0092>s VH-1. I gotta tell you, the music these kids listen
to today . . . It<U+0092>s like a lotta noise to me. I don<U+0092>t know<U+0085>

Chandler: Joey, why is your cable out?

Joey: I uh, oh! Because, uh, I haven<U+0092>t really paid the bill

Chandler: If you need money, will you please-please just let me loan you some
money?

Joey: No, Chandler. Look, forget about it, okay? Look, I know things have been a
little tight since Janine moved out. Oh, was she hot. 

Chandler: Whoa ho.

Joey: I know! Yeah, but, look I can handle it. All right? Look, I can listen to
the radio, huh? And Ross gave me this great book (holds up the Playboy magazine).

Chandler: (picks up the phone) All right, you want to see if the joke stealer
will let us watch the show at his place?

Joey: Sure.

Chandler: (with phone to ear, obviously hearing no dial tone) Paid your phone
bill?

Joey: Not so much.

[Scene: Monica and Chandler's, Monica and Rachel are on the couch looking at the Playboy
magazine. When they hear someone coming, Monica goes to hide it under the sofa cushions.] 

Phoebe: (entering) Hey.

Monica: (relieved) It<U+0092>s only you.

Phoebe: Wh-wh-what are you doing?

Rachel: We are looking at a Playboy.

Phoebe: Oh, I want to look too! (She runs over and sits down and checks out a
picture). Yikes!

Monica: So do you think that these pictures<U+0097>Are, are they trying to tell a
story?

Rachel: Oh, yeah, sure. I mean, like in the case of this young woman, she has
lost her clothes, so she rides naked on the horse, she<U+0092>s crying out, <U+0091>Where are
they, where are they?<U+0092>

Monica: Well, she<U+0092>s not going to find them lying in the grass like that.

(They flip through the pages to another picture.)

Phoebe: Oh, yeah. Aw, remember the days when you used to go out to the barn,
lift up your shirt, and bend over?


Rachel and Monica: Yeah.

Rachel: You see, now, I would date this girl. She<U+0092>s cute, she<U+0092>s
outdoorsy, you know, and she knows how to build a fire. I mean, that<U+0092>s got to come in
handy

Monica: Okay, I<U+0092>ve got a question. If you had to pick one of us to date,
who would it be?

Rachel: (thinks) I don<U+0092>t know.

Monica: Me neither.

Phoebe: Rachel.

Monica: What?!

Phoebe: I don<U+0092>t know. (Pause) Me neither.

[Scene: Central Perk, Joey, Chandler, and Ross are sitting on the couch.]

Joey: You forget how many great songs Heart had.

Chandler: Yeah.

Ross: You know, Barracuda was the first song I learned to play on the keyboard.

Chandler: So, you heard it, you repeated it, so that must mean you wrote it.

Joey: Oh, you guys, with this joke. I gotta say, I know I cracked up, but
I<U+0092>m not even sure I got it.

Ross: What, you didn<U+0092>t get it? The doctor is a monkey.

(He and Chandler crack up.)

Chandler: And monkeys can<U+0092>t write out prescriptions.

(He and Ross crack up again. Joey just sits there)

Chandler: (stops laughing, to Ross) You are not allowed to laugh at my joke.

Ross: Your joke? Well, I think <U+0091>the Hef<U+0092> would disagree, which is why
he sent me a check for one hundred ah-dollars.

Chandler: So, you stole my joke, and you stole my money.

Ross: Well, I was going to stick it in the ATM, but now I think I<U+0092>ll show
the sexy teller that I am a published writer.

Chandler: Well, she is going to know that you stole the joke.

Ross: Oh, what are you going to do, follow me down there?

Chandler: Yeah!

Ross: Well, I<U+0092>m not going to go now anyway (he goes to sit down).

Chandler: Okay (he goes to sit down).

(Ross leaps out of his chair and runs out the door, with Chandler in hot pursuit.)

Gunther: (handing Joey the bill) Here you go.

Joey: Ah, Gunther, I can<U+0092>t pay for this right now because I<U+0092>m not
working, so I<U+0092>ve had to cut down on some luxuries like uh, payin<U+0092> for stuff.

Gunther: Well, if you want, you can work here.

Joey: Uh, I don<U+0092>t know. Ya see, it<U+0092>s just, see I was a regular on a
soap opera y<U+0092>know? And to go from that to this, I just<U+0085> Plus, I<U+0092>d have to
wait on all my friends.

Gunther: Okay, but the money<U+0092>s good, plus you get to stare at Rachel as
much as you want.

Joey: What?!

Gunther: Flexible hours.

Joey: Maybe I could be a waiter. Could I use the phone?

[Scene: Monica and Chandler<U+0092>s bedroom, they are in bed together.]

Monica: (visibly upset) She picked Rachel. I mean, she tried to back out of it,
but it was obvious. She picked Rachel.

Chandler: (visibly upset) He took my joke, he took it. 

Monica: It<U+0092>s wrong. You know what else is wrong? Phoebe picking Rachel.

Chandler: You know who else picked Rachel? Ross, and you know what else Ross
did? He stole my joke. You know what? I<U+0092>m going to get a joke journal. Y<U+0092>know?
And document the date and time of every single one of my jokes.

Monica: That<U+0092>s a good idea. 

Chandler: Yeah!

Monica: Do you know what<U+0092>s a bad idea?

Chandler: Picking Rachel.

Monica: That<U+0092>s right. (A noise comes from the living room.) Did you hear
something?

Chandler: Maybe it<U+0092>s the sound of Ross climbing into my brain and stealing
my thoughts.

Monica: It<U+0092>s coming from the living room.

(They go out to investigate, and find Joey wrapped in a blanket watching their
television.)

Joey: (sheepishly) I finished my book. (Chandler and Monica slowly retreat back
to bed.)

[Scene, Phoebe and Rachel<U+0092>s, they<U+0092>re sitting together on the couch.]

Monica: (entering) Hey, you guys.


Phoebe and Rachel: Hey.

Monica: (laughing) Oh, don<U+0092>t you guys look cute. You guys make
such a cute couple.

Rachel: Monica, what are you doing?

Monica: (laughing harder) Nothing, I<U+0092>m just trying to recreate
some of the fun that we had at my place the other day. (To Phoebe) Remember, when you
picked Rachel over me? That was funny.

Phoebe: I guess it was kinda funny.

Monica: (angrily) It wasn<U+0092>t funny at all! Why would you do that?
Why didn<U+0092>t you pick me?

Phoebe: Fine. The reason that I was leaning a little bit more toward Rachel than
you is just that you<U+0092>re <U+0085> just <U+0085> kinda high maintenance<U+0097>Okay
let<U+0092>s go to lunch!

Monica: That is completely untrue. You think I<U+0092>m high maintenance? Okay,
prove it. I want you to make a list and we<U+0092>re going to go through it point by point!

Phoebe: No, okay, you<U+0092>re right. You<U+0092>re easy-going. You<U+0092>re just
not as easy-going as Rachel. She<U+0092>s just more flexible and-and mellow. That<U+0092>s
all.

Rachel: (To Monica) Well, people are different.

Phoebe: Ya, you know, Rachel <U+0085> she<U+0092>ll do whatever you want.
Y<U+0092>know, you can just walk all over her.

Rachel: What? Wait a minute. What are you saying, that I<U+0092>m a pushover?
I<U+0092>m not a pushover.

Phoebe: Oh, okay, (laughing) you<U+0092>re not a pushover.

Rachel: Oh my <U+0085> you think I<U+0092>m a pushover. Well wait, watch this, you
know what? You<U+0092>re not invited to lunch. What do you think of that? I think
that<U+0092>s pretty strong, that<U+0092>s what I think. Come on, Monica, let<U+0092>s go to
lunch. (She leaves)

Monica: (to Phoebe) You start working on that list. (She grabs her
coat and leaves, too.)

[Cut to the hallway.]

Rachel: I cannot believe her.

Monica: I know. Where do you wanna go eat?

Rachel: Oh, oh, I love that Japanese place.

Monica: I<U+0092>m sick of Japanese. We<U+0092>re not going there.

Rachel: All right, wherever you wanna go is cool.

Monica: All right.

[Scene: Central Perk, Monica, Rachel, and Chandler are there as Ross enters and sees
Gunther.]

Ross: (showing the Playboy magazine to Gunther) Oh, hey, Gunther, check this
out.

(Gunther looks at the joke and laughs.)

Gunther: Yeah, that-that Chandler cracks me up. 

(Ross begins to say something, realizes what Gunther just said, turns, and glares at
Chandler. Chandler just shrugs it off.)

Joey: Hey Ross, listen, you want anything to drink, <U+0091>cause I<U+0092>m heading
up there.

Ross: Uh, yeah, I<U+0092>ll take a coffee. Thanks, man.

Joey: Sure. (To Monica and Rachel) Coffee? <U+0091>Cause I<U+0092>m going up there. 

Rachel: No.

Monica: No, thank you.

Joey: (to a table of strangers) You guys need anything, <U+0091>cause I<U+0092>m
heading up there. 

Woman: I<U+0092>d love an ice water.

Joey: You got it.

Monica: Joey, what are you doing?

Joey: Just being friendly. (He gives Monica a <U+0091>what<U+0092>s wrong with
you?<U+0092> look and proceeds to walk behind the counter.)

Rachel: Joey, honey, I don<U+0092>t think you<U+0092>re supposed to go back there.

Joey: Nah, it<U+0092>s okay. Right, Gunther? (Winks at him as if they<U+0092>re in
on a secret together.)

Gunther: Don<U+0092>t wink at me. And put on your apron.

Joey: Okay, but I don<U+0092>t see you asking any other paying customers to put on
aprons. 

Monica: Joey, do you work here?

Joey: No.

Customer: Hey, waiter.

Joey: Yeah?

Commercial Break

[Scene: Central Perk, continued from earlier.]

Monica: Joey, what<U+0092>s going on. What didn<U+0092>t you tell us you work here?

Joey: It<U+0092>s kind of embarrassing, y<U+0092>know. I mean, I was an actor and
now I<U+0092>m a waiter. It<U+0092>s supposed to go in the other direction.

Chandler: So is your apron. You<U+0092>re wearing it like a cape.

Joey: I mean, the job<U+0092>s easy and the money<U+0092>s good, you know? I guess
I<U+0092>m going to be hanging out here anyway. I might as well get paid for it, right? I
just feel kind of weird serving you guys.

Rachel: Come on, Joey, I did it and it was fine.

Ross: Yeah, why would it be weird? Hey, Joey, can I get some coffee?

Joey: Okay, I guess it doesn<U+0092>t seem that weird.

Ross: Seriously, I-I asked you before and you still haven<U+0092>t gotten it.

Joey: See, now it<U+0092>s weird again.

Chandler: I think it<U+0092>s great that you work here. You<U+0092>re going to make
a lot of money, and here<U+0092>s your first tip: Don<U+0092>t eat yellow snow. (He laughs,
then picks up a pen, glares at Ross, and writes in his journal). Ah ha ha, 2:15,
coffeehouse.

Rachel: Well, you know what? This is great. Finally, I have someone I can pass
on my wisdom too. Let me tell you about a couple of things I learned while working at the
coffeehouse. First of all, the customer is always right. (Joey nods.) A smile goes a long
way. (Joey smiles) And if anyone is ever rude to you? Sneeze muffin.

Joey: Thanks, Rach. Look, you guys are just terrific. Y<U+0092>know? Now, how
about clearing out of here so I can get some new customers. It<U+0092>s all about turnover.

Ross: Joey, seriously, can I get my coffee?

Joey: Oh, I<U+0092>m sorry, Ross. I<U+0092>ll get it for you right now. And since I
made you wait, I<U+0092>ll toss in a free muffin.

(He looks at Rachel and winks, she gives him the thumbs-up sign.)

[Scene: Phoebe and Rachel<U+0092>s, Phoebe is sitting on the couch as Rachel and Monica
enter.]

Rachel: Phoebe. We would like to talk to you for a second.

Phoebe: Okay.

Monica: So, maybe I am a little high maintenance. And maybe Rachel is a little
bit of a pushover. But you know what we decided you are?

Rachel: Yes, we are very sorry to tell you this, but you, Phoebe, are flaky.

Monica: Hah!

Phoebe: That true, I am flaky.

Rachel: So, what, you<U+0092>re just, you<U+0092>re just okay with being flaky?

Phoebe: Yeah, totally.

Monica: Well, then, I<U+0092>m okay with being high maintenance.

Rachel: Yeah, and I am okay with being a pushover.

Phoebe: That<U+0092>s great. Good for you guys.

Monica: I am not high maintenance!

Rachel: I am not a pushover!

Phoebe: Who said you were?


Monica and Rachel: You did!

Phoebe: Oh, I<U+0092>m flaky. I<U+0092>ll say anything.

[Scene: Central Perk, Joey is coming back from using the phone.]

Joey: Hey, Gunther. Can you uh, can you cover for me? I just got an audition.

Gunther: No, I<U+0092>m leaving to get my hair dyed.

Joey: Really?! I like your natural color. Come on man, it<U+0092>s a great part.
Look, check it out. I<U+0092>m the lead guy<U+0092>s best friend and I wait for him in this
bar and save his seat. Listen-listen. <U+0091>I<U+0092>m sorry, that seat<U+0092>s saved.<U+0092>

Gunther: That<U+0092>s the whole part?


Joey: Okay, maybe he<U+0092>s not his best friend, but <U+0085>

Gunther: Okay, I<U+0092>ll see you in an hour.

Joey: Oh, man, I could totally get that part. <U+0091>I<U+0092>m sorry, that seat is
taken.<U+0092>

Patron: Oh, excuse me.

Joey: No, no, I didn<U+0092>t mean you. But, you believed me, huh?

Patron: I believed you were saving this seat for someone.

Joey: So, you<U+0092>d hire me, right?

Patron: For what?

Joey: Exactly! All right, everybody, listen up. The coffeehouse is going to be
closed for about an hour. 

Customers: Huh? What?

Joey: Yeah, it<U+0092>s for the kids. To keep the kids off drugs. It<U+0092>s a very
important issue in this month<U+0092>s Playboy. I<U+0092>m sure you all read about it.

[Scene: Monica and Chandler's, Chandler and Ross are both pouting and sitting on the
couch.]

Ross: It<U+0092>s my joke.

Chandler: It<U+0092>s my joke.


Ross and Chandler: It<U+0092>s my joke.

Ross: Y<U+0092>know, I don<U+0092>t think we<U+0092>re going to settle this.

Chandler: Let<U+0092>s have Monica decide.

Ross: Yeah!

Chandler: Yeah!

Ross: Hey Mon.

Chandler: Mon, get out here!

Ross: Monica! (She appears, not sure why she was summoned.)

Chandler: Okay, okay. You have to help us decide whose joke this is.

Monica: Why do I have to decide?

Chandler: Because you<U+0092>re the only one that can be fair.

Ross: Yeah.

Monica: I can<U+0092>t be fair. You<U+0092>re my boyfriend.

Ross: Yeah, but I<U+0092>m your brother. We<U+0092>re family. That<U+0092>s the most
important thing in the world.

Chandler: (to Ross) Don<U+0092>t try to sway her. (To Monica) (Softly) I<U+0092>m
your only chance to have a baby. Okay, let<U+0092>s go.

Ross: We<U+0092>ll each tell you how we came up with the joke and then you decide
which one of us is telling the truth<U+0097>me. 

Monica: Okay, Chandler, you go first.

Chandler: Okay, I thought of the joke two months ago at lunch with Steve.

Monica: Oh, wait, is he the guy I met at Christmas?

Chandler: Can I finish my story?!

Monica: Do you want me to pick you?!

Ross: See, I would never snap at you like that.

(Chandler motions to Monica that he<U+0092>ll give her two babies.)

Monica: Continue.

Chandler: So Steve said he had to go to the doctor. And Steve<U+0092>s
doctor<U+0092>s name is Doctor Muppy. So I said, <U+0091>Doctor Monkey?<U+0092> And that is how
the whole Doctor Monkey thing came up. (He slams his feet up on the table to emphasize his
point.)

Ross: Are you kidding? Okay, look. I-I studied evolution. Remember, evolution?
Monkey into man? Plus, I<U+0092>m a doctor, and I had a monkey. I<U+0092>m Doctor Monkey!

Chandler: I<U+0092>m not arguing with that.

Monica: All right, I<U+0092>ve heard enough. I<U+0092>ve made my decision.

(Both Chandler and Ross are eager to hear her decision.) 

Monica: You are both idiots. The joke is not funny, and it<U+0092>s offensive to
women, and doctors, and monkeys! You shouldn<U+0092>t be arguing over who gets credit, you
should be arguing over who gets blamed for inflicting this horrible joke upon the world!
Now let it go! The joke sucks! 

(Monica leaves the room)

Ross: It<U+0092>s your joke.

Chandler: Is not.

[Scene: Central Perk, Rachel is sitting at a table and Phoebe is on the couch. Chandler
and Monica can be seen outside, she<U+0092>s lecturing him, and pushes him inside. He then
nods to Rachel, and is obviously counting off the seconds in his head and then Monica
makes a grand enterance.]

Monica: Hi, Chandler. There you are.

Chandler: Hi, oh hi.

Monica: Hey, it<U+0092>s Phoebe and Rachel. Um, why don<U+0092>t you tell them what
you were telling me earlier about me not being high maintenance?

(Rachel and Phoebe exchange looks.)

Chandler: (starts to recite a rehearsed speech) Monica is a self-sufficient,
together lady. (Pause.) Being with her has been like being on a vacation. And what may be
perceived as high maintenance is merely attention to detail and<U+0097>(He falters and
Monica prompts him.)<U+0097>generosity of spirit.

Rachel: Wow, you know what? That is the best fake speech I think I<U+0092>ve ever
heard.

Phoebe: Really? I<U+0092>ve heard better.

Monica: Wait, wait, he came up with that himself. Tell them, Chandler.

Chandler: (To Chandler) I<U+0092>m out of words. Should I just say the whole thing
again?

Monica: Look, I am not high maintenance. I am not. Chandler!

Chandler: (pauses as he struggles with what he has to say) You<U+0092>re a little
high maintenance.

Monica: Ahhh! You are on my list.

Chandler: I<U+0092>m sorry. You<U+0092>re not easy-going, but you<U+0092>re
passionate, and that<U+0092>s good. And when you get upset about the little things, I think
that I<U+0092>m pretty good about making you feel better about that. And that<U+0092>s good
too. So, they can say that you<U+0092>re high maintenance, but it<U+0092>s okay, because I
like <U+0085> maintaining you.

Monica: (embarrassed) (To Phoebe and Rachel) I didn<U+0092>t even tell him to say
that. (They hug). All right you<U+0092>re off my list. 

Chandler: (happily) I<U+0092>m off the list. (Sits on the couch.) 

Monica: (sits next to him) Phoebe, it<U+0092>s okay that you don<U+0092>t want me to
be your girlfriend because I have the best boyfriend.

Phoebe: (to Monica) Y<U+0092>know, suddenly I find you very attractive.

(Joey enters.)

Chandler: Hey, buddy boy, how<U+0092>d the audition go?

Joey: Not good, no. I didn<U+0092>t get the part, and I lost my job here, so
<U+0085>

Phoebe: Wow! That is a bad audition.

Rachel: How-how did you lose your job here?

Joey: Well, I had the audition but Gunther said I had to stay here and be in
charge so he could go get his hair dyed. So, I went anyway, and then he fired me.

Rachel: He left work in the middle of the day to do a personal errand and left
you in charge when you<U+0092>ve been working here two days? That<U+0092>s not, that<U+0092>s
not right.

Joey: Yeah, what are ya gonna do?

Rachel: Joey, you can<U+0092>t let him get away with that. Ya know what, I<U+0092>m
not going to let him get away with that. I<U+0092>m going to say something to him<U+0097>No, I
really shouldn<U+0092>t say anything<U+0097>No, I should say something to him. (Goes to the
counter) Gunther, I want you to give Joey his job back. That is really not fair that you
have to fire him<U+0085>

Gunther: Okay.

Rachel: What?

Gunther: He can have his job back.

Rachel: That<U+0092>s right, he can have his job back. I<U+0092>m glad we got that
all straightened out. There you go, Joey, you got your job back.

Joey: That<U+0092>s great. Thanks Rach.

Rachel: Yeah, pretty nice, huh? Now who<U+0092>s a pushover?

Phoebe: (returning from the bathroom) Rach, you<U+0092>re in my seat.

Rachel: Oh, I<U+0092>m sorry. (Gets up and moves.)

Ending Credits

[Scene: Monica and Chandler's, Monica, Phoebe, and Rachel are sitting at the kitchen
table, talking.]

Phoebe: Hey, I never got to hear who you guys would pick to be your girlfriend.

Monica: I pick you, Phoebe.

Rachel: Oh, yeah. Definitely you, Pheebs.

Phoebe: Yeah, well, I kinda thought.

(Phoebe gets up from the table, and while her back is turned, Rachel and Monica
indicate via sign language that they each would have picked the other.)

(Joey, Chandler and Ross enter.)

Chandler: Hey.

Rachel: Hey. Oh, I have a question. If-if-if one of you had to pick one of the
other two guys to go out with, who would you pick?

Ross: No way. 

Joey: I<U+0092>m not answering that.

Chandler: Joey! (Pause as they all stare at him.) No way. I<U+0092>m not answering
that. 

End


