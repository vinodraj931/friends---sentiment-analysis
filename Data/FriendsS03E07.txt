

The One With the Race Car Bed

Written by: Seth Kurland
Transcribed by: Eric Aasen



[Scene: Central Perk, the whole gang is there, Ross is telling a story about what
happened at work and the rest of the gang are thinking to themselves, denoted by italics.]

Ross: So I told Carl, <U+0091>Nobody, no matter how famous their parents are,
nobody is allowed to climb on the dinosaur.<U+0092> But of course this went in one ear and
out.....

Rachel: I love how he cares so much about stuff. If I squint I can pretend
he<U+0092>s Alan Alda.

Monica: Oh good, another dinosaur story. When are those gonna become extinct?

Chandler: If I was a superhero who could fly and be invisible, that
would be the best.

Gunther: What does Rachel see in this guy? I love Rachel. I wish she was my
wife.

(Joey is singing in his head.)

Phoebe: Who<U+0092>s singing?


Opening Credits


[Scene: Monica and Rachel's, the whole gang is there including Janice, they<U+0092>re
watching Happy Days.]

Ross: Hey. When you guys were kids and you played Happy Days, who were
you? I was always Richie.

Monica: I was always Joanne.

Joey: Question. Was ah, <U+0091>Egg the Gellers!<U+0092> the war cry of your
neighbourhood?

(A commercial for the Mattress King, Janice<U+0092>s ex-husband, comes on TV.)

Phoebe: Ewww! Oh! It<U+0092>s the Mattress King!

Joey: Booo!!

Chandler: (to Janice) Don<U+0092>t look honey. Change the channel! Change the
channel!

Janice: Wait! Wait! I wanna see this. After I divorce him, half of that kingdom
is gonna be mine.

Matress King: (on TV) <U+0091>Despair fills the mattress showroom. My kingdom
is suddenly without a queen. I<U+0092>m so depressed I<U+0092>m going to slash... my prices!!
Check it out! Four ninety-nine for a pillow top queen set! I<U+0092>m going medieval on
prices!

Chandler: What a wank!

Janice: Oh, I cannot believe he<U+0092>s using our divorce to sell mattresses.

Monica: I know! And four ninety-nine for a pillow top queen set, who cares about
the divorce, those babies will sell themselves. (they all stare at her) And I<U+0092>m
appalled for you by the way.

Matress King: (on TV) I<U+0092>m close. I<U+0092>m cheap. I<U+0092>m the king.

[Scene: Central Perk, Rachel is on the phone, everyone else is there except Joey.]

Rachel: <U+0091>Okay. (listens) Okay, daddy we<U+0092>ll see you tomorrow night.
(listens) Okay bye-bye.<U+0092> (hangs up)

Ross: We?

Rachel: Are ah, having dinner with my Dad tomorrow night, I hope that<U+0092>s
okay.

Ross: Oh shoot, tomorrow<U+0092>s not so good, I<U+0092>m supposed to um, fall off
the Empire State building and land on a bicycle with no seat. Sorry.

Rachel: Ross, my father doesn<U+0092>t hate you.

Ross: Please, he refers to me as <U+0091>wethead<U+0092>.

Rachel: But honey he calls everybody by a nickname! Okay, look, I know, all
right, just one dinner, please, just one night for me, please. I just want him to love you
like I do. (Ross looks at her) All right, well not exactly like I do, but, but, if you do
come to dinner, I<U+0092>ll love you like I do in that black thing that you like.

Chandler: (leaning in) I<U+0092>ll go.

Ross: Fine.

Rachel: Thank you.

Ross: Hi Gunther.

Gunther: Yeah, we<U+0092>ll see!

Joey: (entering) Hey, you guys!

Phoebe: Hey!

Joey: Guess what?

Ross: What?

Joey: I got a gig!

All: Yay!!

Chandler: See, that<U+0092>s why I could never be an actor. Because I can<U+0092>t
say gig.

Phoebe: Yeah, I can<U+0092>t say croissant. (realises) Oh my God!

Monica: What<U+0092>s the part?

Joey: Well, it<U+0092>s not a part, no. I<U+0092>m teaching acting for soap operas
down at the Learning Extension.

Ross: Come on! That<U+0092>s great.

All: Wow!

Joey: Yeah, yeah. It<U+0092>s like my chance to give something back to the acting
community.

Ross: Y<U+0092>know your probably not allowed to sleep with any of your students.

Joey: (glares at him) I know!

[Scene: Mattress King, Monica and Phoebe are shopping for a new mattress.]

Phoebe: Ugh! I don<U+0092>t know Monica. It feels funny just being here. I mean if
you buy a bed from Janice<U+0092>s ex-husband, that<U+0092>s like betraying Chandler.

Monica: Not at these prices.

Phoebe: (sees a little kid playing with a race car bed) (to kid) Hi. Y'know in
England this car would be on the other side of the store. (the kid just stares at her, and
she makes the <U+0091>that went right over your head<U+0092> motion) Woo!

Monica: (lying down on a mattress) Oh! Ohhhhh! Oh! Phoebe, come here. Aw, this
is my new bed. You gotta feel this bad boy.

Phoebe: Eh, Monica it, it feels so weird, y'know, Chandler<U+0092>s your friend...
(hops onto the bed) Oh! Oh my God! Aw, all right take this bed, you can make other
friends.

[Scene: Classroom. Joey is writing his name on the board, but turns around before
he<U+0092>s done which causes him to write his name with a downward curve, and he then
underlines it, and draws the line right through his name.]

Joey: Good evening. I<U+0092>m Mr. Tribbiani. And I will be teaching acting for
soap operas. Now um, on my first day as (proudly) Dr. Drake Remoray on Days of Our
Lives, (looks for a reaction from his students, and gets none.) I learned that one of
the most important things in soap opera acting is reacting, this does not mean acting
again, it means, you don<U+0092>t have a line, but someone else just did. And it goes like
this. (looks all intense for a moment and then gasps, the students cheer him) Thanks,
thanks, a lot. Oh, by the way, before I forget to work in soap operas some of you will
have to become much more attractive. All right, moving right along.

[Scene: Monica and Rachel's, Joey is entering, Phoebe is already there waiting for the
delievery guy.]

Joey: Hi!

Phoebe: Hey! Ooh! How was teaching last night?

Joey: Oh it was great. Yeah, you get to say stuff like, <U+0092>Hey, the bell
doesn<U+0092>t dismiss you, I dismiss you.<U+0092>

Phoebe: Ooooh, nice.

Joey: Oh, and guess what, I got an audition for All My Children.

Phoebe: Oh, yay!

Joey: Yeah, it<U+0092>s this great part, this boxer named Nick. And I<U+0092>m so,
so right for it, y'know, he<U+0092>s just like me. Except he<U+0092>s a boxer, and has an evil
twin.

(There is a knock on the door.)

Phoebe: Oh. (goes and answers the door and there is this huge black
delievery guy.) 

Guy: Dom da-da dom! Here ye! Here ye! Delivery from the Mattress King. (to
Phoebe) You Miss Geller?

Phoebe: Okay.

Guy: Sign here. (hands her a clipboard)

Phoebe: Oh, do I have a middle name. All right Monica Velula Geller. It<U+0092>s
that bedroom there. (points to Monica<U+0092>s room)

Joey: Hey, Monica bought a bed from the Mattress King?

Phoebe: Yeah, so please, please, please, don<U+0092>t say anything to Chandler.

Joey: You want me to lie to Chandler?

Phoebe: Is that a problem?

Joey: No.

Phoebe: Oh, hey, hey Nick the boxer let<U+0092>s see what you got. All right ya,
put <U+0091>em up. Come on. (they start shadow boxing)

Joey: Hey, you<U+0092>re ah, pretty good at this.

Phoebe: Yeah, well I had to learn, I was staying at the Y and some off the young
men weren<U+0092>t acting Christian enough.

Joey: Ahh!

(Joey throws a punch and just lightly taps her on the shoulder, Phoebe counters with a
jab to the nose.)

Joey: Hey now!

(Phoebe throws another jab, and lands it on Joey<U+0092>s nose, causing it to bleed.)

Joey: Hey!!! Oww!! And I<U+0092>m bleeding.

Phoebe: Oh! Oh! Oh!

Joey: Okay, great.

Phoebe: Wow! And I<U+0092>m a vegetarian! All right, all right, well I<U+0092>m
sorry, we<U+0092>ll put some ice on it. 

Joey: Okay.

Phoebe: <U+0091>Kay, put your head back.

Joey: All right. I can<U+0092>t see.

Phoebe: All right, I have ya. Oh God.

Guy: Which bedroom do ya want it in Miss Geller?

Phoebe: Oh, it<U+0092>s the compulsively neat one by the window, okay.

Guy: Gotcha. (he and his helper walk in carrying the racecar bed.)

[Scene: Restaurant, Rachel and Ross and Dr. Green are having dinner.]

Rachel: Hi Daddy!

Dr. Green: This where they put it? What, there no table available in the
kitchen! Hello, baby.

Rachel: You remember Ross.

Dr. Green: Um-hmm.

Ross: Nice to see you again Dr. Green. 

Dr. Green: So! (they both try to sit next to Rachel but Dr. Green is
successful.) (to Ross) How<U+0092>s the library?

Ross: Ugh, museum.

Dr. Green: What happened to the library?

Ross: There never was a library. I mean there are libraries, its just that I ah,
I never worked at one.

Dr. Green: You know what<U+0092>s really good here, the lobster. What do you say
shall I just order three.

Ross: Yeah, if you<U+0092>re really hungry. (Dr. Green stares at him) It was a
joke, I made a joke.

Rachel: Yeah, actually Daddy Ross is allergic to lobster.

Dr. Green: What kind of person is allergic to lobster? I guess the kind of
person that works at a library.

Ross: It<U+0092>s not a library...

Dr. Green: (interrupting him) I know!! It<U+0092>s a museum! What, you<U+0092>re the
only one around here who can make a joke! At least mine was funny. Ah, waiter, we will
have two lobsters and a menu. (nods at Ross, and mouths I don<U+0092>t know to the waiter.)

[Scene: After dinner.]

Ross: So, Dr. Green, how<U+0092>s the old boat.

Dr. Green: They found rust. You know what rust does to a boat?

Ross: It gives it a nice antiquey look.

Dr. Green: (he stares at Ross) Rust, is boat cancer, Ross.

Ross: Wow. I<U+0092>m sorry, when I was a kid I lost a bike to that. (Rachel
giggles at that)

Dr. Green: Excuse me for a moment, will you please, I want to say good night to
the Levines, before we go.

Rachel: Okay.

Ross: Okay! (picks up a knife and pretends to stab his heart.)

Rachel: Aw honey stop! It<U+0092>s not that bad.

Ross: Yeah. (sees the bill) Op! Uh-oh! I think your Dad must<U+0092>ve added
wrong. He only tipped like four percent.

Rachel: Yeah. That<U+0092>s Daddy.

Ross: That<U+0092>s Daddy?! But doesn<U+0092>t it bother you? You<U+0092>re a
waitress.

Rachel: Yes, it bothers me Ross, but y'know if he was a regular at the coffee
house, I<U+0092>d be serving him sneezers.

Ross: So?

Rachel: So. Ross, I<U+0092>ve bugged him about this a million times, he<U+0092>s not
gonna change.

Ross: You really serve people sneezers?

Rachel: Well um, I don<U+0092>t.

Dr Green: You kids ready?

Ross: Thanks again, Dr. Green.

Dr. Green: All right.

(Ross takes a twenty and slips it underneath the bill when Dr. Green isn<U+0092>t
looking.)

Dr. Green: Oh, wait, wait, wait, wait, I think I forgot my receipt.

Ross: Oh, ah, you don<U+0092>t need that.

Dr. Green: Why not?

Ross: The carbon, it<U+0092>s messy, I mean it gets on your fingers and causes,
the, the ah, night blindness.

Dr. Green: (gets his receipt and notices the twenty) What is this? Who put a
twenty down here? Huh?

Ross: Oh, yeah, that would be me, um, I have, I have a problem I-I tip way too
much, way, way, too much, it<U+0092>s a sickness really.

Rachel: Yeah it is, it is. (to Ross) We really, really have to do something
about that.

Ross: I know.

Dr. Green: Excuse me, you think I<U+0092>m cheap?

Rachel: Oh Daddy, no he didn<U+0092>t mean anything by that, he really
didn<U+0092>t.

Ross: Nothing I do means anything, really.

Dr. Green: This is nice. I pay two hundred dollars for dinner, you put down
twenty, and you come out looking like Mr. Big Shot. You really want to be Mr. Big Shot?
Here, I<U+0092>ll tell you what, you pay the whole bill, Mr. Big Shot, all right. (rips up
the bill, and throws it at Ross, then leaves)

Ross: Well Mr. Big Shot is better than <U+0091>wethead<U+0092>.

[Scene: Classroom, Joey is lecturing on facial expressions.]

Joey: Okay, some tricks of the trade. Now, I<U+0092>ve never been able to cry as
an actor, so if I<U+0092>m in a scene where I have to cry, I cut a hole in my pocket, take a
pair of tweezers, and just start pulling. Or ah, or, let<U+0092>s say I wanna convey that
I<U+0092>ve just done something evil. That would be the basic <U+0091>I have a fishhook in my
eyebrow and I like it<U+0092> (Does it by raising one eyebrow, and showing off the pretend
fishhook.) Okay, let<U+0092>s say I<U+0092>ve just gotten bad news, well all I do there is try
and divide 232 by 13. (looks all confused) And that<U+0092>s how it<U+0092>s done. Great soap
opera acting tonight everybody, class dismissed.

Student: Hey, Mr. Trib.

Joey: Hey-hey.

Student: Guess what, I got an audition!

Joey: Awww, one of my students got an audition. I<U+0092>m so proud.

Student: I was wondering if you would consider coaching me for it?

Joey: You bet! What<U+0092>s the part?

Student: Oh it<U+0092>s great, it<U+0092>s a role on All My Children, Nick
the boxer.

(Joey does the <U+0091>232 divided by 13 bad news<U+0092> look.)

Commercial Break

[Scene: Hallway, Ross and Rachel are returning from dinner.]

Rachel: You had to do it, didn<U+0092>t you? You couldn<U+0092>t just leave it
alone.

Ross: Four percent. Okay. I tip more than that when there<U+0092>s a bug in my
food.

Rachel: Ross, tonight was about the two of you getting along. (Ross groans and
rubs his neck) Oh, would you just see my chiropractor, already.

Ross: Yeah, I<U+0092>m gonna go to a doctor who went to school in a mini-mall.

(they go into Monica and Rachel<U+0092>s, and see Phoebe hopping around.)

Ross: Hey Pheebs, what are you doing?

Phoebe: I<U+0092>m, I<U+0092>m freaking out! Monica kinda trusted me with something
and she shouldn<U+0092>t have! All right, I haven<U+0092>t lived here in a while, so I have to
ask you something. Does Monica still turn on the lights in her bedroom?

Rachel: Um. yeah.

Phoebe: I am soo dead. (goes to Monica<U+0092>s room)

Rachel: All right, look, here<U+0092>s the bottom line Ross, this is fixable, if
we act fast, okay. So, I<U+0092>ll invite him to brunch tomorrow and you can make nice.

Ross: Look, honey, I have tried to make nice, it doesn<U+0092>t work.

Rachel: Okay, look, Ross, I realise that my Father is difficult, but that<U+0092>s
why you have got to be the bigger man here.

Ross: Look sweetie, I could be the bigger man, I could be the biggest man, I
could be a big, huge, giant man, and it still wouldn<U+0092>t make any difference, except
that I could pick your Father up and say <U+0091>Like me! Like me tiny doctor!<U+0092>

Rachel: Okay, well can<U+0092>t you just try it one more time Ross? For me? For
me?

Ross: Rachel one brunch is not gonna solve anything. You gotta face it, okay
we<U+0092>re never gonna get along.

Rachel: Okay, well you are just gonna have too, okay. Because I already got a
Mother and a Father who cannot stay in the same room together, okay, I don<U+0092>t wanna
have to have a separate room for you too!! (starts to cry)

Ross: Okay, okay, okay. (hugs her) I<U+0092>ll get the bagels.

[Scene: Monica<U+0092>s bedroom, Phoebe is trying to hide the bed from Monica.]

Monica: (sees the bed) What<U+0092>s this?

Phoebe: Isn<U+0092>t it cool! Varoom! Varoom!

Monica: This is not the bed I ordered!

Phoebe: I know, you must<U+0092>ve won like a contest or something! 

(Phoebe starts to make a sound like a car accelerating)

Monica: Phoebe!

(Phoebe makes a sound like a car screeching to a halt.)

Monica: Why is this car in my bedroom?

Phoebe: I<U+0092>m sorry, okay, I-I wasn<U+0092>t looking, and the store says that
they won<U+0092>t take it back because you signed for it...

Monica: When did I sign for it?

Phoebe: When I was you! Y'know what, it<U+0092>s all Joey<U+0092>s fault,
<U+0091>cause he left his nose open!

Monica: Did you make brownies today?

Chandler: Knock, knock.

Monica: (to Phoebe) Quick, take off your dress, he won<U+0092>t notice the bed.

Chandler: Hey, I<U+0092>m going for sushi does anybody want.. (enters and sees the
bed) Whoa-whoa, somebody missed the off ramp.

Phoebe: It<U+0092>s Monica<U+0092>s bed. What?

Chandler: Okay. (to Monica) It<U+0092>s a racecar.

Phoebe: So. This has always been Monica<U+0092>s bed, what you<U+0092>re just
noticing now, how self-involved are you?

Chandler: Okay, well it this bed isn<U+0092>t new, how come there is plastic on
the mattress?

Monica: Sometimes I have bad dreams. (starts to break down, and Phoebe offers
her, her hand to comfort her.)

[Scene: Classroom, Joey is coaching his student.]

Student: Look, I just saw my best friends brains smeared across the canvas,
that<U+0092>s not gonna be me, not me.

Joey: Wow! That was good. That was...(points to his pocket) Tweezers?

Student: No.

Joey: Whoa. That was really good.

Student: Thanks, any suggestions?

(Joey gets the evil look on his face.)

[Scene: Central Perk, Chandler, Monica, and Phoebe are there, yelling at Joey.]

Chandler: You told him to play the boxer gay!!

Joey: Well, I-I might<U+0092>ve said supergay.

Chandler: You totally screwed him over.

Monica: Joey, you<U+0092>re this guy<U+0092>s teacher. I mean how could you do this?

Joey: Because, Monica, the guy<U+0092>s so good, and I really, really want this
part.

Phoebe: Well, if you really, really want it, then it<U+0092>s okay.

[Scene: Monica and Rachel's, Rachel is greeting her Father for their brunch.]

Rachel: (opening the door) Hi Daddy.

Dr. Green: Baby. Ross.

Ross: Dr. Green. How are you? (offers his hand, and Dr. Green puts his scarf on
it.)

Dr. Green: Thanks for dinner last night.

Ross: Thank you for teaching me a valuable lesson.

Dr. Green: Nice hair. What<U+0092>d ya do? Swim here?

Ross: (to Rachel) Okay, that<U+0092>s it, I can<U+0092>t take it anymore.

Rachel: What? What? He<U+0092>s interested in you. He-he likes your hair, he just
wants to know how you got here.

Ross: Oh, please. Sweetie it<U+0092>s hopeless, okay, I<U+0092>m just gonna go.
(starts to leave rubbing his neck)

Rachel: What?!

Ross: Look, look I<U+0092>m sorry. It<U+0092>s just that....

Dr. Green: Ross? What<U+0092>s with the neck?

Rachel: He<U+0092>s got this thing. And I keep telling him to go to my
chiropractor...

Dr. Green: You<U+0092>re still going to that chiropractor, that man couldn<U+0092>t
get into medical school in Extapa!

Ross: Thank you! That<U+0092>s what I keep saying.

Rachel: Excuse me, Dr. Bobby happens to be an excellent doctor.

Ross: Uh.

Dr. Green: Wait a minute, his name is Dr. Bobby?

Rachel: Well that<U+0092>s his last name.

Ross: And his first name.

Dr. Green: He<U+0092>s Bobby Bobby?

Rachel: It<U+0092>s Robert Bobby.

Dr. Green: Oh.

Rachel: And um, excuse me, he helps me.

Ross: Oh-ho please. Ask her how?

Dr. Green: What do you need help for?

Rachel: With my alignment. I<U+0092>ve got one leg shorter than the other.

Dr. Green: Oh God!

Ross: Argue with that.

Rachel: What? It<U+0092>s true, my right leg is two inches shorter.

Dr. Green: Come on! You<U+0092>re just titling! (to Ross) Her legs are fine!

Ross: I know that!

Dr. Green: So, why do you let her go to a chiropractor for?

Rachel: I<U+0092>m sorry, let her?

Ross: What can I do, she doesn<U+0092>t listen to me about renter<U+0092>s insurance
either.

Dr. Green: Wait a minute, you don<U+0092>t have renter<U+0092>s insurance?!

Rachel: No.

Dr. Green: Well what if somebody steals something? How are you gonna run after
him with one leg shorter than the other?!

(Both he and Ross start laughing)

Ross: Hey, would you ah, would you like some juice?

Dr. Green: I<U+0092>d love some juice. Thanks.

Ross: Okay. (to Rachel) Wow! This is going so well. Did you see us? Did you see?

Rachel: Yeah honey, I<U+0092>m standing right there! Why didn<U+0092>t you just tell
him about the mole I haven<U+0092>t got checked yet.

Ross: Excellent!

[Scene: Classroom, Joey is talking to his students.]

Joey: (sadly) There will come a time in each of your careers when you<U+0092>ll
have a chance to screw over another soap opera actor. I had such an opportunity in the
recent, present. And I<U+0092>m ashamed to say that I took it, I advised a fellow actor to
play a role, homosexually. Yeah, we both auditioned for the part, and uh, as it turned
out, they ah, they liked the stupid gay thing and cast him. And now, he<U+0092>s got a two
year contract opposite Susan Luchhi, the first lady of daytime television, and me, me
I<U+0092>m stuck here teaching a bunch of people, most of whom are too ugly to even be on
TV. I<U+0092>m sorry, I<U+0092>m sorry, I<U+0092>m sorry. (he gets a huge round of applause from
his students.) Thank you.

[Scene: Mattress King, Monica is trying to return her bed.]

Jester: Uh, may I help you?

Monica: Yeah, I talked to you on the phone, I<U+0092>m the lady that got stuck
with the racecar bed.

Jester: Look, it<U+0092>s like I told you, there<U+0092>s nothing I can do. You
signed for it, Monica Velula Geller.

Joey: All right, Jester man, look we wanna see the king.

Jester: Nobody sees the king!

Joey: Oh-ho-kay, I<U+0092>m talking to the king. (starts to go to a back room)

Jester: Hey! You can<U+0092>t go back there!

(Joey goes to the door, but stops and looks through the window at Janice and the
Mattress King, her ex-husband, kissing.)

Janice: Oh my God.

(Joey fakes a scream.)

Closing Credits

[Scene: Monica<U+0092>s bedroom, Chandler is playing with the bed.]

Chandler: Varrrrrroom! Hey! Watch it lady! Varrrrrrrrrrom! (makes a screeching
sound as he pretends to stomp on the brakes.) Hey-hey good lookin<U+0092>! (honks the
bed<U+0092>s little horn on the steering wheel.) Varrrrrrrrroom. (notices Rachel and stops)
All right, I<U+0092>ll leave. My bed<U+0092>s so boring.


End

