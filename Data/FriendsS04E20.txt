

The One With All the Wedding Dresses

Story by: Adam Chase
Teleplay by: Michael Curtis & Gregory S. Malins
Transcribed by: Eric Aasen

[Scene: Joey<U+0092>s bedroom, he is asleep and snoring loudly. Chandler enters wondering
who left their engine running.]

Chandler: Are you kidding me?! Joey. Joey! Joey! Joey! Joey! Joey! Joey! Joey!
Joey! Joey!!

Joey: (joining in, in his sleep) Joey. Joey. Joey. Joey! Joey!!

(Chandler acts disgusted, but is happy that Joey has stopped snoring. However, just as
he is about to leave, Joey starts snoring again. So to get him to stop, he slams the door
shut, waking Joey.)

Chandler: Oh. Oh, did-did-did I wake you?

Opening Credits

[Scene: Central Perk, Chandler is getting another cup of coffee.]

Chandler: Gunther, can I get another cup of coffee, please? (Gunther starts to
pour him another cup.) So uh, what do you do when you<U+0092>re not working here?

Gunther: You don<U+0092>t need to fill these silences.

Chandler: Oh, okay, thanks. (He goes back to the couch and rejoins Monica, Joey,
and Phoebe.)

Monica: Chandler, that<U+0092>s like your fourth cup of coffee!

Chandler: Well, I am drinking lots of cups of coffee because I<U+0092>m exhausted!
Because Joey started snoring!

Monica: He<U+0092>s in a different room! He<U+0092>s really that loud?

Joey: (proudly) Oh, you should here me.

Chandler: It<U+0092>s not something to be proud of, okay? You have to go to a
sleep clinic!

Joey: Look, I told ya, I<U+0092>m not going to any clinic! I don<U+0092>t have a
problem, you<U+0092>re the one with the problem! You should go to a "Quit being a baby
and leave me alone" clinic!

Chandler: They don<U+0092>t have those.

Joey: Yeah, they do! Quit being a baby and leave me alone! There, you<U+0092>ve
just had your first class!

Monica: Y<U+0092>know I used to go out with this guy that was a really light
sleeper, and whenever I started to snore, he would just roll me over<U+0085>

Joey: Ohhh, yeah!

Monica: He would just roll me over and I would stop snoring.

Chandler: Next time you snore, I<U+0092>m rolling ya over!

Joey: I gotta do what I gotta do, you gotta do what you gotta do, you just do
it.

Ross: (entering) Hey guys!

Chandler: Hey, all right!

Phoebe: Hey!

(Joey starts humming Here Come the Bride.)

Phoebe: Oh, the Olympics.

Monica: Have you guys picked a date yet?

Ross: Oh no, not yet.

Phoebe: I still cannot believe you<U+0092>re engaged! (Ross looks at her)
Just <U+0091>cause its happening so fast; not <U+0091>cause you<U+0092>re such a loser. 

Ross: Oh. Thanks. Uh, has anyone seen Rach?

Monica: Ugh, she<U+0092>s upstairs not doing the dishes! And I tell ya
something! I<U+0092>m not doing them this time! I don<U+0092>t care if those dishes sit in the
sink until they<U+0092>re all covered with<U+0097>I<U+0092>ll do them when I get home!

Ross: Yeah<U+0097>oh! Hey listen umm, Emily found this wedding dress in
London<U+0085>

Phoebe: Already?!

Ross: Yeah, but it didn<U+0092>t fit. Well, luckily there<U+0092>s a store here that
has one left in her size, but I<U+0092>m the groom, I<U+0092>m not supposed to see the
dress<U+0085> 

Monica: I<U+0092>ll pick it up for you!

Ross: Thank you.

Monica: Okay.

Chandler: Oh, she<U+0092>s got you running errands, y<U+0092>know, picking up
wedding dresses<U+0085> (Laughs and makes like Indiana Jones and his whip) Wah-pah!

Ross: What<U+0092>s wah-pah?

Chandler: Y<U+0092>know, whipped! Wah-pah!

Joey: That<U+0092>s not whipped! Whipped is wh-tcssh!

Chandler: That<U+0092>s what I did. Wah-pah!

Joey: You can<U+0092>t do anything!

[Scene: Monica and Rachel's, Rachel is not doing the dishes. She hears someone coming
up the stairs and quickly puts down her magazine and pretends like she<U+0092>s actually
doing the dishes.]

Rachel: Hey, Mon, I was just doing the dishes! 

Ross: Hey!

Rachel: Oh! It<U+0092>s you. (She stops doing the dishes.) Hi.

Ross: Hey, do uh, do you have a minute?

Rachel: Yeah, yeah, I was just about to take a break anyways, so<U+0085>

Ross: So listen uh, I know you and I haven<U+0092>t really had a chance to talk
since uh, Emily and I decided to get married, and uh, I was just wondering how you were.

Rachel: Oh.

Ross: I know if you were getting married I<U+0092>d feel, kinda<U+0085>..
y<U+0092>know.

Rachel: Yeah. Yeah. Definitely, well it definitely took me by surprise, but
I<U+0092>m okay.

Ross: Yeah?

Rachel: Yeah.

Ross: All right, I just wanted to check.

Rachel: Oh, that<U+0092>s sweet.

(He goes over to hug her.)

Ross: You<U+0092>re great. And I-I know someday this will happen for you too. You
just hang in there.

Rachel: (breaking the hug) Uhh, hang in there?

Ross: Oh, no, I didn<U+0092>t mean, uh<U+0085>

Rachel: I mean maybe you didn<U+0092>t hear about a serious relationship called me
and Joshua?

Ross: Oh, I thought you guys had just been on like four dates, I didn<U+0092>t
realise that had become anything, yet.

Rachel: Oh, no-no-no, no-no-no, it has become, it has<U+0097>yeah. Oh no, those
were four great dates. 

Ross: Oh. Yeah?

Rachel: Yeah. Oh, yeah. And I mean, the connection, I mean y<U+0092>know,
emotionally, mentally, physically<U+0085>

Ross: Wow, that<U+0092>s-that<U+0092>s-that<U+0092>s incredible.

Rachel: I know isn<U+0092>t it? It<U+0092>s like I<U+0092>m right there with Joshua.

Ross: Uh-huh.

Rachel: You are right there with Emily. And it<U+0092>s y<U+0092>know, it<U+0092>s
kinda like<U+0085>. it<U+0092>s a tie! Well, I gotta get, I gotta get back to the dishes.

Ross: I gotta get to work.

Rachel: Oh yeah? Fine.

Ross: Hey, y<U+0092>know, y<U+0092>know what would make me really happy?

Rachel: Oh yeah, no, what<U+0092>s that?

Ross: If like the four of us could all y<U+0092>know, hang out together. Uh, in
fact Emily<U+0092>s coming into town this weekend, why don<U+0092>t you say we all have
dinner? Say, Sunday night?

Rachel: That would be great!

Ross: Yeah, all right, it<U+0092>s a date. (He leaves)

Rachel: (to the closed door) Hang in there. You hang in there. (Gives him the
raspberry.) 

Ross: (coming back in) Did you say something?

Rachel: No, just singing. (Does a little song.)

[Scene: Beatrice Bridal Shop, Monica and Phoebe are there to pick up Emily<U+0092>s
dress.]

Monica: Oh my God! Ohh! Look at this one! It<U+0092>s so beautiful!

Phoebe: Yeah, but y<U+0092>know, about have of these are gonna end up getting
divorced.

The Saleslady: May I help you ladies?

Monica: Oh, yes, umm, I<U+0092>m here to pick up a dress that you have on hold.

The Saleslady: Yes, what<U+0092>s the name, please?

Monica: Emily Waltham.

The Saleslady: Yes! I have it right here. (Phoebe and Monica both gasp at the
dress.) Would you like to try it on Ms. Waltham?

Monica: (laughs) Okay.

[Time lapse. Monica is wearing the dress and starring at herself in the mirror.]

Phoebe: You<U+0092>re the most beautiful bride I<U+0092>ve ever seen.

Monica: I am, aren<U+0092>t I?

The Saleslady: Ms. Waltham?

Monica: Yes?

The Saleslady: We<U+0092>re closing.

Monica: All right. (Goes to take off the dress.)

The Saleslady: And could I get my ring back?

(She disgustedly takes the ring off and gives it back.)

[Scene: Joey<U+0092>s bedroom, he<U+0092>s snoring again and Chandler is there to roll him
over.]

Chandler: All right buddy, time to roll over. (Rolls him over, and discovers a
surprise) (Looking down) No-no! (Covers his eyes) No, no-n-n-n-no!! You are going to a
clinic! You<U+0092>re going to a clinic, and a pyjama store!

[Scene: Monica and Rachel's, Monica is doing the dishes.]

Monica: Does she use the cups? Yes! I believe she does. Does she use the plates?
Yes! I believe she does. (Looks at the wedding dress and stops.)

[Time lapse, Monica is now wearing the dress while doing the dishes and is
making like she is thanking her guests for coming to her wedding. Paging Dr. Crane. Dr.
Fraiser Crane!]

Monica: Oh. Thank you. Ohhh, thank you very much. Oh, thank you for coming.
(There<U+0092>s a knock on the door.) Uh, just a second!

Phoebe: No-no, let me in!

Monica: Phoebe?

Phoebe: Yeah!

Monica: Can you just hold on for one minute?

Phoebe: No, you have to let me in right now!!

Monica: Are you alone?

Phoebe: Yes!

Monica: All right.

(She goes over and lets Phoebe bounce in wearing her own wedding dress.)

[Scene: Chandler and Joey's, Joey is complaining about going to the clinic.]

Joey: This sucks! I didn<U+0092>t know I had to stay up all night before I went to
this stupid sleep clinic! I<U+0092>m so tired!

Chandler: It<U+0092>s 6:00.

Joey: Yeah, well<U+0085>

Rachel: (entering) Hi!

Chandler: Hey, I hear that you and Joshua are going out to dinner with Ross and
Emily, and I think that<U+0092>s, I think that<U+0092>s really cool.

Joey: Yeah, Rach, I think you<U+0092>re handling that really well.

Rachel: Handling it? What do you mean, handling it? There<U+0092>s nothing to
handle. Now, maybe I would have a problem with this if it wasn<U+0092>t for me and Joshua.
Y<U+0092>know, they<U+0092>re not gonna get married anyway!

Chandler: What?

Rachel: Come on! They rushed into this thing so fast it<U+0092>s ridiculous! I
mean, they<U+0092>re gonna be engaged for like what? A year? And somewhere along the way,
one of them is gonna realise what they<U+0092>ve done and they<U+0092>re call the whole thing
off. I<U+0092>m telling ya, you<U+0092>re gonna be dancing at my wedding before you<U+0092>re
dancing at there<U+0092>s.

Chandler: Yeah, well, I don<U+0092>t dance at weddings.

Rachel: Why not?

Chandler: Because weddings are a great place to meet women, and when I dance, I
look like this<U+0085> (Starts to dancing really, really, really badly. Ross enters behind
him and he stops.)

Ross: Hey man.

Chandler: Hey!

Ross: So, what are you guys doing four weeks from today?

Chandler: Nothing.

Rachel: Nothing.

Joey: I am<U+0085> (Looks in his date book.) free!

Ross: Great! Because Emily and I are getting married in a month!


Joey and Chandler: What?!

Ross: Yep!

Rachel: In a month?

Ross: Yeah!

Rachel: You mean, you mean 30 days?

Ross: Yeah.

Rachel: From now? 

Ross: Yeah.

Rachel: Well, that<U+0092>s great.

Ross: Yeah! Yeah, Emily always wanted to get married in this beautiful place
that her parents got married, but it<U+0092>s going to be torn down, so<U+0085> I mean, I-I
know it<U+0092>s crazy, but everything up <U+0091>til now has been so crazy, and I don<U+0092>t
know, this just feels right. Y<U+0092>know?

Joey: (still looking in date book) Hey! That<U+0092>s the day after I stop
menstruating! (They all look at him.) This isn<U+0092>t mine.

Commercial Break

[Scene: Central Perk, Rachel is waiting impatiently for Joshua.]

Joshua: (entering) Hey, Rachel.

Rachel: Hi!

Joshua: What<U+0092>s up? You<U+0092>re voice sounded all squeaky on the phone.

Rachel: Ohh, nothing, I just wanted to see you. See you and hug you. (Hugs him)
See you.

Joshua: Great!

Rachel: Yeah! (She sits down) Sit!

Joshua: (sitting) You okay?

Rachel: I<U+0092>m more than okay, I am really, really happy! Wanna know why?

Joshua: Do I?

Rachel: <U+0091>Cause I am really happy about us. I think we are, I think we are
so on the right track! Y<U+0092>know? I mean, I think we are working, I think we are
clicking. Y<U+0092>know?

Joshua: Yeah, sure-sure, yeah,
we<U+0092>re-we<U+0092>re-we<U+0092>re-we<U+0092>re-we<U+0092>re clicking.

Rachel: Yeah-yeah, y<U+0092>know if-if there was just like one little area where
I<U+0097>that I think we need<U+0097>we would need to work on; I-I would think it was
we<U+0092>re just not crazy enough!

Joshua: I-I gotta say, I-I-I-I<U+0092>m not too sure I agree with that.

Rachel: Well, yeah, right, y<U+0092>know what? Yeah, you<U+0092>re right, I mean, we
no, we have our fun. Yeah! But if (Grunts uncomprehensively)<U+0085><U+0085>I mean, I mean
like craaaazy! Y<U+0092>know? Okay, all right. This is gonna, this is gonna sound
y<U+0092>know, a little umm, hasty, but uh, just go with it. Umm. Ugh. What if we got
married?

Joshua: What?! (Gunther is listening in.)

Rachel: Oh, I know, I know, it<U+0092>s-it<U+0092>s so, it<U+0092>s so totally like,
"Whoa! Can we do this?" Y<U+0092>know, I mean, but I mean it just feels right!
Don<U+0092>t you think? It does! I mean, it just feels right, don<U+0092>t you think?

Joshua: Wow! Uhh, Rachel uhh, you<U+0092>re a real special lady, but my divorce
isn<U+0092>t final yet and, and, and we<U+0092>ve been on four days, so I<U+0092>m thinking
"No, but thanks."

Gunther: YOU IDIOT!!!!!

[Scene: The Sleep Clinic, Joey is having trouble staying awake.]

Sleep Clinic Worker: Your name, please?

Joey: Joey Tribbiani.

Sleep Clinic Worker: Um-hmm, and did you stay up all night in preparation for
your sleep study. (Joey doesn<U+0092>t answer) Uh, sir? (Joey starts snoring)

Chandler: (answering for him) Yes he did.

Sleep Clinic Worker: Alll right, we<U+0092>ll call you in a few minutes.

(As she leaves, a beautiful woman enters and sits down across from the boys.)

Chandler: (waking Joey) Hey, check out that girl! She is really hot!

Joey: (sleepily) Yeah, she is. Wow! (Falls back asleep, loudly) How you
doin<U+0092>?

(Chandler wakes him up, again.)

Joey: What?!

Chandler: You<U+0092>re coming on to the entire room! (He goes over to pick up a
stack of magazines next to her, and to get her attention, he throws them back down.)
I<U+0092>m Chandler.

Woman: I<U+0092>m Marjorie.

Chandler: Hi.

Marjorie: Hi.

Chandler: You mind if I<U+0085>

Marjorie: No, please. 

(He sits down next to her.)

Chandler: So uh, what are you in for?

Marjorie: I talk in my sleep.

Chandler: What a coincidence, I listen in my sleep.

Joey: (asleep) So why don<U+0092>t you give me your number?

[Scene: Monica and Rachel's, Monica and Phoebe, still defying reality, are now throwing
a bouquet at each other, pretending to catch the actual bouquet at an actual
wedding.]

Monica: Okay, ready?

Phoebe: Yeah.

Monica: Okay.

(She turns around and throws the bouquet to Phoebe.)

Phoebe: (catching it) I got it! Mine! (They both hug)

Monica: Congratulations!

Phoebe: Thank you!

Monica: Okay! My turn! My turn!

Phoebe: Okay! (Gets into position) Okay, ready?

Monica: (cocking her head from side to side in some pre-bouquet-catching ritual)
Yeah.

Phoebe: Okay. (Phoebe turns and throws it on the couch.)

Monica: (upset) That was a terrible throw!!

Phoebe: I<U+0092>m not gonna right to you! That<U+0092>s not real!

Monica: Look at me! My big concern is what<U+0092>s real?! (Finally realises) Oh
my God. We<U+0092>re really sad, aren<U+0092>t we?

Phoebe: Yeah, I think we are.

Monica: This isn<U+0092>t even my dress.

Phoebe: Well, at least you didn<U+0092>t rent yours from a store called,
"It<U+0092>s Not Too Late."

Monica: I<U+0092>m changing out of this.

Phoebe: Me too.

Monica: In like a half-hour?

Phoebe: Me too.

Monica: Okay, throw it straight this time.

Phoebe: Okay.

(She throws it straight, and Monica makes a big deal about catching it.)

Monica: I<U+0092>m getting married next!!

Phoebe: Yay!

[Scene: Central Perk, Monica and Phoebe, back to reality, are sitting in normal
clothes.]

Phoebe: I hate my regular clothes now! Y<U+0092>know? I look down and-and I
know that this isn<U+0092>t gonna be the most special day of my life.

Monica: Yeah. I mean it was kinda fun for a while, but didn<U+0092>t you start
feeling silly?

Phoebe: I guess.

(Monica crosses her legs and is still wearing the garter belt.)

Phoebe: Oh my God! 

Monica: Oh God.

Phoebe: Oh, you<U+0092>re such a cheater!

Chandler: (entering) Hello! Little ones.

Monica: Hey!

Phoebe: Hey!

Monica: So, is Joey gonna stop snoring?

Chandler: Yep! And! A beautiful woman agreed to go out with me. (They<U+0092>re
stunned.) Joey wanted to ask her out, but uh, she picked me.

Phoebe: Oh, how<U+0092>d that happen?

Chandler: Because I<U+0092>m cooler.

Monica: No, seriously.

Chandler: Well she<U+0092>s, she<U+0092>s the kinda girl<U+0097>Joey was unconscious. 

(Joey enters, wearing a mouth guard like boxers wear.)

Joey: (muffled by the mouth guard) Hey you guys! What<U+0092>s happening?

Monica: Oh my God!

Phoebe: What is that?

Joey: (muffled) Oh, they gave it to me at the sleep clinic, and it<U+0092>s gonna
help me not to snore.

Monica: Well, are you asleep right now, Joe? <U+0091>Cause I don<U+0092>t think you
have to wear it unless you are!

Joey: (takes out the mouth guard) I know I don<U+0092>t have too! It tastes good.
(Puts it back in.)

Chandler: Plus, you look cool.

(Joey totally agrees with this statement and kicks his feet up.)

[Scene: Monica and Rachel's, Monica is putting away the wedding dress, finally.]

Rachel: (entering from her bedroom) Well, I just called Joshua<U+0085>

Phoebe: Oh, how did it go?

Rachel: Well, I did my best to convince him that I<U+0092>m not some crazy girl
who is dying to get married<U+0097>I<U+0092>m just going through a hard time.

Phoebe: What did he say?

Rachel: Well uh, his answering machine was very understanding. Ugh. I
feel blue.

Monica: Ohh, sweetie! (Goes to comfort her.) Hey, I bet you anything that
he<U+0092>s gonna call you again.

Rachel: Yeah, maybe, but I don<U+0092>t think I even care. I don<U+0092>t think
he<U+0092>s the one I<U+0092>m sad about. Y<U+0092>know, I know that I said that I am totally
okay with Ross getting married, but as it turns out, I don<U+0092>t think I<U+0092>m handling
it all that well.

Phoebe: Yeah, maybe.

Rachel: And I-I am just trying to figure out why.

Phoebe: Any luck?

Rachel: Well, yeah, y<U+0092>know how Ross and I were on again, off again, on
again, off again? I guess I just figured that somewhere down the road, we would be on
again.

Monica: Again. Y<U+0092>know what? I think we all did.

Ross: (entering) Hey!

Monica: Hey! (She jumps up and throws Emily<U+0092>s wedding dress into
Rachel<U+0092>s room.)

Ross: So, I got us some reservations for Sunday night, okay? How about,
Ernie<U+0092>s at 9 o<U+0092>clock?

Rachel: Yeah, well, you uh, better make it for three.

Ross: Oh, see I-I don<U+0092>t know if we<U+0092>re gonna be hungry at three.

Rachel: Three people. Joshua<U+0092>s not gonna be there.

Ross: What happened?

Rachel: Uh, well, I think, I think he broke up with me.

Ross: Noo. Why?

Rachel: Well, apparently he scares easy.

Ross: Oh, Rachel, I<U+0092>m-I<U+0092>m sorry.

Rachel: It<U+0092>s okay. Sometimes, things don<U+0092>t work out the way you<U+0092>d
thought they would.

Ross: Come here.

(They hug.)

Rachel: (breaking the hug) Oh, hey, don<U+0092>t you have to go pick up Emily?

Ross: Yeah. 

Rachel: Yeah.

Ross: You okay?

Rachel: Yeah! I got my girls.

(He leaves.)

Rachel: Ugh. (She goes over and lays her head on Phoebe<U+0092>s lap.)

Phoebe: (looks at Monica) Hey, y<U+0092>know what might cheer you up?

Rachel: What?

[Time lapse, all three girls are now wearing wedding dresses, eating popcorn, drinking
beer, and watching TV.]

Rachel: Y<U+0092>know, I gotta tell ya, this really does put in a better mood.

Monica: Oh, I wish there was a job where I could wear this all the time. (Pause)
Maybe someday, there will be.

(There<U+0092>s a knock on the door.)

Monica: Oh God! He<U+0092>s gonna come by and borrow some candles for his big
date!

Rachel: Oh, okay! (She goes to answer the door.)

Monica: No-no, Rachel, don<U+0092>t get it! He can<U+0092>t see us!

Phoebe: No, yeah! The groom cannot see the bride!

Rachel: I<U+0092>m not gonna marry Chandler!

Phoebe: Not after this!

Rachel: Okay, you guys, just relax. (She goes over to open the door, and as she
does, she says.) I doooo. (Sees that it<U+0092>s Joshua, not Chandler that knocked on the
door.)

Joshua: I gotta go.

Rachel: Oh, wait, Joshua! Joshua! (Pause) (Comes back inside) Yeah, well, that
oughta do it.

Closing Credits

[Scene: Chandler<U+0092>s bedroom, he is sleeping with Marjorie. All of the sudden,
Marjorie starts talking in her sleep, awakening Chandler. After a little bit, she quiets
back down, and Chandler tries to get back to sleep. There<U+0092>s a short pause until she
starts screaming, causing Chandler to scream with her. She quickly calms down. This all
wakes up Joey, who comes over wearing the mouth guard, opens the top half of
Chandler<U+0092>s door, and starts to complain about the noise.]

Joey: (muffled by the mouth guard) Dude! I am trying to sleep! (Shrugs to
say, "What<U+0092>s up with that?")

End

