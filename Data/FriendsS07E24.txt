

Friends: The Stuff You<U+0092>ve Never Seen

Hosted by: Conan O<U+0092>Brien
Transcribed by: Eric Aasen

This is a special out takes episode. The cast and Conan are sitting around the set of
Central Perk, talking about the stuff we<U+0092>ve never seen. 

Transcriber<U+0092>s Note: This is stuff we never saw from all of the seasons, so for all
of the scene settings I will be using the current arrangements. Even though some of the
out takes take place when Chandler was living with Joey and Rachel was living with Monica,
when Joey and Chandler were living in Monica and Rachel<U+0092>s, and the current
arrangements.

[Scene: Central Perk, the cast of Friends along with Conan O<U+0092>Brien are sitting and
talking.]

Conan: It<U+0092>s a tradition here on Friends after every taping for me to
hang out with you guys, (They all laugh) talk down the episode umm<U+0085> The point of this
whole thing is what people see in America is: they see Friends, they love the show,
it looks like a smooth running machine, but behind the scenes there<U+0092>s deceit,
mistrust, and hate. And I thought, I thought we<U+0092>d actually take a look at uh,
y<U+0092>know some of these moments where you guys are<U+0097>there are mistakes. You make
mistakes.

Jennifer: Once and a while.

Lisa: From time to time.

Conan: For example, I don<U+0092>t have to memorize lines. You guys actually have
to remember what to say and you probably forget from time to time. Yes?

Matthew: Our energy just comes way up when there<U+0092>s an audience here and
when that happens, something happens between your brain and your mouth sometimes and it
just doesn<U+0092>t, it just doesn<U+0092>t work.

[Cut to Central Perk, Ross, Phoebe, Monica, and Chandler are there. I think it<U+0092>s
The One With The Joke.]

Ross: Uh, oh-oh, umm no you didn<U+0092>t. I did.

Chandler: Oh uh-uh, no-no-no-no-uh-uh. (He starts laughing, causing everyone
else to laugh.)

[Cut to Joey and Rachel's, Phoebe is talking. It looks like when Rachel and Monica
lived in this apartment.]

Phoebe: So, we realize that<U+0097>Oh no<U+0085> (She resets herself) I<U+0092>m
telling it! I<U+0092>m telling it<U+0085> (She loses it.)

[Cut to Monica and Chandler's, Joey is talking to Monica and Chandler.]

Joey: Ha-ha. Look<U+0097>Come on, I don<U+0092>t know what to do<U+0085>or say. (He
laughs.)

[Reset]

Joey: Ha-ha, very funny. I don<U+0092>t know what to do! Y<U+0092>know? Holy crud!

[Reset]

Joey: Ha-ha-ha, very funny. Look, I don<U+0092>t know what to do! (Long pause, as
everyone cracks up.)

Courtney: It is one of those days!

[Cut to Monica and Chandler's, Phoebe is speaking Italian to Joey<U+0092>s grandmother.
I<U+0092>m spelling phonetically.]

Phoebe: <U+0091>Xcusa seniora, voulez-bere quakay<U+0097>[Beep]<U+0097>uck it!

Matt: Wow Pheebs, you-you speak gutter?

[Cut back to the cast and Conan.]

Conan: You still get nervous everybody just before a show?

Matthew: Absolutely.

Lisa: Everybody.

Courtney: It<U+0092>s amazing like all week long we<U+0092>ve-we<U+0092>ve been saying
the same lines and then the audience is here and we will mess up, and if you mess up once,
then you<U+0092>ll get nervous because you<U+0092>ll<U+0097>you know you<U+0092>ll probably mess
up again.

[Cut to Central Perk, first season Monica is talking.]

The Director: Action!

Monica: (holding her hand in front of her face) When you were little you slept
through-through the Grand Canyon.

The Director: Watch again that hand.

Courtney: This<U+0092>ll be five/ten takes.

The Director: Okay.

Courtney: Okay. You know it<U+0092>s gonna happen.

The Director: Once again, and action!

Monica: (the hand<U+0092>s still there) When-when you were little you slept
through the Grand Canyon. (She actually itches her nose this time.)

Courtney: Oh! Okay! I<U+0092>m gonna try it without the coffee cup <U+0091>cause I
think it<U+0092>s the left hand that<U+0092>s messing me up.

[Reset]

Monica: When you were little you slept through the Grand<U+0097>(Pointing
again)<U+0097>Oh fffff<U+0085>.

[Cut to Monica and Chandler's, Monica and Phoebe are talking.]

Monica: Sorry, let<U+0092>s go back! <U+0091>Cause you<U+0092>ve got more to say.

Lisa: Do I? Absolutely. Absolutely. Yeah, that<U+0092>s your fault. I<U+0085>say.

(They both laugh.)

Lisa: I love you.

Courtney: I love you! (They hug.)

[Cut back to the cast and Conan.]

Conan: But there must be, there must<U+0097>are a lot of moments over the years
where you<U+0092>re just trying to do your job, something goes wrong.

[Cut to Joey and Rachel's, Joey is giving Chandler the bracelet from season 2.]

Joey: Hey and, this is a little extra something for y<U+0092>know, always being
there for me.

(Suddenly there<U+0092>s a noise off stage and the camera on Joey swings around.)

Matthew: Hey Joey! The camera hit our wall!

Matt: What?!

[Cut to Monica and Chandler's, Chandler and Joey are living here and Ross is writing on
the Magna-Doodle when Rachel opens the door causing the door knob to hit his hand.]

David: Ahh! Sh<U+0097>(Beep)<U+0097>it that hurt!

Jennifer: Are you okay?

[Reset]

[When he hears Jennifer try to open the door he jumps back, and Jennifer is unable to
open the door this time.]

[Cut the hallway, Rachel is exited from Monica<U+0092>s when the door closes on her
skirt.]

Chandler: Hey Rach! (She breaks up and goes back into Monica<U+0092>s.)

[Cut to Joey and Rachel's, with the giant entertainment center Joey is exiting from his
room.]

Joey: Oh yeah? Then how come I keep<U+0097>(He notices that the marker board they
use has been left on the entertainment center and holds up his discovery.)

[Cut to Monica and Chandler's, first season, Monica is making a giant sub-sandwich and
is talking to Rachel. I think it<U+0092>s The One With Fake Monica.]

Monica: It was so wild! We told <U+0091>em we were the Gunderson<U+0092>s in
16<U+0085>

(Jennifer starts laughing.)

Courtney: (laughing) I spit on her!

[Cut back to the cast and Conan.]

Jennifer: (to Lisa) Operation. You had a fun one.

Lisa: Oh yeah!

Jennifer: With Operation.

Conan: It was a little game. Yeah, with an electric buzzer.

[Cut to Monica and Chandler's, The One With George Stephanopoulos, Phoebe is showing
Monica and Rachel that she brought Operation to their slumber party.]

Phoebe: Oh-ooh, and I brought Operation, but umm I lost the umm<U+0085> (It
starts buzzing) It<U+0092>s making a noise.

[Cut back to the cast and Conan.]

Lisa: But le Blanc really doesn<U+0092>t mess up much.

Conan: You don<U+0092>t verbally mess up, but sometimes physically? You mess up.

Matt: I have had some clumsy moments I guess you can call <U+0091>em.

[Cut to Central Perk, to the theme from The Dick Van Dyke show Joey runs into
Central Perk carrying a stack of Soap Opera Digests and falls on the step. He does
bounce right back up making it all that much funnier.]

[Cut back to the cast and Conan.]

Lisa: He fell down once! And we re-did it and we went back. And
he<U+0097>(laughs)<U+0097>he was afraid he was gonna fall down<U+0085>

Conan: You could actually see him trying not to fall down.

[Cut to Central Perk, same as before Joey is entering.]

Joey: Pheebs! (He looks down as he goes down the step to make sure he
didn<U+0092>t fall again.) Check it out! (He starts laughing when he realized what he did.)

[Cut back to the cast and Conan.]

Courtney: This particular time when he continued to fall or y<U+0092>know, try not
to fall, I was in the room with Matthew and Matthew was like, "Should I do it?"

[Reset from before, Matt doesn<U+0092>t fall or look down.]

Joey: Pheebs! Check it out! Check it out! Check it out! Check it out! (Hands her
one.)

Phoebe: Ooh, Soap Opera Digest!

(As she<U+0092>s saying that Joey is to pull out a chair and sit down, only Matthew comes
running in from off camera and dives for the same chair.)

[Cut back to the cast and Conan.]

Conan: Matthew, you have a reputation with the rest of the cast that sometimes
you like to, you like to fool around a bit. I mean like if something<U+0092>s naturally
going wrong you like to get in there and juice it a little bit. True or false?

Matthew: I don<U+0092>t necessarily like to juice things<U+0085>

Courtney: He said true or false!

[Cut to Monica<U+0092>s restaurant kitchen, it<U+0092>s the episode where Joey is working
as a waiter at Monica<U+0092>s restaurant. Joey is patting her breast from when she set it
on fire.]

Joey: Oh! Ooh-ooh!

Monica: What are you doing?!

Joey: You<U+0092>re still a tiny bit on fire there.

Monica: Oh geez! Okay! Thanks!

(Matthew runs in and starts patting the other breast, then walks away. Matt slowly
stops patting her breast.)

[Cut to that same kitchen, only this is The One With The Proposal, Richard is telling
Monica something.]

Richard: It was great seeing you the other night.

Monica: Well, it was good to see you too. Did you come down here to tell me
that?

Richard: Noo! I came down here to tell you something else. I came here
to tell you I still love you.

(Suddenly the door flies open and in walks Chandler!!)

Chandler: What the fu<U+0097>(beep)<U+0097>ck are you doing?!

[Cut back to the cast and Conan.]

Conan: Now you guys work with animals a lot. You had to work early on with a
monkey<U+0085>

Jennifer: That damn monkey.

Conan: That damn monkey.

[Cut to Monica and Chandler's, The One Where The Monkey Gets Away, Rachel is watching a
soap opera with Marcel.]

Rachel: Okay. Okay, see now the one with the feather boa? That<U+0092>s Dr.
Francis. She used to be a man. Oh look! There<U+0097>(Marcel (Katie) jumps away)<U+0097>Okay.
(And runs behind her on the back of the couch for a little while.) 

Jennifer: Katie. Geez! See Katie, come here<U+0097>Katie! (Katie spills some
popcorn.) Katie, come here Katie. (She obeys and sits where she<U+0092>s supposed to.) Very
good.

[Reset, they<U+0092>re about to start the scene when Katie suddenly jumps up startling
Jennifer.]

[Cut back to the cast and Conan.]

Jennifer: Lisa<U+0092>s laugh though<U+0085>There<U+0092>s<U+0097>It<U+0092>s so
infectious. It<U+0092>s one of those things<U+0085>just forget about it. Once it starts,
it<U+0092>s all gone.

[Cut to Joey and Rachel's, it<U+0092>s actually Joey and Rachel<U+0092>s. Phoebe is trying
to convince Rachel to switch with her and live with Monica as Joey looks on.]

Rachel: No y<U+0092>know, I don<U+0092>t want to switch! Come on! I can throw wet
paper towels here!

Phoebe: Well at Monica<U+0092>s you can eat<U+0097>(Suddenly cracks up.)

Lisa: Okay, shh yeah. <U+0091>Cause it<U+0092>s not silly.

The Director: Here we go.

Jennifer: Okay.

The Director: And<U+0085>action!

Rachel: No-no Ph-Ph-Ph-Ph<U+0085> (She starts laughing.)

[Reset]

Jennifer: (to Lisa) Ready?

Lisa: Yeah.

(They all start laughing.)

[Cut back to the cast and Conan.]

Conan: And then it just builds on itself and there<U+0092>s no doing the scene
after. I mean you probably wait and really get it together and do it.

David: For me, I have a hard time with le Blanc in particular. When-when<U+0097>I
mean when<U+0085>

Conan: (to Matt) You bastard.

Matt: Yeah, what did I do?

David: <U+0085>I keep a straight face he-he delivers like this look, a reaction to
you, or a certain take, I-I<U+0097>I mean I find it so funny.

[Cut to Monica and Chandler's, Ross is eating breakfast with Joey and Monica. Joey is
walking towards Ross.]

Joey: Mornin<U+0092>

Ross: Mornin<U+0092>

(Joey hugs Ross<U+0092>s neck and has a look of complete contentment on his face which,
after a short while, causes David and Matt to start laughing.)

[Cut to Monica and Chandler's, Monica is throwing a party. Joey is talking to Ross
about the bad audition he just had while pouring booze onto a snow cone.]

Joey: I mean what kind of an actor, what kind of an actor can<U+0092>t even say,
"Hmm, noodle soup?"

(David smiles.)

Matt: That<U+0092>s a good one? (They both laugh.)

David: Now, we should go back and take the other line.

Matt: Okay, I<U+0092>ll just put a little more booze on there. (Pours some more
on.)

Ross: I think, subconsciously<U+0085>

Joey: (cracking up) Nope, you lost me.

[Reset, Joey is about to pour more booze on.]

David: No! Come on!

[Cut to Joey and Rachel's, Joey and Ross are giving Phoebe and Rachel the bride<U+0092>s
maid test.]

Joey: Okay, the next situation is for Rachel. The wedding is about to start you
walk into the back room and you find Monica taking a nap with Ross. (Ross lies on the
floor.) I<U+0092>ll be Monica. Go! (He jumps down and cuddles up with Ross.)

Ross: (jumping up) No! No! No!

(David and Matt just lose it then.)

[Cut back to the cast and Conan.]

Conan: We<U+0092>ll be right back with more Friends, less me.

[Cut to Monica and Chandler's, Courtney and Matthew are getting ready to do a scene
where Monica<U+0092>s sick.]

Courtney: I<U+0092>m doing my brother.

Matthew: That<U+0092>s<U+0085>gross.

Commercial Break

[Conan and the cast in Central Perk.]

Conan: You uh, you<U+0092>ve worked with<U+0097>They always say a performer should
never work with pets or children.

Jennifer: Aww, the kids.

[Cut to Carol and Susan<U+0092>s apartment, from next week<U+0092>s episode Rachel is
talking to Ben.]

The Director: Action.

Rachel: Okay. Now<U+0097>What is my first line?

Ben: (prompting her) What did we agree?

Jennifer: Okay<U+0085>

[Cut back to the cast and Conan.]

David: The good thing about the young kids though, they<U+0092>re completely
unpredictable. Which is a lot of fun as an actor to respond with. But there was one
story<U+0085>

Jennifer: Oh little Ben.

David: Where the story was that I was anticipating that I would be around to
hear my son<U+0092>s first words spoken. But the scene was about that he wasn<U+0092>t
supposed to be able to speak and, uh for some reason when we started doing the show<U+0085>

[Cut to Central Perk, Ross is taking Ben to visit Rachel who<U+0092>s working there.]

Ross: Hi! We<U+0092>re visiting!

Rachel: Yay!

Ross: It<U+0092>s Ben and his Da-Da. Da-Da? Can you say Da-Da? Y<U+0092>know, you
might as well say it because I told your<U+0085>

Ben: Da<U+0085>Da.

[Reset]

Ross: Can you say Da-Da? See, I<U+0092>m gonna tell your mommies you said it
anyway, so you might as well try<U+0085>

Ben: Da<U+0085>Da.

Ross: Huh?

Ben: Da<U+0085>Da.

David: Yeah, he<U+0092>s saying Da-Da.

[Cut back to the cast and Conan.]

Matt: And then sometimes during the show y<U+0092>know but you<U+0092>re like, the
scene<U+0092>s going one way but you<U+0092>re just tempted to say something another time.
Like, do you remember that one where Monica<U+0092>s baking cookies in our old apartment?

Jennifer: She<U+0092>s trying to intrigue us to hang out with her.

Matt: Yeah, she<U+0092>s trying to waft the smell across the hall to get us to
come hang out in her new place, and we<U+0092>re sitting there eating pizza and I think it
was you (Points to Lisa) that said<U+0085>

[Cut to Monica and Chandler's, the scene described above.]

Phoebe: What do I smell?

[Cut back to the cast and Conan.]

Matt: I think I was supposed to say, "I don<U+0092>t know," and go over
and open the door. And I went<U+0085>

[Cut to Monica and Chandler's, same scene.] 

Phoebe: What do I smell?

Joey: (Pause) Sorry. (Raises his hand in shame.)

[Cut back to the cast and Conan.]

Matt: (everyone laughs) And then it was like four takes later before we could
get through it with a straight face.

[Cut to Monica and Chandler's, same scene.] 

Phoebe: What do I smell?

(Pause.)

David: Le Blanc.

[Reset] 

Phoebe: What do I smell?

Joey: I don<U+0092>t know, but it smells good. (He gets up and heads for the door
only to stop short and start laughing.)

[Cut back to the cast and Conan.]

Conan: But audiences<U+0097>You have a live studio audience and they must love
that. They must love it when they see you guys playing.

[Cut to Joey and Rachel's, Ross is living with Chandler and Joey. Joey and Ross have
built a fort out of boxes, Chandler enters and they stand up slowly.]

Chandler: What are you guys doing?

(Ross and Joey both reach down and pull up their pants.)

Ross: Nothing.

Joey: Nothin<U+0092>.

[Cut back to the cast and Conan.]

Conan: So that you will intentionally do something
that<U+0097>they<U+0092>ll-they<U+0092>ll intentionally screw it up?

Courtney: Yeah!

David: Yeah!

[Cut to Monica and Ross leaving Joey and Chandler<U+0092>s hotel room in London. As they
exit Joey and Chandler enter from the bathroom with both of their pants down around their
ankles.]

Matt: (noticing the laughter) Why? What<U+0092>s the matter?

(They both bounce back into the bathroom.)

[Cut back to the cast and Conan.]

Conan: (to Courtney) You-you<U+0092>ve worn a fat suit on the show. And, a lot of
people love you in the fat suit. Do you like wearing the fat suit? Is it fun?

[Cut to Central Perk, Fat Monica and Rachel are on the couch.]

Courtney: I<U+0092>m melting!

[Cut back to the cast and Conan.]

Courtney: They made me dance, in the fat suit.

[Cut to Monica and Chandler's, Courtney is dancing in the fat suit and after shaking
her groove thing sits down in exhaustion.]

[Cut back to the cast and Conan.]

Conan: I-I heard some of you guys talking about this earlier, but sometimes
there<U+0092>s just a word that someone has to say that you<U+0092>ll get hung up on. And
it<U+0092>ll just<U+0097>the way you say the word is funny to everybody else.

[Cut to Monica and Chandler's, is the one where Rachel screwed up the desert and Ross
and Joey are trying to enjoy it.]

Ross: That tastes like feet!

(David puts his napkin up to his mouth and starts laughing at his own line. Matt
notices him after a while and starts laughing as well.)

Matt: It tastes like (mimicking him in a high-pitched voice) feet!

David: Okay, we<U+0092>re good.

The Director: Action!

Ross: That tastes like feet!

(Matt starts laughing.)

[Cut back to the cast and Conan.]

Matt: Sometimes the dialogue itself is just so funny and
you<U+0092>ll<U+0097>we<U+0092>ll be rehearsing during the week and you
just<U+0097>whatever-whatever the joke is; it<U+0092>s so funny we can<U+0092>t get through it
in rehearsal and just<U+0085>

Jennifer: You just know.

Matt: You-you mentally make a flag on it and you say, "Okay show night,
I<U+0092>m just<U+0097>I<U+0092>ll never be able to get through this."

[Cut to Monica and Chandler's, Sick Monica is trying to entice Chandler to have sex
with her.]

Monica: Are you saying that you don<U+0092>t want to get with this?

Chandler: You see, I don<U+0092>t say<U+0097>(Starts laughing.)

[Cut to Monica and Chandler's, it<U+0092>s the one with the fake chocolate. Monica has
baked some cookies and Phoebe is trying them.]

Phoebe: (spitting the cookie out onto a napkin) Oh, sweet
Je<U+0097>(Beep)<U+0097>sus! Oh! Monica, these are the (laughing) cookies they serve in hell!

[Cut to Monica and Chandler's, the gang is watching Joey<U+0092>s debut on Days.]

Chandler: (noticing the woman on the screen) Whoa! She<U+0092>s purty!

Joey: Oh yeah, and she<U+0092>s really nice too! She taught me about y<U+0092>know,
how to work with the cameras and smell-the-fart acting.

Monica: Joey?

Rachel: I<U+0092>m sorry<U+0085>

Ross: Excuse me, what?

(Matt turns back and looks and them, but instead of his next line he starts laughing.)

[Cut back to the cast and Conan.]

Conan: When you have to do physical business for a scene, I mean there must be;
there must be a lot of funny moments when you have to physically do a task as part of a
scene.

David: Yeah, Rachel, Chandler, and Ross had to try to get a couch up a
stair<U+0097>a very narrow New York stairwell and that was probably<U+0085>I-I think it was
the hardest I<U+0092>ve-I<U+0092>ve laughed in my life period.

[Cut to the scene described above.]

Ross: Pivot! (They pivot) Pivot! (They pivot) Okay, pivot! Piv-at! (He starts
laughing.)

[Reset]

Ross: Pivot! Piv-at! Piv-at!!

Chandler: Shut up! Shut up! Shut up!!

(David is laughing.)

David: Pivat!! (In a high pitched voice) Pivat!!!

Commercial Break

[Cut back to the cast and Conan.]

Conan: You<U+0092>ve done over 150 episodes, but your favorite moments that 80
years from now you<U+0092>ll be thinking about?

Matthew: It is when we<U+0092>re able to crack each other up.

[Cut to Monica and Chandler's, Chandler is cleaning the apartment for Monica and is
frustrated with things not lining up.]

Chandler: Monica<U+0092>s gotta have the phone in the right place
and<U+0097>(Frantic babbling.)

Ross: (To Chandler) All right! All right! All right! (To Joey) We are
fixing it. 

(Matt starts laughing.)

David: He<U+0092>s gone. He<U+0092>s<U+0085> 

(Matt mimics Chandler.)

[Cut back to the cast and Conan.]

David: We enjoy watching each other. And I settle for watching each other<U+0092>s
performance, and we like each other.

[Cut to Ross<U+0092>s second wedding reception, Joey has just told him the band is ready
with Rachel looking on.]

Ross: Oh the band<U+0092>s ready! And well<U+0097>I-I<U+0097>we gotta do what the band
says, right? I don<U+0092>t care about the stinkin<U+0092> band!!

(Jennifer starts laughing.)

[Cut back to the cast and Conan.]

Conan: Okay Friends gang, thanks for doing it.

David: Well thank you so much.

Jennifer: Thank you Conan!

Matthew: Well thank for coming here, it<U+0092>s good to see you.

[Cut to Central Perk, Phoebe at the mike.]

Phoebe: Thank you my babies. (Waves good-bye.)

Ending Credits

[Cut to Monica<U+0092>s work kitchen, she<U+0092>s on fire again and Joey is putting her
out.]

Monica: Oh what are doing?!

Joey: You<U+0092>re still a tiny bit on fire there!

Monica: I think you got it!

Courtney: I<U+0092>m sorry, I messed up.

Matt: I<U+0092>ll do it again and again if you want.

[Cut to Monica and Chandler's, the bad desert Ross is quickly eating the mound on his
plate.]

Ross: No-no! I<U+0092>ll-I<U+0092>ll<U+0085> (He takes too much and some falls out of
his mouth, which starts him laughing.)

David: That is too much!

(Matt grabs his plate and takes some of what<U+0092>s on Ross<U+0092>s plate.)

[We close with a bunch of scenes where they screw up and make weird noises. It finishes
with.]

Matthew: Let me start that again.

End

