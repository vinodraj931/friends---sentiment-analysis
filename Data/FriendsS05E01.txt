

The One After Ross Says Rachel

Written by: Seth Kurland
Transcribed by: Eric Aasen

[Scene: Ross<U+0092>s Wedding, continued from last season, the Minister is about to marry
Ross and Emily.]

Minister: Friends. Family. We are gathered to celebrate here today the joyous
union of Ross and Emily. (Time lapse) Now Ross, repeat after me. I Ross...

Ross: I Ross...

Minister: Take thee, Emily...

Ross: Take thee, Rachel...(All his friends have looks of shock on their faces.
He realizes what he said. Quickly he says.) Emily. (A slight chuckle.) Emily.

Minister: (Looking and feeling awkward. he looks towards Emily.) Uhh...Shall I
go on?

Rachel: (To the woman sitting in front of her) He-he said Rachel, right? Do you
think I should go up there?

Emily: Yes, yes, do go on.

Minister: I think we<U+0092>d better start again. Ross, repeat after me. I,
Ross<U+0085>

Ross: I, Ross<U+0085>

Minister: Take thee, EM-I-LY<U+0085>

Ross: Take thee, (Glares at the Minister) Emily. (Chuckles) Like
there<U+0092>d be anybody else. (Emily is glaring at him.)

Minister: As my lawfully wedded wife, in sickness and in health, till death
parts us.

Ross: As my lawfully wedded wife, in sickness and in health, until death parts
us. Really, I do. Emily. (Points at her.)

Minister: May I have the rings? (He is given the rings) Emily, place this ring
on Ross<U+0092>s finger as a symbol of your bond everlasting. (She jams the ring onto his
finger) Ross, place this ring in Emily<U+0092>s hand as a symbol of the love that encircles
you forever.

Ross: Happy too.

Minister: Ross and Emily have made their declarations and it gives me great
pleasure to declare them husband and wife.

Ross: Yay!

Minister: You may kiss the bride.

(He goes to kiss her, but she isn<U+0092>t very receptive of the kiss. She keeps avoiding
him, until Ross finally gets to kiss her on her cheek.)

Mrs. Geller: (To Mr. Geller) This is worse than when he married the lesbian.

(The band starts to play, and the recessional starts. Ross tries to take Emily<U+0092>s
hand, but she snatches it away from him.)

Emily: Just keep smiling.

Ross: Okay.

Joey: Well, that went well. Yeah.

Chandler: It could<U+0092>ve been worse, he could<U+0092>ve shot her.

(Ross and Emily make it to the lobby.)

Ross: (laughs) That uh, that was pretty funny. Wasn<U+0092>t it?

(Emily gives him a forearm shot across the stomach.)

Opening Credits

[Scene: The Wedding reception, Ross and Emily are in the bathroom and Emily is yelling
at him. Rachel, Chandler, Joey, and Monica are standing outside the doorway.]

Emily: (Yelling from inside the bathroom) You<U+0092>ve spoiled everything!
It<U+0092>s like a nightmare! My friends and family are out there! How can I face them?! How
can you do this to me?!

Joey: (To the gang) Hey, no matter what happens with Ross and Emily, we still
get cake right?

Ross: (exiting the bathroom) That-that-that<U+0092>s all right, no honey, you take
your time sweetie. I<U+0092>ll be right out here. (She slams the door in his face, to the
gang) She<U+0092>s just fixing her makeup.

Emily: I hate you!!

Ross: And, I love you!! (He walks into the living room)

Mr. Geller: Boy, bad time to say the wrong name, huh Ross?

Ross: That<U+0092>s true, thanks dad. (To All) People should be dancing! Huh? Hey,
this is a party! Come on! Joey, dance!! (He starts to dance but stops when no one else
joins him.)

(Mrs. Waltham<U+0092>s phone rings and she answers it.)

Mrs. Waltham: Yes, Waltham interiors.

Phoebe: (On the phone, in New York) Uh, hello, this is Ross Geller<U+0092>s
personal physician, Dr. Philange.

Mrs. Waltham: Who?

Phoebe: Yeah, I<U+0092>ve discovered that Ross forgot to take his brain medicine,
uh, now without it, uh, in the brain of Ross, uh women<U+0092>s names are interchangeable,
through-through no fault of his own.

Mrs. Waltham: Oh my God, Phoebe.

Phoebe: No, not Phoebe, Dr. Philange. Oh no! You have it too!

(Mrs. Waltham hangs up on her.)

Phoebe: Hello?

(Cut to Chandler and Monica at the buffet table.)

Chandler: Hey.

Monica: Hey.

Chandler: Oh wow, I hope you don<U+0092>t take this the wrong way but, I know we
had plans to meet up tonight and, ugh, I<U+0092>m just kinda worried about what it might do
to our friendship.

Monica: I know. How could we have let this happen?

Chandler: Seven times!

Monica: Ugh! Well, y<U+0092>know, we were away<U+0085>

Chandler: In a foreign, romantic country<U+0085>

Monica: I blame London.

Chandler: Bad London! (Takes a spoon and smacks the turkey.)

Monica: So look umm, while we<U+0092>re st-still in London, I mean, we can keep
doing it right?

Chandler: Well, I don<U+0092>t see that we have a choice. But, when we<U+0092>re
back home, we don<U+0092>t do it.

Monica: Only here.

Chandler: Y<U+0092>know, I saw a wine cellar downstairs<U+0085>

Monica: I<U+0092>ll meet you there in two minutes.

Chandler: Okay! 

(He throws down his plate and runs to the wine cellar, Monica is about to follow him
but is intercepted by Rachel.)

Rachel: Mon, honey, I gotta ask you something.

Monica: (impatiently) Now?

Rachel: Ross said my name up there, I mean, come on, I just can<U+0092>t pretend
that didn<U+0092>t happen can I?

Monica: Oh, I-I don<U+0092>t know.

Rachel: Monica, what should I do?

Monica: Just uh, do the right thing. (Uses some breath spray)

Rachel: What?

Monica: Toe the line. Thread the needle. Think outside the box! (Tries to leave,
but is stopped by Rachel.)

Rachel: Whoa, wait, listen, I think I<U+0092>m just gonna talk to Ross about what
he think it meant.

Monica: Wait. Rachel, no, he<U+0092>s married. Married! If you don<U+0092>t realize
that, I can<U+0092>t help you.

Rachel: Okay, you<U+0092>re right. You<U+0092>re right. You can<U+0092>t help me.

(Cut to Mr. and Mrs. Geller.)

Mrs. Geller: Jack, is it all our fault? Were we bad parents?

Mr. Waltham: (walking by) Yes.

Mr. Geller: Oh yeah, well who serves steak when there<U+0092>s no place to sit, I
mean how are you supposed to eat this?

Joey: Hey, what<U+0092>s up? (He has solved the problem of eating the steak,
he<U+0092>s eating it with his hands.)

(Cut to Monica and Chandler, Monica is running up to him.)

Monica: Where were you? We were supposed to meet in the wine cellar?

Chandler: Forget it, that<U+0092>s off.

Monica: Why?!

Mr. Waltham: (drunkenly) The next tour of the wine cellar will plan in two in-in
minutes<U+0085>

(Joey walks up to them.)

Monica: Joey, what are you doing? You promised Phoebe you wouldn<U+0092>t eat meat
until she has the babies!

Joey: Well, I figured we<U+0092>re in another country, so it doesn<U+0092>t count.

Monica: That<U+0092>s true.

Chandler: The man<U+0092>s got a point.

(Cut to Rachel and Ross.)

Rachel: Oh, hi!

Ross: Hi!

Rachel: Hi. Sorry, things aren<U+0092>t working out so well.

Ross: Oh no! It could be better, but it<U+0092>s gonna be okay, right?

Rachel: Oh yeah! Of course, I mean, she<U+0092>s gonna get over this, y<U+0092>know?
I mean, so you said my name! Y<U+0092>know you just said it <U+0091>cause you saw me there, if
you<U+0092>d have seen a circus freak, you would<U+0092>ve said, "I take thee circus
freak." Y<U+0092>know, it didn<U+0092>t mean anything, it<U+0092>s just a mistake. It
didn<U+0092>t mean anything. Right?

Ross: No! No! Of course it didn<U+0092>t mean anything! I mean, uh well, I can
understand why Emily would think it meant something, y<U+0092>know, because-because it was
you<U+0085>

Rachel: Right<U+0085>

Ross: But it absolutely didn<U+0092>t. (Yelling towards the bathroom) It
didn<U+0092>t!! It didn<U+0092>t!!

Joey: (approaching) Ross, hey, the band<U+0092>s ready outside for your first
dance with Emily, so<U+0085>

Ross: (sarcastic) Oh! Oh-oh, the band<U+0092>s ready! Well, I-I-we gotta do what
the band says<U+0097>I don<U+0092>t care about the stupid band!!

Joey: You spit on me man! (Wipes his face.)

Ross: Look, I<U+0092>m sorry.

Joey: Emily is kinda taking a long time, huh?

Rachel: (laughs) Y<U+0092>know when I locked myself in the bathroom at my wedding,
it was because I was trying to pop the window out of the frame.

Ross: Oh, right!

Rachel: Get the hell out of there, y<U+0092>know?

(They all start laughing, and quickly stop when they realize what she just said and run
over to the bathroom.)

Ross: (Bangs on the bathroom door) Emily? Emily? I<U+0092>m coming in. (He opens
the door to reveal that the window is gone, along with Emily.)

Rachel: Well, look at that, same thing.

[Scene: London Marriott, Monica and Chandler are walking to her room.]

Chandler: Listen, in the middle of everything if I scream the word,
"Yippee!" just ignore me.

(She laughs and opens the door to reveal Rachel sitting on the bed.)

Monica: Oh my God, Rachel! Hi!

Chandler: Oh, hello Rachel.

Rachel: Ross said my name. Okay? My name. Ross said my name up there that
obviously means that he still loves me! (They both just stare at her.) Okay, don<U+0092>t
believe me, I know I<U+0092>m right<U+0097>do you guys want to go downstairs and get a drink?

Chandler: Yes, we do. But, we have to change first.

Monica: Yes, I want to change. And why-why don<U+0092>t you go down and get us a
table?

Chandler: Yeah, we<U+0092>ll be down in like five minutes.

Monica: (elbows him) Fifteen minutes.

Rachel: Okay.

(The phone rings and Rachel answers it.)

Rachel: Hello? Oh, Pheebs! (To them) It<U+0092>s Phoebe!

Chandler: Oh, yay<U+0085>

Monica: Great<U+0085>

Rachel: Hi!

Phoebe: Hi, so what happened?

Rachel: Well, Ross said my name.

Phoebe: Yeah, I know, but I don<U+0092>t think that means anything.

Rachel: Okay, Pheebs, y<U+0092>know what, let<U+0092>s look at this objectively all
right? Ninth grade, right? The obsession starts. All right? The summer after ninth grade
he sees me in a two-piece for the first time, his obsession begins to grow. So then<U+0085>

Chandler: (To Monica) Hey, listen, why don<U+0092>t we go change in my room?

Monica: But my clothes are<U+0097>ohh! (They both leave.)

(Cut to Chandler<U+0092>s room, he opens the door slowly to see if Joey is there and
after seeing that he isn<U+0092>t, ushers Monica into the room, closes the door, and the
security bar.)

Chandler: Wow, you look<U+0085>

Monica: No time for that!

(They both start to frantically rip each other<U+0092>s clothes off, but are interrupted
when Joey tries to open the door.)

Joey: Hey, dude, let me in. I got a girl out here!

Chandler: Well, I<U+0092>ve got a girl in here.

Joey: No you don<U+0092>t, I just saw you go in there with Monica!

Chandler: Well, we<U+0092>re-we<U+0092>re hanging out in here!

Joey: Look, which one of us is gonna be having sex in there, me or you?

Chandler: Well, I suppose I<U+0092>d have to say you!! But, what if we<U+0092>re
watching a movie in here?

Monica: Which we are, and-and we already paid for it. It<U+0092>s My Giant!

Joey: My Giant? I love that movie!

[Scene: Ross and Emily<U+0092>s room, Chandler and Monica are still looking for a place
to do the deed.]

Monica: You really think this is okay?

Chandler: Well, Ross and Emily aren<U+0092>t gonna use it.

Monica: Oh, it<U+0092>s so beautiful. Ohh! Y<U+0092>know, I-I don<U+0092>t know if I
feel right about this.

Chandler: Oh Mon-Mon-Mon-Mon-look, this is the honeymoon suite. The room
expects sex. The room would be disappointed if it didn<U+0092>t get sex. All of the
other honeymoon suites would think it was a loser.

Monica: Okay!

Chandler: Okay!

(They both run to rip the covers off the bed, but are interrupted by Ross.)

Ross: (entering) Emily?!

Chandler: Nope, not under here!

Monica: You didn<U+0092>t find her?

Ross: No, I<U+0092>ve looked everywhere!

Chandler: Well, you couldn<U+0092>t have looked everywhere or else you
would<U+0092>ve found her!

Monica: Yeah, I think you should keep looking!

Chandler: Yeah, for about 30 minutes.

Monica: Or 45.

Chandler: Wow, in 45 minutes you can find her twice. (Monica smiles at that.)

Ross: No! For all I know, she<U+0092>s trying to find me but couldn<U+0092>t because
I kept moving around. No, from now on, I<U+0092>m staying in one place. (He sits down on the
bed.) Right here.

Monica: Well, it<U+0092>s getting late.

Chandler: Yeah, we<U+0092>re gonna go.

Ross: Actually, do you guys mind staying here for a while?

Monica: Ugh, y<U+0092>know, umm we gotta get up early and catch that plane for New
York.

Chandler: Yeah, it<U+0092>s a very large plane.

Ross: (disappointed) That<U+0092>s cool.

Chandler: But, we<U+0092>ll stay here with you.

Ross: Thanks guys! (They both sit down on either side of him.) I really
appreciate this, y<U+0092>know, but you don<U+0092>t have to rub my butt.

(Chandler slowly takes his hand away.)

Commercial Break

[Scene: Ross and Emily<U+0092>s room, the next morning. Ross is now asleep and has his
head in Monica<U+0092>s lap and his feet on Chandler<U+0092>s lap. Monica and Chandler are
both still awake and depressed.]

Chandler: We have to leave for New York in an hour.

Monica: I know, I<U+0092>ve been looking at those doors, they look pretty sound
proof, don<U+0092>t you think?

Chandler: We can<U+0092>t do that that<U+0092>s insane. I mean <U+0091>A<U+0092> he
could wake up and <U+0091>B<U+0092> y<U+0092>know, let<U+0092>s go for it.

(They both try to slowly extricate themselves from Ross, but there<U+0092>s a knock on
the door that awakens him.)

Ross: Em-Emily? (Looking around for her.) Em-Emily? (He runs to the door.)
Emily! (He opens the door to reveal the Walthams standing outside.)

Mr. Waltham: No.

Mrs. Waltham: You can forget about Emily, she<U+0092>s not with us.

Mr. Waltham: We<U+0092>ve come for her things.

Ross: Wait, well wh-wh-wh-where is she?

Mr. Waltham: She<U+0092>s in hiding. She<U+0092>s utterly humiliated. She
doesn<U+0092>t want to see you ever again.

Mrs. Waltham: We<U+0092>re very sad that it didn<U+0092>t work out between you and
Emily, monkey. But, I think you<U+0092>re absolutely delicious.

Mr. Waltham: Excuse me, I<U+0092>m standing right here!

Mrs. Waltham: Oh yes, there you are.

Rachel: (entering, carrying an armful of those little soaps.) Hey-hey, you guys
oh hurry up, get some, there<U+0092>s a whole cart outside<U+0085> (Sees the Walthams and
stops.)

Mr. Waltham: Goodbye Geller.

Ross: Now, hold on! Hold on! (Stops him) Look, look, your daughter and I are
supposed to leave tonight for our honeymoon, now-now you-you tell her that I<U+0092>m gonna
be at that airport and I hope that she<U+0092>ll be there too! Oh yeah, I said Rachel<U+0092>s
name, but it didn<U+0092>t mean anything, Okay? She<U+0092>s-she<U+0092>s just a friend and
that<U+0092>s all! (Rachel sits down, depressed.) That<U+0092>s all! Now just tell Emily that
I love her and that I can<U+0092>t imagine spending my life with anyone else. Please,
promise me that you<U+0092>ll tell her that.

Mr. Waltham: All right, I<U+0092>ll tell her. (To his wife) Come on bugger face!

Mrs. Waltham: (As she walks pass Ross, she pats his but.) Call me.

Mr. Waltham: You spend half your life in the bathroom, why don<U+0092>t you ever
go out the bloody window!

[Scene: A 747 somewhere over the North Atlantic, Monica and Chandler are sitting in
first class, depressed.]

Monica: Y<U+0092>know, maybe it<U+0092>s best that we never got to do it again.

Chandler: Yeah, it kinda makes that-that one night special. (Realizes something)
Y<U+0092>know, technically we still are over international waters.

Monica: I<U+0092>m gonna go to the bathroom, maybe I<U+0092>ll see you there in a
bit?

Chandler: <U+0091>Kay!

(Monica gets up and heads for the bathroom, Chandler turns to watch her go and is
startled to see Joey sitting in Monica<U+0092>s seat.)

Joey: Can I ask you something?

Chandler: Uhh, no.

Joey: Felicity and I, we<U+0092>re watching My Giant, and I was thinking,
"I<U+0092>m never gonna be as good an actor as that giant." Do you think I<U+0092>m
just wasting my life with this acting thing?

Chandler: No.

Joey: I mean, the giant is like five years younger than me, y<U+0092>know, you
think I<U+0092>ll ever get there?

Chandler: Yes.

Joey: Thanks man. 

Chandler: Okay man. (Chandler starts to get up.)

Joey: But what about how much taller he is than me?

(Time lapse, Chandler is finishing his third little bottle of booze.)

Joey: I mean, there<U+0092>s no way I can make myself taller now, y<U+0092>know? And
who knows what science will come up with in the future, but Chandler, what if I die an
unsuccessful, regular sized man?

(Monica returns.)

Joey: Hey, Monica, wow you<U+0092>ve been in the bathroom for like a half-hour.

Monica: I know!

Joey: Had the beef-tips, huh?

[Scene: Monica and Rachel's, Phoebe is eating cereal from a bowl she has balanced on
her stomach as Joey, Chandler, and Monica return.]

Phoebe: Hey!

Chandler: Hey!

Joey: Hi!

(They all hug.)

Phoebe: (To Joey) You ate meat! (Joey is shocked) (To Chandler and Monica) You
had sex! (They<U+0092>re shocked.)

Chandler: No we didn<U+0092>t!

Phoebe: I know you didn<U+0092>t, I was talking about Monica.

Monica: Phoebe, I did not have sex.

Phoebe: This pregnancy is throwing me all off.

Joey: All right, I<U+0092>m gonna go say hi the chick and the duck.

Phoebe: Oh, me too!

Joey: Why would you need to say hi to them, you<U+0092>ve been feeding them for
four days?

Phoebe: Oh right, maybe I<U+0092>ll just go home. 

(She grabs her bag and leaves, Joey moves a little quicker to his apartment, leaving
Monica and Chandler alone.)

Monica: Well, we certainly are alone.

Chandler: Yes! Good thing we have that, <U+0091>Not in New York<U+0092> rule.

Monica: Right. Umm, listen since we<U+0092>re-we-re on that subject, umm, I just
wanted to tell you that uh, well, I-I was going through a really hard time in London, what
with my brother getting married and that guy thinking I was Ross<U+0092>s mother<U+0085>

Chandler: Right.

Monica: Well, an-anyway, I just<U+0097>that night meant a lot to me, I guess
I<U+0092>m just trying to say thanks.

Chandler: Oh. Y<U+0092>know, that night meant a lot to me too, and it wasn<U+0092>t
because I was in a bad place or anything, it just meant a lot to me <U+0091>cause,
you<U+0092>re really hot! Is that okay?

Monica: (laughs) That<U+0092>s okay.

Chandler: And I<U+0092>m cute too.

Monica: And you<U+0092>re cute too.

Chandler: Thank you! (They hug.) All right, I gotta go unpack.

Monica: Okay.

Chandler: Bye. 

(After he closes the door, Monica starts to follow him, but thinks better of it and
stops.)

Chandler: (entering) I<U+0092>m still on London time, does that count?

Monica: That counts!

Chandler: Oh, good! (They start kissing.)

[Scene: An airport in London, Ross is waiting for Emily to show up to go on their
honeymoon and sees Rachel walking past.]

Ross: Rach! Rach!

Rachel: (she stops and turns) Hi!

Ross: Hi! What are you, what are you doing here?

Rachel: Well, I-I-I<U+0092>ve been on Standby for a flight home for hours. 

Ross: Oh.

Rachel: Ohh, so no sign of Emily huh?

Ross: Not yet.

Rachel: So umm, what time are you supposed to leave?

Gate Agent: (On the P.A.) This is the last call for Flight 1066 to
Athens. The last call.

Ross: Pretty soon I guess.

Rachel: Yeah. I<U+0092>m sorry.

Ross: I just, I don<U+0092>t understand, I mean, how-how can she do this?
Y<U+0092>know, what, am I, am I like a complete idiot for thinking that she<U+0092>d actually
show up?

Rachel: No, you<U+0092>re not an idiot, Ross. You<U+0092>re a guy very much in love.

Ross: Same difference.

Gate Agent: (On the P.A.) All ticketed passengers for Flight 1066 to
Athens should now be on board.

Ross: I get it! Well, that<U+0092>s that.

Rachel: No, you know what, I think you should go.

Ross: What?

Rachel: Yeah, I do. I think you should go, by yourself, get some distance, clear
your head, I think it<U+0092>d be really good.

Ross: Oh, I don<U+0092>t, I don<U+0092>t, I don<U+0092>t know<U+0085>

Rachel: Oh, come on Ross! I think it would be really good for you!

Ross: I could, yeah, I can do that.

Rachel: Yeah.

Ross: I can<U+0092>t, I can<U+0092>t even believe her! No, y<U+0092>know what, I am, I
am gonna go!

Rachel: Good!

Ross: I know, why not?

Rachel: Right!

Ross: Right?

Rachel: Right!

Ross: Y<U+0092>know<U+0097>thanks! (They hug)

Rachel: Okay, I<U+0092>ll see you back at home, if I ever get a flight out of
here.

Ross: Yeah, well<U+0085>nah.

Rachel: What? Wait, what?

Ross: Why don<U+0092>t you come, I mean, I-I have two tickets, why not?

Rachel: Well-well, I don<U+0092>t know Ross<U+0097>really?

Ross: Yeah, yeah, it<U+0092>ll be great! You can, you can lay on the beach and I
can cry over my failed marriage. See-see how I make jokes?

Rachel: Uh-huh.

Ross: No really, I mean, I mean, God, I could use a friend.

Rachel: Oh wow, uh okay, uh maybe. Umm, yes, I can do that!

Ross: Okay!

Rachel: Okay!

Ross: Cool!

Rachel: All right!

Ross: Come on! (They go to the jetway, Ross hands the tickets to the gate
agent.) Here. 

Rachel: Oh, okay, we<U+0092>re going. Yeah.

Ross: Ah! Ah! I forgot my jacket!

Rachel: Oh, wait-wait-wait<U+0085>

Ross: You tell them to wait!

Rachel: Okay. Wait! Wait!

(Ross retrieves his jacket and sees that not only has Emily arrived, but she as seen
Rachel take her place on the plane.)

Ross: Emily.

(She stares at him and Ross realizes what she<U+0092>s thinking.)

Ross: Oh no-no-no! Oh-no! (Emily starts to run out and Ross chases her.) No! No!
Emily!

Ending Credits

[Scene: Flight 1066 to Athens, Rachel is ordering a drink for Ross and herself.]

Rachel: Ahh, yes, I will have a glass of the Merlot and uh, (points to
Ross<U+0092>s seat.) he will have a white wine spritzer. Woo! (Looks out the window.) Hey,
look at that, the airport<U+0092>s moving. (Realizes that that<U+0092>s not how it works.)
Hey, are we moving?! Are we moving? Why are we moving? Hey, time-out, umm, yeah, does the
captain know that we<U+0092>re moving? (Sits back in defeat.) Oh my God. Oh, my gosh.

End




