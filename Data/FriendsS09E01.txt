

The One Where No One Proposes

Written by: Sherry Bilsing-Graham & Ellen Plummer
Transcribed by: Eric Aasen

[Scene: Rachel<U+0092>s Room, Joey moves Ross<U+0092>s coat to get the tissues Rachel wants
and the engagement ring box Mrs. Geller gave him falls out of the pocket it was inside.
Joey goes to one knee, picks up the box, opens it, and sees that it<U+0092>s an engagement
ring.]

Rachel: Joey.

(He turns to face Rachel on one knee with the box open.)

Rachel: (seeing the ring) Oh my God. (Pause) Okay.

(Joey is stunned.)

[Cut to Ross getting of an elevator carrying a bouquet of flowers and walking down the
hall to Rachel<U+0092>s room.]

[Cut back into Rachel<U+0092>s room.]

Rachel: So uh<U+0085>I guess we should<U+0085>make it official huh?

Joey: Uh<U+0085> Look Rach<U+0085>(Ross enters.) Hey Ross is here! Hey look!
It<U+0092>s my good friend Ross. Hey Ross.

Ross: Hey Joey. (To Rachel) Hey you.

Rachel: Hey you.

Joey: Hey and look he brought flowers. Thanks Ross, but I<U+0092>m really more of
a candy guy. (Laughs.)

Ross: You<U+0092>re weird today. (He turns to Rachel and Joey puts the ring back.)
(To Rachel) Listen I uh, wanted to talk to you about something.

Rachel: Uh yeah, actually I kinda need to talk to you too.

Ross: Uh Joey, can you give us just a minute?

Joey: No.

Ross: What?

Joey: Oh, I<U+0092>m sorry. I meant no.

Monica: (entering with everyone else including Mr. Geller) Hi! Hey look
who<U+0092>s here!

Mr. Geller: Where<U+0092>s my granddaughter? I<U+0092>ve been practicing my magic
tricks.

Chandler: He pulled a quarter out of my ear!

Ross: Hey, where<U+0092>s uh, where<U+0092>s mom?

Mr. Geller: She went to pick up Aunt Liddy.

Monica: Oh, Aunt Liddy<U+0092>s coming? That means we get five dollars each!

Mr. Geller: So when do I get to meet Emma and show her this? (Pulls a bouquet of
flowers out of his sleeve.)

Chandler: Okay. Wow.

Ross: Uh Dad, Emma<U+0092>s in the nursery. I<U+0092>ll take you now. If you want,
but (To Rachel) I really want to talk to you.

Rachel: I know, I still need to talk to you.

Joey: Oh hey but, before you guys do that (To Rachel) I need to talk to you, and
Ross, I need to talk to you.

Phoebe: (To Monica) Oh and I need to talk to you.

Monica: About what?

Phoebe: To see if know what these guys are talking about.

Opening Credits

[Scene: Outside the Nursery, everyone but Rachel is standing and looking into the
window.]

Monica: Isn<U+0092>t she beautiful?

Mr. Geller: Look at her, my first grandchild.

Ross: What about Ben?

Mr. Geller: Well of course Ben, I meant my first granddaughter. (To Monica,
mouths) Wow.

Phoebe: (taking Ross aside) Have umm, have you thought anymore about you and
Rachel?

Ross: Oh well yeah, actually I was going to talk to her when you guys all came
in the room.

Phoebe: Yay! It<U+0092>s so exciting! Wow, you could<U+0092>ve done that with us
there.

Ross: Yeah right.

Phoebe: Oh sure okay, you can touch yourself in front of us but you can<U+0092>t
talk to Rachel.

Ross: What?! When have I ever touched myself in front of you guys?

Phoebe: Oh please! Just before when you were asleep in the lounge! That Armenian
family was watching you instead of the TV. Oh, that reminds me. That Mr. Hasmeje still has
my Gameboy.

Joey: (taking Chandler aside) Hey Chandler, can I talk to you for a second?

Chandler: Sure.

Joey: Dude I just did something terrible.

Chandler: That was you?! I thought it was Jack!

Joey: No! No, that was Jack! Rachel thinks I asked her to marry me!

Chandler: What?! Why does she think that?

Joey: Because it kinda looked like I did.

Chandler: Again, what?!

Joey: Okay well, I was down on one knee with the ring in my hand<U+0085>

Chandler: As we all are at some point during the day.

Joey: It wasn<U+0092>t my ring! It fell out of Ross<U+0092>s jacket! And when I
knelt down to pick it up Rachel thought I was proposing!

Chandler: Ross had a ring?! And he was gonna propose?

Joey: I guess.

Chandler: And you did it first?! This is gonna kill him! You know how much he
loves to propose!

Joey: I know! I know it<U+0092>s awful.

Chandler: Well, what did she say?

Joey: (happily) She said yes.

Chandler: Does Ross know?

Joey: Oh God, what the hell am I going to tell him?

Chandler: Well maybe you don<U+0092>t have to tell him anything.

Joey: Oh, I like that. Yeah<U+0085>

Chandler: If you clear things up with Rachel then Ross never needs to find out,
but you have to do it now before he hears about it and kicks your ass!

Joey: (laughs) Now let<U+0092>s not get carried away. (He walks away as Monica
comes over and hugs Chandler from behind.)

Monica: I want a baby.

Chandler: Honey, we<U+0092>ve been over this. I need to be facing the other way.

Monica: Come on! Come on, if we have sex again it<U+0092>ll double our chances of
getting pregnant. Do you think that closet<U+0092>s still available?

Chandler: I<U+0092>m so tired. (She starts kissing him.) Yeah okay, but no
foreplay.

Monica: Deal!

[Back in front of the nursery window.]

Ross: Dad seriously! Y<U+0092>know you really should see someone about that!

Mr. Geller: Noted.

Ross: I wanna go talk to Rachel for a minute, are you gonna be okay alone for a
bit?

Mr. Geller: Are you kidding me, I could stay and look at her forever.

Ross: (noticing something) Actually umm<U+0085> (He turns Mr. Geller<U+0092>s head
to look at Emma.)

[Scene: Rachel<U+0092>s Room, Phoebe is entering.]

Phoebe: Hey!

Rachel: Hi.

Phoebe: Are you all right?

Rachel: Uhh<U+0085> I think I just got engaged.

Phoebe: Oh my God! He did it?

Rachel: Well<U+0085>did you know he was gonna ask me?

Phoebe: Are you kidding? I<U+0092>m like the one who talked him into it. I like to
think of myself as the puppet master of the group.

Rachel: And you really think this is a good idea?

Phoebe: I just talked him into it, don<U+0092>t tell me I have to do you too. The
puppet master gets tired people.

Rachel: I just don<U+0092>t know! It just doesn<U+0092>t feel right.

Phoebe: Why?! You two are so meant to be together, everybody thinks so.

Rachel: Really?! Even Ross?

Phoebe: Especially Ross!

Joey: (entering) Oh uh, hey Pheebs. Uh y<U+0092>know what? I<U+0092>ll-I<U+0092>ll
come back later. (He goes to leave but runs into Ross who<U+0092>s entering.)

Ross: Wow! Kind of uh, kind of a full house here. I<U+0092>ll guess
just<U+0085>I<U+0092>ll come back. (Ross exits followed by Joey.)

Phoebe: There he goes, your fianc�e. 

Rachel: I guess so.

Phoebe: Although he does play with himself in his sleep.

Rachel: I can<U+0092>t say that I<U+0092>m surprised.

[Scene: A hallway, Joey and Ross find Mr. Geller with his ear up against a
janitor<U+0092>s closet door.]

Ross: Dad, what are you doing?

Mr. Geller: I think there are people in there having sex.

(Ross turns to look at Joey.)

Joey: It can<U+0092>t be me, I<U+0092>m standing right here.

Mr. Geller: Wanna peek?

Ross: No!

Mr. Geller: Come on!

Ross: Y<U+0092>know what? I don<U+0092>t like you without mom. (To Joey) Come on.
(Walks away.)

Joey: (To Ross) We<U+0092>re not peeking? (Follows him.)

Mr. Geller: Well I<U+0092>m peeking. (He peeks.) Oh my God!

Chandler: Hello sir, you know Monica.

[Scene: Rachel<U+0092>s Room, she is taking the ring out of Ross<U+0092>s jacket, looks at
it, and puts it on her finger as Joey enters.]

Joey: Hey uh, is it okay to come in?

Rachel: Of course! Oh Joey, this ring I<U+0085>it<U+0092>s beautiful I love it!

Joey: Yeah uh look Rach, there<U+0092>s something I gotta tell ya.

(There<U+0092>s a knock on the door and a nurse enters carrying Emma.)

Rachel: Hey!

Nurse: Hey! Are you ready to try nursing again?

Rachel: Yeah! Hi Emma. Hey, why do you think she won<U+0092>t take my breast?

Nurse: It<U+0092>s all right honey, it takes some babies a while to get it, but
don<U+0092>t worry. It<U+0092>ll happen.

Joey: (watching) Yowsa! (Looks away.)

Rachel: Okay sweetie, you can do it. Just open up and put it in your mouth.

Joey: Dear Lord.

Rachel: I<U+0092>m sorry honey, what were you saying?

Joey: Oh uh-uh yeah, I think that<U+0085>

Rachel: Oh look, she<U+0092>s pulling away again! Do you think my nipples are too
big for her mouth? (Joey gets embarrassed.) She looks scared. Doesn<U+0092>t she look
scared?

Joey: Y<U+0092>know, I don<U+0092>t really know her.

Nurse: Why don<U+0092>t we try massaging the breast to stimulate the flow. (Does
so.)

Joey: (To God) Are you kidding me?!

Rachel: It<U+0092>s just so frustrating! Why doesn<U+0092>t she want my breast?!

Joey: I don<U+0092>t know! Maybe she<U+0092>s crazy! (Storms out.)

[Scene: The Lobby, Ross is eating a sandwich as Phoebe rushes up to him.]

Phoebe: Oh hey! Wait up!

Ross: Hi!

Phoebe: Congratulations! I didn<U+0092>t want to say anything in front of Joey
<U+0091>cause I didn<U+0092>t know if he knew yet.

Ross: What, that we had a baby? Come on let<U+0092>s give him a little credit,
although, he did eat a piece of plastic fruit earlier.

Phoebe: No! No, that you and Rachel are engaged!

Ross: What?

Phoebe: Oh, it<U+0092>s a secret. Oh goodie! Yes! We haven<U+0092>t done the secret
thing in a long time.

Ross: Phoebe, there is no secret. Okay? I didn<U+0092>t propose.

Phoebe: Are you lying? Is this like that time you tried to convince us that you
were a doctor?

Ross: (pause) I am a doctor! Y<U+0092>know what? I<U+0092>m just gonna go and
talk to Rachel myself.

Phoebe: All right, me too. (They go into her room and see that she<U+0092>s
sleeping.) Should we wake her up?

Ross: No! No, come on let her sleep! She<U+0092>s so exhausted.

Phoebe: And so engaged. (Points to the ring that Rachel is wearing.)

Ross: What? (Motions for Phoebe to go outside with him.) Oh my God! She-she
thinks we<U+0092>re engaged! Why? Why? Why would she think we<U+0092>re engaged?!

Phoebe: Perhaps because you gave her an engagement ring? Y<U+0092>know Ross
doctors are supposed to be smart.

Ross: I didn<U+0092>t give her that ring!

Phoebe: Really?

Ross: No!

Phoebe: So whose ring is it?

Ross: It<U+0092>s mine.

Phoebe: Is it an engagement ring?

Ross: Yes!

Phoebe: But you didn<U+0092>t give it to her?

Ross: No!

Phoebe: But you were going to propose?

Ross: No!!

Phoebe: Huh, I might be losing interest in this.

Ross: Look. Look, my mom gave me that ring because she wanted me to propose to
Rachel, but all I wanted to do is if she maybe<U+0085>kinda<U+0085>wanted
ah<U+0085>start<U+0085>things up again.

Phoebe: Oh, what beautiful lukewarm sentiment.

Ross: Look, I didn<U+0092>t want to rush into anything. And it seemed like she
didn<U+0092>t want to either. But I don<U+0092>t, I don<U+0092>t understand how any of this
happened! What? Did she find the ring in my jacket, assume that I was going to propose,
throw it on, and-and just start telling people?

Phoebe: No! No, she said you actually proposed to her.

Ross: Well I didn<U+0092>t! I didn<U+0092>t propose! (Pause) Unless uh<U+0085> (Pause)
Did I? I haven<U+0092>t slept in forty hours and<U+0085>it does sound like something I would
do.

Commercial Break

[Scene: The Janitor<U+0092>s Closet, Chandler and Monica are trying to figure out what to
do now.]

Chandler: Look, we can<U+0092>t stay in here forever.

Monica: Oh, I still can<U+0092>t believe my dad saw us having sex! He didn<U+0092>t
make it to one of piano recitals, but this he sees!

Chandler: This is okay. We<U+0092>re all adults here; there<U+0092>s nothing to be
ashamed of. Now, let<U+0092>s put our underwear in our pockets and walk out the door. (They
do so and find Mr. Geller leaning against a wall stunned.)

Monica: Hi Dad! I can still call you that right?

Mr. Geller: Of course. I<U+0092>ll always be your dad.

Chandler: I just want you to know that what you witnessed in there, that
wasn<U+0092>t for fun.

Monica: It wasn<U+0092>t fun?!

Chandler: (To Monica) Why? Why-why-would you<U+0097>Wh-why<U+0085> (To Mr. Geller)
Look, I just don<U+0092>t want you to think that we<U+0092>re animals who do it whenever we
want.

Mr. Geller: Oh, I don<U+0092>t think that. Before today I never thought of you two
having sex at all. It was a simpler time.

Monica: The truth is, Dad, we<U+0092>re-we<U+0092>re trying.

Mr. Geller: What?

Monica: Yeah, we<U+0092>re trying to get pregnant.

Mr. Geller: Oh my God! This is so exciting! Well, get back in there! (Points to
the closet) I<U+0092>ll guard the door!

Monica: Well, that<U+0092>s okay dad, we-we can wait until later.

Mr. Geller: Whoa-whoa-whoa! I don<U+0092>t think so! Aren<U+0092>t you ovulating?

Monica: Daddy?!

Mr. Geller: Well you gotta get at it princess! When your mother and I were
trying to conceive you, whenever she was ovulating, bam, we did it. That<U+0092>s how I got
my bad hip.

Chandler: That<U+0092>s funny, this conversation<U+0092>s how I got the bullet hole
in my head.

Mr. Geller: This one time I had my knee up on the sink and your mother, she
was<U+0085>

Monica: Daddy! I don<U+0092>t think we need to hear about the specific positions
you and mom had sex.

Mr. Geller: You<U+0092>re right, you<U+0092>re right. This is about your positions.
Now, what I saw in the closet is not the optimum position for conceiving a child, although
it might feel good.

Monica: I don<U+0092>t feel good right now.

Mr. Geller: But pleasure is important, (To Chandler) and it helps if the woman
has an orgasm. You up to the task sailor?

Chandler: Seriously sir, my brains? All over the wall.

[Scene: Rachel<U+0092>s Room, Monica is entering.]

Monica: Hey.

Rachel: Hey. I need to tell you something.

Monica: Well, now<U+0092>s a good time. I<U+0092>m on my way to have my ears cut
off.

Rachel: Joey asked me marry him.

Monica: What?

Rachel: Joey proposed to me.

Monica: Is he crazy?! You just had Ross<U+0092>s baby!

Rachel: Well, I-I said yes.

Monica: What?! Are you crazy? You just had Ross<U+0092>s baby! It<U+0092>s-it<U+0092>s
so inappropriate. No, it<U+0092>s worse than that. It<U+0092>s wrong. It<U+0092>s<U+0085> It is
bigger than mine! (Rachel<U+0092>s engagement ring.)

Rachel: I know. Days of Our Lives, thank you very much.

Monica: You can<U+0092>t marry him!

Rachel: Why not? I don<U+0092>t want to do this alone! And he<U+0092>s such a sweet
guy and he loves me so much.

Monica: Well do you love him?

Rachel: Sure.

Monica: Sure?

Rachel: Yeah, I mean whatever.

Monica: Honey, the question is<U+0085>do you really want to marry Joey?

Rachel: No. No, I don<U+0092>t. Could you be a dear and go tell him?

[Scene: A Hallway, Chandler is following Joey.]

Chandler: You still haven<U+0092>t told Rachel you weren<U+0092>t really proposing?

Joey: No! She had the ring on, she seemed so excited, and then she took her
breast out.

Chandler: Joey, you have to tell her what<U+0092>s going on! And what did it look
like?!

Joey: I didn<U+0092>t look at it. Stupid baby<U+0092>s head was blocking most of it.

Chandler: Go and tell Rachel right now before Ross finds out.

Joey: Look, it<U+0092>s not that easy. She said she wanted to marry me. I
don<U+0092>t want to hurt her.

Chandler: Okay, look, just do it gently.

Joey: You<U+0092>re right. You<U+0092>re right. I-I<U+0092>ll go tell her now before
Ross finds out and I<U+0092>ll be gentle. I can do that. I am a gentle person. Oh, by the
way. Two people screwing in there (Points to the closet Chandler and Monica were in) if
you want to check that out.

[Scene: Rachel<U+0092>s Room, Ross is entering.]

Ross: Hey.

Rachel: Hey.

Ross: Listen, I um<U+0085> I heard about the engagement.

Rachel: Surprised?

Ross: And confused. Rach, sweetie, I-I um<U+0085>I didn<U+0092>t propose to you.

Rachel: I know.

Ross: I don<U+0092>t think you do.

Rachel: You didn<U+0092>t propose to me. Joey did.

Ross: Poor baby, you<U+0092>re so tired. Rach, I didn<U+0092>t propose to you, Joey
didn<U+0092>t propose to you, and Chandler didn<U+0092>t propose to you.

Rachel: Uh<U+0085> You didn<U+0092>t propose to me, Chandler didn<U+0092>t propose to
me, but Joey did.

(Joey enters.)

Ross: Joey proposed to you?

Joey: I can come back.

Ross: Hey, wait! Wait-wait-wait! Joey, did you propose to her?

Joey: No.

Rachel: Yes you did!

Joey: Actually, technically, I didn<U+0092>t.

Rachel: Well then why did you give me a ring?

Ross: Wait! Whoa-whoa, you<U+0085>you gave her the ring?

Joey: No! No, and I did not ask her to marry me!

Rachel: Yes, you did!

Joey: No, I didn<U+0092>t!

Rachel: Yes, you did!

Joey: No, I didn<U+0092>t!

Rachel: Yes, you did! And don<U+0092>t you say, "No, I didn<U+0092>t!"

Joey: Ahhh!

Rachel: He was right there. He got down on one knee and proposed.

Ross: Whoa! You were down on one knee?

Joey: Yeah. Yeah, that looks bad. But I didn<U+0092>t<U+0085>I didn<U+0092>t propose!

Ross: Then what did happen?

Rachel: Yeah, what did happen?

Joey: Okay, the ring fell on the floor and I went down to pick it up and you
thought I was proposing.

Rachel: Yeah, but you said, "Will you marry me?"

Joey: No, I didn<U+0092>t!

Rachel: Yes, you did!

Joey: No, I didn<U+0092>t!

Rachel: Yes, you did<U+0097>Oh my God you didn<U+0092>t! (Screams) Well then why
didn<U+0092>t you tell me that before?!

Joey: Well I tried, but people kept coming in and then you took your breast out!

Ross: Whoa! Hey! Whoa-whoa-whoa, you saw her breast?!

Joey: (To Ross) I<U+0092>ll tell you about it later. Be cool.

Rachel: Well then Joey, what the hell were you doing with an engagement ring?!

Joey: It wasn<U+0092>t my ring! It<U+0092>s Ross<U+0092>s ring! That<U+0092>s why I felt
so bad Rach, because he was going to propose.

Ross: What?!

Rachel: You were gonna propose to me?

Ross: Uhh<U+0085> No.

(An awkward silence follows.)

Joey: Well, this is awkward. {See? I told you so.}

Ross: But I-I was going to see if y<U+0092>know, maybe you uh, start dating again
but that<U+0097>I mean that-that was all, Rach.

Joey: Dude, step up! I proposed.

Ross: No, you didn<U+0092>t!

Joey: Oh that<U+0092>s right. There<U+0092>s a lot going on here and I think I ate
some bad fruit earlier.

(There<U+0092>s a knock on the door and the Nurse enters carrying Emma.)

Nurse: Hey, she just woke up! She<U+0092>s hungry. Why don<U+0092>t we give this
another try?

Rachel: Okay.

Ross: (To Joey) I can<U+0092>t believe you told her I was going to propose!

Joey: I can<U+0092>t believe you<U+0092>re not going to propose!

Ross: Hey, I<U+0092>m not going to rush into anything!

Joey: Oh yeah, dude, I totally understand. Usually after I have a baby with a
woman I like to slow things down!

Rachel: Oh my God!

Ross: What?

Rachel: She<U+0092>s doing it Look, she<U+0092>s breast-feeding look!

Joey: (looking at the ceiling) Ah, it<U+0092>s beautiful.

Nurse: I<U+0092>ll come back for her later.

Rachel: Okay.

Ross: Thank you. (The nurse exits.)

Rachel: Oh wow, this feels weird.

Ross: Good weird?

Rachel: Wonderful weird.

Joey: Y<U+0092>know what you guys? I<U+0092>m uh, I<U+0092>m gonna go too. And uh,
I<U+0092>m sorry about everything.

Rachel: Honey don<U+0092>t worry, it was my mistake.

Joey: No, Rach, I should<U+0092>ve told you sooner. It<U+0092>s just that<U+0085>Man!
That kid is going to town! (Joey makes his awkward exit.)

Rachel: She<U+0092>s perfect.

Ross: We<U+0092>re so lucky.

Rachel: We really are.

Ross: Look, I-I know it<U+0092>s not a proposal and I don<U+0092>t know where you
are, but with everything that<U+0092>s been going on and with Emma and<U+0085>I<U+0092>ve been
feeling<U+0085>

Rachel: I know. I know. I<U+0092>ve feeling<U+0085>

Ross: Yeah?

Rachel: Yeah. (Laughs nervously)

Ross: Okay, well, that<U+0085> Wow, okay, well, umm<U+0085>then maybe, at least we
can, we can talk about us again.

Rachel: Yeah, maybe.

Ross: Well good, okay. I-I, kind of think y<U+0092>know if we<U+0085>if<U+0085>
You<U+0092>re wearing the ring.

(Pause.)

Rachel: Wh-what<U+0092>s that?

Ross: And you told Phoebe you were engaged.

Rachel: I<U+0092>m sorry, what?

Ross: When you thought Joey proposed did<U+0085>did you say yes?

Closing Credits

[Scene: Outside the Janitor<U+0092>s Closet, there are people having sex and Mr. Geller
is trying to give them some pamphlets.]

Mr. Geller: Kids, I spoke to a doctor and picked up this pamphlets on how to get
pregnant. (He slides them under the door.)

Monica: (walking by with Chandler.) Hey dad!

Chandler: Hey.

Mr. Geller: (pause) Sorry to bother you again, but could you pass my pamphlets
back? (They do so.) Thank you.

End

